//
//  MessageSycWebService.swift
//  Demo
//
//  Created by Ankur Kumar on 11/3/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation
public class MessageSycWebService: NSObject {
    var userToken:NSString?
    var appConstants: AppConstants = AppConstants()
    
    let ContentType = "Content-Type"
    let ContentTypeValue = "application/json"
    
    override init() {
        super.init()
        userToken = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.token)
    }
    
    func HTTPPostJSON(url: String,  data: NSString,
                      callback: (String, String?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        request.HTTPBody = data.dataUsingEncoding(NSUTF8StringEncoding);
        
        request.setValue(ContentTypeValue, forHTTPHeaderField: ContentType)
        HTTPsendRequest(request, callback: callback)
    }
    
    
    
    func HTTPGetJSON(url: String,  data: NSString,
                      callback: (String, String?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        request.HTTPBody = data.dataUsingEncoding(NSUTF8StringEncoding);
        
        request.setValue(ContentTypeValue, forHTTPHeaderField: ContentType)
        HTTPsendRequest(request, callback: callback)
    }
    
    func HTTPsendRequest(request: NSMutableURLRequest,
                         callback: (String, String?) -> Void) {
        let task = NSURLSession.sharedSession()
            .dataTaskWithRequest(request) {
                (data, response, error) -> Void in
                if (error != nil) {
                    callback("", error!.localizedDescription)
                } else {
                    callback(NSString(data: data!,
                        encoding: NSUTF8StringEncoding)! as String, nil)
                }
        }
        
        task.resume()
    }
    
}

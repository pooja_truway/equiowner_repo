//
//  WebService.swift
//  Demo
//
//  Created by KOMAL SHARMA on 06/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation

class WebService: NSObject {
    var userToken:NSString?
    var appConstants: AppConstants = AppConstants()

     override init() {
        super.init()
        userToken = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.token)        
    }
    
    func sha256(securityString : String) {
        let data = securityString.dataUsingEncoding(NSUTF8StringEncoding)!
        var hash = [UInt8](count: Int(CC_SHA256_DIGEST_LENGTH), repeatedValue: 0)
        CC_SHA256(data.bytes, CC_LONG(data.length), &hash)
        let output = NSMutableString(capacity: Int(CC_SHA256_DIGEST_LENGTH))
        for byte in hash {
            output.appendFormat("%02x", byte)
        }
        
        userToken = output.uppercaseString
    }
    
    func HTTPPostJSON(url: String,  data: NSString,
                      callback: (String, String?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "POST"
        request.HTTPBody = data.dataUsingEncoding(NSUTF8StringEncoding);
        
        
        if !SharedPreferenceUtils._sharedInstance.getBoolValue(appConstants.IS_LOGIN){
            sha256("HorseWebApp")
        }
        userToken = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.token)
        print(userToken)
        
        request.setValue("\(userToken!)", forHTTPHeaderField: appConstants.token)
        
       
       // request.timeoutInterval=120
        HTTPsendRequest(request, callback: callback)
    }
    
    func HTTPsendRequest(request: NSMutableURLRequest,
                         callback: (String, String?) -> Void) {
        let task = NSURLSession.sharedSession()
            .dataTaskWithRequest(request) {
                (data, response, error) -> Void in
                if (error != nil) {
                    callback("", error!.localizedDescription)
                } else {
                    callback(NSString(data: data!,
                        encoding: NSUTF8StringEncoding)! as String, nil)
                }
        }
        
        task.resume()
    }
    
    
   // to send multipart form data to server
func HTTPMultiFormApiRequest(urlPath:String,fileName:String,data:NSData,
        completionHandler: (NSURLResponse!, NSData!, NSError!) -> Void){
        
        let url: NSURL = NSURL(string: urlPath)!
        let request1: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        request1.HTTPMethod = "POST"
        
        let boundary = "*****"
        
        let fullData = (data,boundary:boundary,fileName:fileName)
        
        request1.setValue("multipart/form-data; boundary=" + boundary,
                          forHTTPHeaderField: "Content-Type")
        
        // REQUIRED!
       request1.setValue("\(userToken!)", forHTTPHeaderField: appConstants.token)
        
        request1.HTTPBody = data
        request1.HTTPShouldHandleCookies = false
        
        let queue:NSOperationQueue = NSOperationQueue()
        
     //   NSURLConnection.sendAsynchronousRequest(request1,queue: queue,completionHandler:completionHandler)
    }
    
    
    
    
    func UploadRequest(urlPath:String,fileName:String,data:NSData)
                       
    {
        let url = NSURL(string:urlPath)
        
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        
        let boundary = generateBoundaryString()
        
        //define the multipart request type
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
     //   let image_data = UIImagePNGRepresentation(image.image!)
        
        

        let body = NSMutableData()
        
        let fname = "test.mp4"
        let mimetype = "image/png"
        
        //define the data post parameter
        
        body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData("Content-Disposition:form-data; name=\"test\"\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData("hi\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        
        
        body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData("Content-Disposition:form-data; name=\"file\"; filename=\"\(fname)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData("Content-Type: \(mimetype)\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(data)
        body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        body.appendData("--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        
        
        request.HTTPBody = body
        
        
        
        let session = NSURLSession.sharedSession()
        
        
        let task = session.dataTaskWithRequest(request) {
            (
            data, response, error) in
            
            guard let _:NSData = data, let _:NSURLResponse = response, error == nil else {
                print("error")
                return
            }
            
            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print(dataString)
            
        }
        
        task.resume()
        
    }
    
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().UUIDString)"
    }
    
    
    
    
    
   /* func uploadGalleryDataInServer(horseID: String, source: String, ext: String, content: String, callback: (String, String?) -> Void){
        //send to server
        let parameters = ["action": "galleryadd",
                          "userId": SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID),
                          "horse_id": horseID,
                          "source":source,
                          "ext":ext,
                          "content":content]
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: NSURL(string: appConstants.LOGIN)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        request.setValue("\(userToken!)", forHTTPHeaderField: appConstants.token)

        do{
        request.HTTPBody = NSJSONSerialization.dataWithJSONObject(parameters, options: nil)
        let dataTask = session.dataTaskWithRequest(request) { data, response, error in
            if (error != nil) {
                callback("", error!.localizedDescription)
            } else {
                callback(NSString(data: data!,
                encoding: NSUTF8StringEncoding)! as String, nil)
            }
        }
    
        dataTask.resume()
        }  catch let parseError {
                print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: '\(jsonStr)'")
            }
    }*/
}

//
//  String.swift
//  Demo
//
//  Created by jyoti on 10/17/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

extension UIFont {
    func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRectWithSize(CGSize(width: width, height: DBL_MAX),
                                                             options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                                                             attributes: nil,
                                                             context: nil).size
    }
}
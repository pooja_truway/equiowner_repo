//
//  Constants.swift
//  Demo
//
//  Created by KOMAL SHARMA on 04/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import AVKit
import AVFoundation
import CoreData
import Foundation

class AppConstants {
   
    //Aplication constants
    var screenWidth = UIScreen.mainScreen().bounds.width
    var screenHeight = UIScreen.mainScreen().bounds.height
    var ScreenWidthFactor = UIScreen.mainScreen().bounds.width/320
    var ScreenHeightFactor = UIScreen.mainScreen().bounds.height/568
    var ScreenFactor = sqrt(pow(UIScreen.mainScreen().bounds.width/320, 2)+pow(UIScreen.mainScreen().bounds.height/568, 2))
    static var SelectedMenu: Int = 0
    static var CurrentScreen: Int = 0
    static var CurrentHorseID: String = ""
    static var dataModelForDetailEditing: NSMutableDictionary!
    static var checkBoxImageArray: NSMutableArray!

    let HomeScreen: Int = 0
    let HorseListingScreen: Int = 1
    let HorseDetailingScreen: Int = 2
    let DailyActivityFromDetailScreen: Int = 3
    let DailyActivityScreen: Int = 4
    let AddDailyActivityScreen: Int = 5
    let CalendarScreen: Int = 6
    let AddOrUpdateEventScreen: Int = 7
    let GalleryScreen: Int = 8
    let OwnerListingScreen: Int = 9
    let OwnerDetailScreen: Int = 10
    let SettingsScreen: Int = 11
    let GalleryScreenFromHorseDetail: Int = 12
    let ChangePasswordScreen: Int = 13
    let AddDailyActivityFromDetailScreen: Int = 14
    let ShareScreenFromGalleryScreenFromHorseDetail: Int = 15
    let ShareScreenFromGalleryScreen: Int = 16
    let MessagingContactListScreen: Int = 17
    let DailyActivityDetailScreenFromDetailScreen: Int = 18
    let DailyActivityDetailScreen: Int = 19
    let MessagingScreen: Int = 20
    let MessagingScreenFromOwnerListing: Int = 21
    let OwnerScreenFromMessageHorseListing: Int = 22
    
    let OwnerScreenDetailByPooja: Int = 23
    let NotificationScreen: Int = 24

    //Notification Keys
    let kN_UpdateProfilePicNotification = "kN_UpdateProfilePicNotification"
    let kN_ShowHorseCredentailsInHeaderNotification = "kN_ShowHorseCredentailsInHeaderNotification"
    let kN_StartTimerNotification = "kN_StartTimerNotification"
    let kN_StopTimerNotification = "kN_StopTimerNotification"
    let kN_FetchTimerNotification = "kN_FetchTimerNotification"

    //App URL
    //let HORSE_HOST_NAME = "http://www.webapponline.mobi/webservice"
    
    //Demo App URL
    //let HORSE_HOST_NAME = "http://www.99informations.com/Equiowner/webservice"
    
    //Live Login
    let LOGIN_WEBAPP = "http://www.webapponline.mobi/webservice"
    
    //New Live Login
    let LOGIN = "http://www.equiowner.club/webservice"
    
    let DAILY_ACTIVITY = "http://www.webapponline.mobi/webservice"
    
 
    
    //Demo Login
  // let LOGIN = "http://www.99informations.com/Equiowner/webservice"

    //Production
    let INITIATE_CHAT = "http://www.webapponline.mobi/webservice"
    
    //DemoProduction
   // let INITIATE_CHAT = "http://www.99informations.com/Equiowner/webservice/"

    //Chat URL
    let REGISTER_USER_URL = "http://65.60.41.2/~eservices/DoctorApp/horseapp/registerUserForMessage.php"
    let SEND_MESSAGE_URL  = "http://65.60.41.2/~eservices/DoctorApp/horseapp/sendMessage.php"
    let GET_MESSAGE_URL  = "http://65.60.41.2/~eservices/DoctorApp/horseapp/getmessages.php"  //crash resolved     

   //IP for chat service changed from 162.221.190.50 to this 65.60.41.2 on 24-4-2017
    //Data Constants
    let IS_LOGIN = "is_login"
    let LOGIN_DATA = "LOGINDATA"
    let DAILYACTIVITYDATA = "dailyactivitytask"
    let GALLERY_DATA = "GALLERY_DATA"
    let ISNOTIFICATIONSON = "NOTIFICATIONS";
    let PROFILE_IMAGE = "profileequitrack";
    let PASSWORD = "PASSWORD";
    let ISOWNER = "ISOWNER";
    let FIRST_TYM_IMAGE = "FIRST_TYM_IMAGE";
    let GCM_TOKEN = "GCM_TOKEN";
    let EVENT_IMAGE_STRING = "";
    let EVENT_IMAGE = "profilePic_code";
    let audioString = "";
    let Base64AudioString = "";

    
    //UI Constants
    let CornerRadius: CGFloat = 5.0
    let button_color = "#1F9951"
    let font_white_color = "#ffffff"
    let font_black_color = "#000000"
    let bg_light = "#20242a"
    let green = "#008f00"
    let orange = "#ff5240"
    let add_daily_activity_bg = "#ededee"
    let bg_light_grey = "#7a8699"
    let outerColor = ["#8850be","#f9a94a","#fdc642","#83bd45","#bdcd60","#f58d89","#7cb1e4","#b54fa5"]
    let selectedColor = "#2ecc71"
    let daily_dark_bg = "#232323"
    let daily_light_bg = "#343434"
    let daily_done_bg = "#3a3a3a"
    let daily_not_done_bg = "#424242"
    
    let daily_pink = "#e6716b"
    let daily_blue = "#4670c4"
    let daily_sea = "#40bcbc"

    let accent_material_light = "#ff009688"
    
    //Webservice contants
    let ErrorCode = "ErrorCode"
    let Message = "Message"
    let message = "message" //pooja
    let fromUser = "fromUser" //pooja
    
    let token = "token"
    let data = "data"
    let data1 = "data1"
    
    let trainerID = "trainerID"

    let memberID = "memberID"
    let emailID = "emailID"
    let firstName = "firstName"
    let lastName = "lastName"
    let gender = "gender"
    let address = "address"
    let age = "age"
    let profilePic = "profilePic"
    let photoSize = "photoSize"
    let noOfPhoto = "noOfPhoto"
    let user_type = "user_type"
    let memberAliasName = "memberAliasName"
    let userName = "userName"
    let userPassword = "userPassword"
    let userToken = "userToken"
    let userSession = "userSession"
    let userSecreteKey = "userSecreteKey"
    let chatCreatedDate = "chatCreatedDate"
    let userSessionTime = "userSessionTime"
    let name = "name"
    let horseIdForAudio = "horseIdForAudio"

   // let owner_id = "owner_id"
     let owner_id = "userId"

    
    let media_base_url = "media_base_url"
    let logo = "logo"

    let notifications = "notifications"
    let notification = "notification"

    let horse_count = "horse_count"
    let unread_message = "unread_message"

    let horses = "horses"
    let _id = "id"
    let passport_id = "passport_id"

    let breed = "breed"
    let colour = "colour"
    let colour_change = "colour_change"
    let height_change = "height_change"
    let media_source = "media_source"
    let media_type = "media_type"
    let owner = "owner"
    let ownername = "name" //pooja
    let thumbnail = "thumbnail"
    let weight_change = "weight_change"
    
    let contacts = "contacts"
    let ResultContact = "ResultContact"
    let userId = "userId"
    let phone = "phone"
    
    
    let gallery = "gallery"
    let source = "source"
    let files = "files"
    
    let event = "event"
    let events = "events"
    let attending = "attendyID"
    let creationDate = "creationDate"
    let description = "description"
    let event_date = "event_date"
    let event_id = "event_id"
    let event_time = "event_time"
    let event_title = "event_title"
    let location = "location"

    let member_id = "member_id"
    let members = "members"
    let member_name = "member_name"
    let profice_pic = "profice_pic"
    let selected = "selected"

    let activity = "activity"
    let task = "task"
    let task_id = "task_id"
    let time = "time"
    let tasklist = "tasklist"
    let datatype_id = "datatype_id"
    let tasklist_value = "tasklist_value"

    let daily_activity_id = "daily_activity_id"
    let datasample = "datasample"
    let datatype = "datatype"
    let tasklist_comment = "tasklist_comment"
    let tasklist_id = "tasklist_id"
    let value_type = "value_type"
    let food = "food"
    
    let action = "action"
     
    // for chat
    let CHAT_USER_REGISTERED = "isUserRegister"
    let CHAT_USER_ID="chatUserid"
    //let CHAT_USER_PASSWORD="chatUserPassword"
    //let CHAT_USER_ID_SERVER="chatUserIdServer"
    let CHAT_MESSAGE_TEXT="message"
    let MESSAGE_TYPE="message_type"
    let current_id_chat = "current_id_chat"
    let MESSAGE_TIME="message_time"
    let MESSAGE_OPPID="message_oppid"
    let MESSAGE_Direction="message_direction"
    let MESSAGE_UID="messageId"

    //let CHAT_PASSWORD="123456789"
    let sender_bubble_color = "#5B666E"
    let reciever_bubble_color = "#5A9B76"
    let passwordChat: String! = "sbxsxbshjklmnb"
    let chat = "chat"
    let CHAT_CURRENT_ID = "current"
    
    
    var unitsItems = ["  litres", "  ml", "  gm", "  mg", "  kg"]
    var dailyItems = ["  Input", "  Manual"]
    
    var food_types = ["Rice Bran", "Sugarbeet Pulp", "Barley", "Corn", "Vegetable Oil", "Lucerne Chaff", "Oaten Chaff","C3 Type Grass Hay","Lucerne Hay" ]

    var unit_types = ["KG", "GM", "LBS", "Ltr", "ML", "MCAL", "MG","PPM","IU/LB","MG/LB" ,"KCAL/GM", "MJ/KG" , "MCAL/D", "POUND", "YARD", "KM" , "Meter" ]

    func getManagedObjectContext() -> NSManagedObjectContext {
        return (UIApplication.sharedApplication().delegate
            as! AppDelegate).managedObjectContext
    }
    
    func convertDictionaryToString(dict: NSDictionary) -> String? {
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(dict, options: [])
            if let json = NSString(data: data, encoding: NSUTF8StringEncoding) {
                return json as String
            }
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func convertStringToDictionaryAudio(text: String) -> [String:AnyObject]? {
        if let data1 = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data1, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    
    func showAlert(title: String , message: String, controller: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        controller.presentViewController(alert, animated: true, completion: nil)
    }
    
    func getCurrentDate() -> String {
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.stringFromDate(date)
    }
   
    func getBackDate() -> String {
        let now = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        now.dateByAddingTimeInterval(Double(60*60*24));
        return dateFormatter.stringFromDate(now)
    }
    
    func hour12To24HourTimeConverter(dateAsString: String) -> String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.dateFromString(dateAsString)
        
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.stringFromDate(date!)
    }
    
    func hour24To12HourTimeConverter(dateAsString: String) -> String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.dateFromString(dateAsString)
        
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.stringFromDate(date!)
    }

    func getAddEventFormattedDate(dateAsString: String) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateObj = dateFormatter.dateFromString(dateAsString)
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.stringFromDate(dateObj!)
    }
    
    
    func getDailyActivityFormattedDate(activityTime: String) -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd H:mm:ss"
        return dateFormatter.dateFromString(getCurrentDate() + " " + activityTime)!
    }
    
    
    func getMessageTime(messageDate : String) ->String
    {
        let date : NSDate = NSDate(timeIntervalSince1970: (Double(messageDate)!/1000.0))
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.stringFromDate(date)
    }
    
    
    
    func getMessageDate(messageDate : String) -> String
    {
        
        let date : NSDate = NSDate(timeIntervalSince1970: (Double(messageDate)!/1000.0))
        //print(dateMilliSecond)
        //        let dateFormatter = NSDateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        //        let date : Foundation.NSDate = dateFormatter.dateFromString(dateMilliSecond)!
        //
        let dateformatter = NSDateFormatter()
        dateformatter.dateStyle = NSDateFormatterStyle.ShortStyle
        var dateStrValue = dateformatter.stringFromDate(date)
        
        
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = "EEEE, MMM dd"
        let dayOfWeekString = dateFormatter1.stringFromDate(date)
        //print(dayOfWeekString)
        dateStrValue = dayOfWeekString
        
        let currentDate = NSDate()
        
        
        
        let  menuDateComare = NSCalendar.currentCalendar().compareDate(currentDate, toDate: date,
                                                                       toUnitGranularity: .Day)
        
        switch menuDateComare {
        case .OrderedDescending:
            print("DESCENDING")
        case .OrderedAscending:
            
            let interval = NSTimeInterval(60 * 60 * 24 * 1)
            let newDate = currentDate.dateByAddingTimeInterval(interval)
            
            let  menuDateComare1 = NSCalendar.currentCalendar().compareDate(newDate, toDate: date,
                                                                            toUnitGranularity: .Day)
            switch menuDateComare1 {
                
            case .OrderedAscending:
                break
                //  print("ASCENDING")
                
            case .OrderedSame:
                
                
                dateStrValue = "Tomorrow"
                
            default : break
                
                
                
            }
            
            
        //  print("ASCENDING")
        case .OrderedSame:
            //  print("SAME")
            dateStrValue = "Today"
        }
        
        return dateStrValue
    }
    
    func nullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    func getHorseAppFolderPath() -> String{
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0])
        let dataPath = documentsPath.URLByAppendingPathComponent("HorseApp")
        do {
            try NSFileManager.defaultManager().createDirectoryAtPath(dataPath!.path!, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        
        return dataPath!.path!
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL!.path!
        
    }
    
    func isVideoSaved(serverFileName:String) -> Bool{
        let filemgr = NSFileManager.defaultManager()
        let filePath="\(getDocumentsURL().path!)/\(serverFileName)";
        
        if filemgr.fileExistsAtPath(filePath) {
            return true
        } else {
            return false
        }
    }

    func showVideoThumbnail(path: String, imageView: UIImageView){
        let videoURL = NSURL.fileURLWithPath(path)
        let asset : AVAsset = AVAsset(URL: videoURL)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        var time        : CMTime = asset .duration
        time.value = 1000 // 1 Sec timeframe
        
        do {
            let img: CGImageRef = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            imageView.image = UIImage(CGImage: img)
            
        } catch let error as NSError {
            print(error.localizedDescription);
        }
    }

    func styleLabels(label: UILabel){
        label.textColor = UIColor.blackColor()
        label.font = UIFont.init(name: "Helvetica", size: 8 * ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
        label.textAlignment = NSTextAlignment.Left
    }
    
    func styleLabelsNew(label: UILabel){
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.init(name: "Helvetica", size: 8 * ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
        label.textAlignment = NSTextAlignment.Left
    }
    
    func styleWhiteColorLabels(label: UILabel){
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.init(name: "Helvetica", size: 10 * ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
        label.textAlignment = NSTextAlignment.Left
    }
    
    func styleInnerLabels(label: UITextField){
        label.textColor = UIColor().HexToColor(bg_light_grey)
        label.font = UIFont.init(name: "Helvetica", size: 8 * ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
        label.textAlignment = NSTextAlignment.Left
    }
    
    func styleFloatingFields(view: SkyFloatingLabelTextField){
        view.font = UIFont.init(name: "Helvetica", size: 8 * ScreenFactor)
        view.backgroundColor = UIColor.clearColor()
        view.tintColor = UIColor().HexToColor(button_color) // the color of the blinking cursor
        view.textColor = UIColor().HexToColor(bg_light_grey)
        view.lineColor = UIColor.clearColor()
        view.errorColor = UIColor.redColor()
        view.placeholderColor = UIColor().HexToColor(button_color)
        view.selectedTitleColor = UIColor().HexToColor(bg_light_grey)
        view.selectedLineColor = UIColor.clearColor()
        view.placeholderFont = UIFont.init(name: "Helvetica", size: 8 * ScreenFactor)
    }
    
    func RBSquareImageTo(image: UIImage, size: CGSize) -> UIImage {
        return RBResizeImage(RBSquareImage(image), targetSize: size)
    }
    
    func RBSquareImage(image: UIImage) -> UIImage {
        let originalWidth  = image.size.width
        let originalHeight = image.size.height
        var x: CGFloat = 0.0
        var y: CGFloat = 0.0
        var edge: CGFloat = 0.0
        
        if (originalWidth > originalHeight) {
            // landscape
            edge = originalHeight
            x = (originalWidth - edge) / 2.0
            y = 0.0
            
        } else if (originalHeight > originalWidth) {
            // portrait
            edge = originalWidth
            x = 0.0
            y = (originalHeight - originalWidth) / 2.0
        } else {
            // square
            edge = originalWidth
        }
        
        let cropSquare = CGRectMake(x, y, edge, edge)
        let imageRef = CGImageCreateWithImageInRect(image.CGImage!, cropSquare);
        
        return UIImage(CGImage: imageRef!, scale: UIScreen.mainScreen().scale, orientation: image.imageOrientation)
    }
    
    func RBResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func randomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
    func randomStringWithLength(len: Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for (var i = 0; i < len; i++){
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        return randomString
    }
}

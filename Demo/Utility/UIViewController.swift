//
//  UIViewController.swift
//  Demo
//
//  Created by KOMAL SHARMA on 05/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    func dismissKeyboard() {
        view.endEditing(true)
    }
}
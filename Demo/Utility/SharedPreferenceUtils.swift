//
//  SharedPreferenceUtils.swift
//  Demo
//
//  Created by KOMAL SHARMA on 02/08/16.
//  Copyright © 2016 HorseApp. All rights reserved.
//

import Foundation

class SharedPreferenceUtils: NSObject {
    
    static var _sharedInstance: SharedPreferenceUtils!

    private static var setupOnceToken: dispatch_once_t = 0
    private let defaults: NSUserDefaults?
    
    class func setup() -> SharedPreferenceUtils {
        dispatch_once(&setupOnceToken) {
            _sharedInstance = SharedPreferenceUtils()
        }
        return _sharedInstance
    }
    
    class var sharedInstance: SharedPreferenceUtils! {
        if _sharedInstance == nil {
        }
        
        return _sharedInstance
    }
    
    override init() {
        self.defaults = NSUserDefaults.standardUserDefaults()
    }
    
    func saveBoolValue(key : String, value: Bool) {
        self.defaults?.setBool(value, forKey: key)
        self.defaults?.synchronize()
    }
    
    func getBoolValue(key : String) -> Bool {
        return (self.defaults?.boolForKey(key))!
    }
    
    func saveIntValue(key : String, value: Int) {
        self.defaults?.setInteger(value, forKey: key)
        self.defaults?.synchronize()
    }
    
    func getIntValue(key : String) -> Int {
        return (self.defaults?.integerForKey(key))!
    }
    
    func saveFloatValue(key : String, value: Float) {
        self.defaults?.setFloat(value, forKey: key)
        self.defaults?.synchronize()
    }
    
    func getFloatValue(key : String) -> Float {
        return (self.defaults?.floatForKey(key))!
    }
    
    func saveStringValue(key : String, value: String) {
        self.defaults?.setValue(value, forKey: key)
        self.defaults?.synchronize()
    }
    
    func getStringValue(key : String) -> String {
        if self.defaults?.valueForKey(key) == nil {
            return ""
        }else{
            return (self.defaults?.valueForKey(key))! as! String
        }
    }
    
    func deleteAllData()  {
        for key in (self.defaults?.dictionaryRepresentation().keys)! {
            self.defaults?.removeObjectForKey(key)
        }
        self.defaults?.synchronize()
    }
    
   /* func saveCustomData(key : String, value: NSData){
        let data  = NSKeyedArchiver.archivedDataWithRootObject(value)
        self.defaults?.setObject(data, forKey: key)
        self.defaults?.synchronize()
    }
    
    func getCustomData(key : String) -> NSData{
        if let data = self.defaults?.objectForKey(key) as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as! NSData
        }else{
            return NSData;
        }
    }*/
}

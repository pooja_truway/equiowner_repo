//
//  UIImageView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 14/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = UIViewContentMode.ScaleAspectFit) {
        guard let url = NSURL(string: link) else { return }
        contentMode = mode
        NSURLSession.sharedSession().dataTaskWithURL(url){
            (data, response, error) in
            guard
                let httpURLResponse = response as? NSHTTPURLResponse where httpURLResponse.statusCode == 200,
                let data = data where error == nil,
                let image = UIImage(data: data)
                else { return }
            
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                
              self.image = image

            }
        }.resume()
    }
    
    func downloadWithCallback(link: String, contentMode mode: UIViewContentMode = UIViewContentMode.ScaleAspectFit, callback: (UIImage) -> Void) {
        guard let url = NSURL(string: link) else { return }
        contentMode = mode
        NSURLSession.sharedSession().dataTaskWithURL(url){
            (data, response, error) in
            guard
                let httpURLResponse = response as? NSHTTPURLResponse where httpURLResponse.statusCode == 200,
                let data = data where error == nil,
                let image = UIImage(data: data)
                else { return }
            
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.image = image
                callback(image)
            }
            }.resume()
    }
}

//
//  LoginInputViews.swift
//  Demo
//
//  Created by KOMAL SHARMA on 05/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class LoginInputViews: UIView {
    var icon: UIImageView = UIImageView()
    var line: UIView = UIView()
    var entryView: UITextField = UITextField()
    var appConstants: AppConstants = AppConstants()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = appConstants.CornerRadius
        self.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addView(type: Int) {
//        icon = UIImageView.init(frame: CGRectMake(10 * appConstants.ScreenFactor,0,10 * appConstants.ScreenFactor,10 * appConstants.ScreenFactor))
//        icon.center = CGPointMake(icon.center.x, self.frame.size.height/2)
//        icon.backgroundColor = UIColor.clearColor()
//        self.addSubview(icon)
//        
//        line.frame = CGRectMake(icon.frame.origin.x + icon.frame.size.width + 10 * appConstants.ScreenFactor, 5 * appConstants.ScreenFactor, 1, self.frame.size.height - 10 * appConstants.ScreenFactor)
//        line.backgroundColor=UIColor.whiteColor()
//        self.addSubview(line)
//        
        entryView.frame = CGRectMake(10 * appConstants.ScreenFactor-10, 5 * appConstants.ScreenFactor, self.frame.size.width - (line.frame.origin.x + line.frame.size.width + 10 * appConstants.ScreenFactor) - 5 * appConstants.ScreenFactor, self.frame.size.height - 10 * appConstants.ScreenFactor)
        entryView.textColor = UIColor.blackColor()
        entryView.tag = type
        entryView.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)

        entryView.backgroundColor = UIColor.whiteColor()
        self.addSubview(entryView)
        
        switch type {
        case 1:
            //User name
           // icon.image = UIImage.init(imageLiteral: "username.png")
            entryView.attributedPlaceholder = NSAttributedString(string:"Username",
                                                                 attributes:[NSForegroundColorAttributeName: UIColor.darkGrayColor()])
            entryView.keyboardType = UIKeyboardType.EmailAddress
        case 2:
            //password
       //     icon.image = UIImage.init(imageLiteral: "password.png")
            entryView.attributedPlaceholder = NSAttributedString(string:"Password",
                                                                 attributes:[NSForegroundColorAttributeName: UIColor.darkGrayColor()])
            entryView.secureTextEntry = true
        case 3:
            //Address
            icon.image = UIImage.init(imageLiteral: "ic_owner_contact.png")
            entryView.attributedPlaceholder = NSAttributedString(string:"Address",
                                                                 attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        case 4:
            //Email
            icon.image = UIImage.init(imageLiteral: "ic_message_contact.png")
            entryView.keyboardType = UIKeyboardType.EmailAddress
            entryView.attributedPlaceholder = NSAttributedString(string:"Email",
                                                                 attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        case 5:
            //Phone
            icon.image = UIImage.init(imageLiteral: "call_contact.png")
            entryView.keyboardType = UIKeyboardType.PhonePad
            entryView.attributedPlaceholder = NSAttributedString(string:"Phone no",
                                                                 attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        case 6:
            //last name
            icon.image = UIImage.init(imageLiteral: "username.png")
            entryView.attributedPlaceholder = NSAttributedString(string:"Last Name",
                                                                 attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
            entryView.keyboardType = UIKeyboardType.Alphabet
            
        default:
            break
        }
    }
}


//
//  HorseDetailView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 14/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class HorseDetailView: UIView, UIScrollViewDelegate, SwiftPhotoGalleryDelegate, SwiftPhotoGalleryDataSource {
    var yy:CGFloat = 0, margin:CGFloat = 0

    var webService: WebService = WebService()
    var appConstants: AppConstants = AppConstants()
    var media_base_url : String?
    var horseData : NSDictionary = NSDictionary()
    
    var name : UILabel = UILabel()
    var nameData : UILabel = UILabel()

    var owner : UILabel = UILabel()
    var ownerData : UILabel = UILabel()

    var breed : UILabel = UILabel()
    var breedData : UILabel = UILabel()
    
    var age : UILabel = UILabel()
    var ageData : UILabel = UILabel()

    var weightChange : UILabel = UILabel()
    var weightChangeData : UILabel = UILabel()

    var heightChange : UILabel = UILabel()
    var heightChangeData : UILabel = UILabel()

    var colorChange : UILabel = UILabel()
    var colorChangeData : UILabel = UILabel()
   
    var horseImage : UIImageView = UIImageView()

    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    var indicator = UIActivityIndicatorView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeVariables()
        drawTopView()
        drawBottomView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeVariables() {
        yy = 0
        margin = 5 * appConstants.ScreenFactor
    }
    
    func getHorseDetail(horseID: String){
       showProgress()
        
        //send to server
        let postString = "action=horsedetail&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&horse_id="+horseID;
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    self.appConstants.showAlert("Oops!", message: "Unable to fetch data...", controller: self.vc!)
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                       self.appConstants.showAlert("Oops!", message: "Unable to fetch data...", controller: self.vc!)
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                       // self.horseList = mainList.objectForKey(self.appConstants.horses) as! NSArray
                        print("mainList = \(mainList)")
                        self.media_base_url = mainList.objectForKey(self.appConstants.media_base_url) as? String
                        self.horseData = (mainList[self.appConstants.horses])! as! NSDictionary
                        self.setData()
                    }
                }
            }
        }
    }
 
    func drawTopView() {
        let nameOwnerLayout =  UIView.init(frame: CGRectMake(0,yy,self.frame.size.width , 30 * appConstants.ScreenFactor))
       // nameOwnerLayout.backgroundColor = UIColor().HexToColor(appConstants.bg_light)
        nameOwnerLayout.backgroundColor = UIColor.clearColor()
        self.addSubview(nameOwnerLayout)
        
        yy+=10
        name.frame = CGRectMake(margin,yy,self.frame.size.width/2 - 2,10 * appConstants.ScreenFactor)
        name.text = "Name"
        styleLabels(name)
        nameOwnerLayout.addSubview(name)
        
        let vline: UIView = UIView(frame : CGRectMake(name.frame.size.width , margin , 1, nameOwnerLayout.frame.size.height - margin - 2))
        vline.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        nameOwnerLayout.addSubview(vline)
        
        owner.frame = CGRectMake(self.frame.size.width/2 + margin * 0.5,yy,self.frame.size.width/2 - 1.5*margin,10 * appConstants.ScreenFactor)
        owner.text = "Owner"
        styleLabels(owner)
        nameOwnerLayout.addSubview(owner)
        
        yy+=name.frame.height
        
        nameData.frame = CGRectMake(margin,yy,self.frame.size.width/2 - 2,10 * appConstants.ScreenFactor)
        nameData.text = ""
        styleLabels(nameData)
        nameOwnerLayout.addSubview(nameData)
       
        ownerData.frame = CGRectMake(self.frame.size.width/2 + margin * 0.5,yy,self.frame.size.width/2 - 1.5*margin,10 * appConstants.ScreenFactor)
        ownerData.text = ""
        styleLabels(ownerData)
        nameOwnerLayout.addSubview(ownerData)
        
        let hline1: UIView = UIView(frame : CGRectMake(1.5*margin, nameOwnerLayout.frame.size.height - 2 , self.frame.size.width - 3*margin, 1))
        hline1.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        nameOwnerLayout.addSubview(hline1)
        
        yy = nameOwnerLayout.frame.origin.y + nameOwnerLayout.frame.size.height + 2
        
        //***** Breed Color Layout =========//
        let breedColorLayout =  UIView.init(frame: CGRectMake(0,yy,self.frame.size.width , 30 * appConstants.ScreenFactor))
      //  breedColorLayout.backgroundColor = UIColor().HexToColor(appConstants.bg_light)
        breedColorLayout.backgroundColor = UIColor.clearColor()
        self.addSubview(breedColorLayout)
        
        yy = 10
        breed.frame = CGRectMake(margin,yy,self.frame.size.width/2 - 2,10 * appConstants.ScreenFactor)
        breed.text = "Breed"
        styleLabels(breed)
        breedColorLayout.addSubview(breed)
        
        let vline1: UIView = UIView(frame : CGRectMake(name.frame.size.width , margin , 1, nameOwnerLayout.frame.size.height - margin - 2))
        vline1.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        breedColorLayout.addSubview(vline1)
        
        age.frame = CGRectMake(self.frame.size.width/2 + margin * 0.5,yy,self.frame.size.width/2 - 1.5*margin,10 * appConstants.ScreenFactor)
        age.text = "Age"
        styleLabels(age)
        breedColorLayout.addSubview(age)
        
        yy+=name.frame.height
        
        breedData.frame = CGRectMake(margin,yy,self.frame.size.width/2 - 2,10 * appConstants.ScreenFactor)
        breedData.text = ""
        styleLabels(breedData)
        breedColorLayout.addSubview(breedData)
        
        ageData.frame = CGRectMake(self.frame.size.width/2 + margin * 0.5,yy,self.frame.size.width/2 - 1.5*margin,10 * appConstants.ScreenFactor)
        ageData.text = ""
        styleLabels(ageData)
        breedColorLayout.addSubview(ageData)
        
        let hline2: UIView = UIView(frame : CGRectMake(1.5*margin, breedColorLayout.frame.size.height - 2, self.frame.size.width - 3*margin, 1))
        hline2.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        breedColorLayout.addSubview(hline2)
        
        yy = breedColorLayout.frame.origin.y + breedColorLayout.frame.size.height + 2
        horseImage.frame = CGRectMake(0,yy,self.frame.size.width,self.frame.size.height * 0.85 - yy)
        horseImage.userInteractionEnabled = true
        horseImage.image = UIImage.init(imageLiteral: "playbtn_i_new.png")

        let showFullScreenImageOrVideo = UITapGestureRecognizer(target: self, action: #selector(showFullScreenImageOrVideo(_:)))
        horseImage.addGestureRecognizer(showFullScreenImageOrVideo)
        self.addSubview(horseImage)
    }
    
    func drawBottomView() {
        yy = self.frame.size.height * 0.85
        let bottomItemWidth:CGFloat = self.frame.size.width/3
      
        let bottomFirstLayout =  UIView.init(frame: CGRectMake(0, yy, bottomItemWidth , self.frame.size.height * 0.15))
      //  bottomFirstLayout.backgroundColor = UIColor().HexToColor(appConstants.bg_light)
        bottomFirstLayout.backgroundColor = UIColor.clearColor()
        self.addSubview(bottomFirstLayout)
        
        
        weightChangeData.frame = CGRectMake(0,10,bottomFirstLayout.frame.size.width/2 - 2,7 * appConstants.ScreenFactor)
        weightChangeData.text = "0"
        weightChangeData.textAlignment = NSTextAlignment.Center
        styleLabels(weightChangeData)
        bottomFirstLayout.addSubview(weightChangeData)
        
        weightChange.frame = CGRectMake(bottomFirstLayout.frame.size.width/2,10,bottomFirstLayout.frame.size.width/2,8 * appConstants.ScreenFactor)
        weightChange.text = "Weight"
        weightChange.textAlignment = NSTextAlignment.Center
        styleLabels(weightChange)
        bottomFirstLayout.addSubview(weightChange)
        
        heightChangeData.frame = CGRectMake(0,18 * appConstants.ScreenFactor,bottomFirstLayout.frame.size.width/2 - 2,8 * appConstants.ScreenFactor)
        heightChangeData.text = "0"
        heightChangeData.textAlignment = NSTextAlignment.Center
        styleLabels(heightChangeData)
        bottomFirstLayout.addSubview(heightChangeData)
        
        heightChange.frame = CGRectMake(bottomFirstLayout.frame.size.width/2, 18 * appConstants.ScreenFactor,bottomFirstLayout.frame.size.width/2,8 * appConstants.ScreenFactor)
        heightChange.text = "Height"
        heightChange.textAlignment = NSTextAlignment.Center
        styleLabels(heightChange)
        bottomFirstLayout.addSubview(heightChange)
   
        colorChangeData.frame = CGRectMake(0,29 * appConstants.ScreenFactor,bottomFirstLayout.frame.size.width/2 - 2,8 * appConstants.ScreenFactor)
        colorChangeData.text = "0"
        colorChangeData.textAlignment = NSTextAlignment.Center
        styleLabels(colorChangeData)
        bottomFirstLayout.addSubview(colorChangeData)
        
        colorChange.frame = CGRectMake(bottomFirstLayout.frame.size.width/2,29 * appConstants.ScreenFactor,bottomFirstLayout.frame.size.width/2,8 * appConstants.ScreenFactor)
        colorChange.text = "Colour"
        colorChange.textAlignment = NSTextAlignment.Center
        styleLabels(colorChange)
        bottomFirstLayout.addSubview(colorChange)
        
        let viewGallery : UIView = UIView(frame:CGRectMake(bottomItemWidth,yy,bottomItemWidth-10,self.frame.size.height * 0.15))
        //viewGallery.backgroundColor = UIColor().HexToColor(appConstants.green);
        viewGallery.backgroundColor = UIColor.clearColor()
        self.addSubview(viewGallery)

        let viewGalleryLabel : UILabel = UILabel()
        viewGalleryLabel.frame = CGRectMake(0,20,bottomItemWidth,20 * appConstants.ScreenFactor)
        viewGalleryLabel.text = "View Gallery"
        viewGalleryLabel.textAlignment = NSTextAlignment.Center
      
        styleLabels(viewGalleryLabel)
        viewGallery.addSubview(viewGalleryLabel)
        viewGallery.backgroundColor=UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        viewGallery.userInteractionEnabled = true
        let openGallery = UITapGestureRecognizer(target: self, action: #selector(moveToGallery(_:)))
        viewGallery.addGestureRecognizer(openGallery)
        
        let dailyActivityLayout : UIView = UIView(frame:CGRectMake(2*bottomItemWidth,yy,bottomItemWidth,self.frame.size.height * 0.15))
      //  dailyActivityLayout.backgroundColor = UIColor().HexToColor(appConstants.orange);
        dailyActivityLayout.backgroundColor = UIColor.clearColor()
        self.addSubview(dailyActivityLayout)
        
        let dailyActivityLabel : UILabel = UILabel()
        dailyActivityLabel.frame = CGRectMake(0,26,bottomItemWidth,10 * appConstants.ScreenFactor)
        dailyActivityLabel.textAlignment = NSTextAlignment.Center
        dailyActivityLabel.text = "Recent" //changed from daily activity
        styleLabels(dailyActivityLabel)
        dailyActivityLayout.addSubview(dailyActivityLabel)
         dailyActivityLayout.backgroundColor=UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        dailyActivityLayout.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(moveToDailyActivity(_:)))
        dailyActivityLayout.addGestureRecognizer(tapRecognizer)
        
        let dailyActivityImage : UIImageView = UIImageView()
        dailyActivityImage.frame = CGRectMake(15,30,bottomItemWidth - 30 ,10 * appConstants.ScreenFactor)
      //  dailyActivityImage.image = UIImage.init(imageLiteral: "daily_horse_icon.png")
        dailyActivityLayout.addSubview(dailyActivityImage)
        
    }
    
    func styleLabels(label: UILabel){
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.init(name: "Helvetica", size: 7 * appConstants.ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
    }
    
    func setData() {
        nameData.text = horseData.objectForKey(appConstants.name) as? String
        ownerData.text = horseData.objectForKey(appConstants.owner) as? String
        breedData.text = horseData.objectForKey(appConstants.breed) as? String
        ageData.text = horseData.objectForKey(appConstants.age) as? String
       
        weightChangeData.text = horseData.objectForKey(appConstants.weight_change) as? String
        heightChangeData.text = horseData.objectForKey(appConstants.height_change) as? String
        colorChangeData.text = horseData.objectForKey(appConstants.colour_change) as? String
        
        let mediaSource:String = (horseData.objectForKey(appConstants.media_source) as? String)!
        
        var notificationDict: NSDictionary = NSDictionary()
        let media_type:String = (horseData.objectForKey(self.appConstants.media_type) as? String)!
        
        if media_type == "image"{
            //Image
            horseImage.downloadedFrom(media_base_url!+mediaSource)
            notificationDict = [appConstants.name: nameData.text!, appConstants.profice_pic: media_base_url!+mediaSource]
        }else{
            //Video
            horseImage.image = UIImage.init(imageLiteral: "playbtn_i_new.png")
           notificationDict = [appConstants.name: nameData.text!, appConstants.profice_pic: ""]
            let arr = mediaSource.characters.split{$0 == "/"}.map(String.init)
            if self.appConstants.isVideoSaved(arr[arr.count - 1]){
                //Play video by default
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0];
                let filePath="\(documentsPath)/\(arr[arr.count - 1])";
                self.appConstants.showVideoThumbnail(filePath, imageView: horseImage)
            }
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName(self.appConstants.kN_ShowHorseCredentailsInHeaderNotification, object: notificationDict)

    }
    
    func moveToDailyActivity(gestureRecognizer: UITapGestureRecognizer){
        if (horseData.objectForKey(appConstants._id) != nil) {
            self.subviews.forEach({ $0.removeFromSuperview() })
            AppConstants.CurrentScreen = appConstants.DailyActivityFromDetailScreen
            
            let horseDailyActivity:DailyActivityListing = DailyActivityListing(frame : self.frame)
            horseDailyActivity.fromWhere = "HORSE_DETAIL"
            horseDailyActivity.backgroundColor = UIColor.clearColor()
            horseDailyActivity.getHorseDailyActivityList(horseData.objectForKey(appConstants._id) as! String)
            self.addSubview(horseDailyActivity)
        }
    }
    
    func moveToGallery(gestureRecognizer: UITapGestureRecognizer){
        if (horseData.objectForKey(appConstants._id) != nil) {
            self.subviews.forEach({ $0.removeFromSuperview() })
            self.backgroundColor = UIColor.clearColor()
            AppConstants.CurrentScreen = appConstants.GalleryScreenFromHorseDetail

            let horseGalleryView:GalleryView = GalleryView(frame : self.frame)
            horseGalleryView.getGalleryDetail(horseData.objectForKey(appConstants._id) as! String)
            self.addSubview(horseGalleryView)
        }
    }
    
    func showFullScreenImageOrVideo(gestureRecognizer: UITapGestureRecognizer){
        if (horseData.objectForKey(appConstants._id) != nil) {
            //Image
            let media_type:String = (horseData.objectForKey(self.appConstants.media_type) as? String)!
            let mediaSource:String = (horseData.objectForKey(appConstants.media_source) as? String)!

            if media_type == "image"{
                //Image            
                let gallery = SwiftPhotoGallery(delegate: self, dataSource: self)
                
                gallery.backgroundColor = UIColor.blackColor()
                gallery.pageIndicatorTintColor = UIColor.clearColor()
                gallery.currentPageIndicatorTintColor = UIColor.clearColor()
                
                vc!.presentViewController(gallery, animated: false, completion: { () -> Void in
                    gallery.currentPage = 1
                })
               
            }else{
                //Video
                let viewController = PlayVideoViewController()
                viewController.moviePath = media_base_url! + mediaSource
                vc!.presentViewController(viewController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: SwiftPhotoGalleryDataSource Methods
    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {
        return 1
    }
    
    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {
        return horseImage.image
    }
    
    // MARK: SwiftPhotoGalleryDelegate Methods
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        vc!.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
}


//
//  ContactListing.swift
//  Demo
//
//  Created by KOMAL SHARMA on 18/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
//import Quickblox

class Notifications: UIView, UITableViewDelegate, UITableViewDataSource {
    var fromWhere: String = ""
    
    var tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    
    var cellIdendifier: String = "NotificationTVC"
    var baseUrl: String = String()
    var contactDict:NSDictionary=NSDictionary()
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    var sendMessageButton:UIButton?
    var horseID: String = ""
    var scrollView:UIScrollView?
    var media_base_url : String?
    var horseData : NSDictionary = NSDictionary()
    var indicator = UIActivityIndicatorView()
    var horseArray:NSArray = NSArray()
    var filtered:NSMutableArray = NSMutableArray()
    var horseList : NSDictionary = NSDictionary()
    var notifNameArray:NSArray = NSArray()
    var notifTypeArray:NSArray = NSArray()
    var NotificationContent:NSArray = NSArray()
    var ClearButton: UIButton = UIButton()
    
    var webService: WebService = WebService()
    
    
    let mainViewController: MainViewController = MainViewController()
    


    override init(frame: CGRect) {
        super.init(frame: frame)
        
       
        
        tableView.frame             =   self.frame;
        tableView.delegate          =   self
        tableView.dataSource        =   self
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.SingleLine;
        tableView.separatorColor    = UIColor.whiteColor().colorWithAlphaComponent(0.3);
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        self.addSubview(tableView)
       // notifNameArray = ["Notification 1","Notification 2","Notification 3","Notification 4"]
       // notifTypeArray = ["New Image","New Event","New Video","New Audio"]
        
    let userId = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)
      getNotifications(userId)
        
        print(AppConstants.CurrentScreen)
         AppConstants.CurrentScreen = appConstants.NotificationScreen
        print(AppConstants.CurrentScreen)

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func getNotifications(userID: String){
       showProgress()
              
        let postString = "action=mynotifications&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID);
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
               if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        print("mainList = \(mainList)")
                        
                        let totalNumber : NSString = mainList .valueForKey("total") as! NSString
                         self.NotificationContent  = mainList.valueForKey("notifications") as! NSArray
                        
                        print(totalNumber)
                        print(self.NotificationContent)
                        self.tableView.reloadData()
                    let userId = SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.memberID)
                        self.RemoveNotifications(userId)
                    }
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40 * appConstants.ScreenFactor    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = NotificationTVC(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
          cell.backgroundColor = UIColor.clearColor()
        
  
        let event = NotificationContent.valueForKey("ntype").objectAtIndex(indexPath.row)
        let number = NotificationContent.valueForKey("total").objectAtIndex(indexPath.row)
        
       // cell.notifLabel.text = NotificationContent[indexPath.row] as? String
        print(event)
        print(number)
        let notificationToDisplay : NSString = " \(event)            \(number) "
       cell.notifLabel.text = notificationToDisplay as String
        print(notificationToDisplay)
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NotificationContent.count
    }
    
    

    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    
    func RemoveNotifications(userID: String){
        showProgress()
        
        let postString = "action=clearnotifications&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID);
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        
                        
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject("0", forKey: "notificationBadge")
                        defaults.synchronize()
                        
                        self.mainViewController.drawUI() //pooja to update notification number

                       
                        
                    }
                }
            }
        }
    }


    
    
    
}

//
//  SideNavigationBarView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 06/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class SideNavigationBarView: UIView {
   
    var appConstants: AppConstants = AppConstants()
    var menuSelected: MenuSelectionCallback?
    var imageArray: [String] = ["ic_horse.png","ic_daily_activity.png","ic_message.png","ic_calender.png","ic_picture.png","ic_owner.png","ic_setting.png"]
    var itemCount:CGFloat = 7
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        addView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addView() {
        var yy:CGFloat = 0
        var counter:Int = 1
        let barItemheight:CGFloat = self.frame.size.height/itemCount
        
        for imageName in imageArray {
            let image: UIImageView = UIImageView.init(frame: CGRectMake(2,yy,self.frame.size.width * 0.4 ,self.frame.size.width * 0.4))
            image.center = CGPointMake(self.center.x, yy + barItemheight/2)
            image.image = UIImage.init(imageLiteral: imageName)
            self.addSubview(image)
            
            let bView: UIView = UIView.init(frame: CGRectMake(0, yy, self.frame.size.width, barItemheight))
            bView.backgroundColor = UIColor.clearColor()
            bView.userInteractionEnabled = true
            bView.tag = counter
            self.addSubview(bView)
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectedMenuItem(_:)))
            bView.addGestureRecognizer(tapRecognizer)
            
            counter+=1
            yy += barItemheight

            let line: UIView = UIView()
            line.frame = CGRectMake(0 , yy - 0.5 , self.frame.size.width, 1)
            line.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
            self.addSubview(line)
        }
    }
    
    func selectedMenuItem(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!
        menuSelected?.menuSelected(viewTouched.tag)
    }
}


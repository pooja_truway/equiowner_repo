//
//  MyTableViewCell.swift
//  Demo
//
//  Created by jyoti on 10/17/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class NotificationTVC: UITableViewCell {
       var notifLabel: UILabel = UILabel()
     var notiftypeLabel: UILabel = UILabel()

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor.clearColor()
       
      notifLabel.frame = CGRectMake(10,15, 150, 70)
        notifLabel.textColor = UIColor.whiteColor()
        // notifLabel.text = "notification 1"
        notifLabel.font = UIFont.init(name: "Helvetica", size: 18 )
        notifLabel.backgroundColor = UIColor.clearColor()
        contentView.addSubview(notifLabel)
        
        notiftypeLabel.frame = CGRectMake(10, 32, 80, 30)
        notiftypeLabel.textColor = UIColor.whiteColor()
        //notiftypeLabel.text = "Video"
        notiftypeLabel.font = UIFont.init(name: "Helvetica", size: 12 )
        notiftypeLabel.backgroundColor = UIColor.clearColor()
        contentView.addSubview(notiftypeLabel)

    
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func setData()  {
       
       }
    
    }

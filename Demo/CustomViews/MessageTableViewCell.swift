//
//  MessageTableViewCell.swift
//  Demo
//
//  Created by pooja on 12/04/17.
//  Copyright © 2017 BreakfastBay. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    var horseData: NSDictionary = NSDictionary()
    var horseName: UILabel = UILabel()
     var message: UILabel = UILabel()
    var appConstants: AppConstants = AppConstants()
    var horseSelected: ListItemCallback?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        
        horseName = UILabel.init(frame: CGRectMake(240, 25, 90, 40))
        horseName.textColor = UIColor.whiteColor()
        horseName.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        horseName.backgroundColor = UIColor.clearColor()
        contentView.addSubview(horseName)
        
        
        message = UILabel.init(frame: CGRectMake(9, 5, 150, 80))
        message.lineBreakMode = .ByWordWrapping
        message.numberOfLines = 0;
        message.textColor = UIColor.whiteColor()
        message.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
        message.backgroundColor = UIColor.clearColor()
       
        contentView.addSubview(message)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setData(model: NSDictionary)  {
        horseData = model
        print(horseData)
        
        
        let string1 = "--"
        
        if (horseData.valueForKey(appConstants.fromUser) as? String) != nil {
          let  appendString2 = string1+(horseData.valueForKey(appConstants.fromUser) as? String)!
             horseName.text = appendString2
        } else {
            print("sender is null")
        }
        
        
      //  let appendString2 = string1+(horseData.valueForKey(appConstants.fromUser) as? String)!
        
        message.text = horseData.valueForKey(appConstants.message) as? String
        
       // horseName.text = horseData.valueForKey(appConstants.fromUser) as? String
       // horseName.text = appendString2 as String
        contentView.addSubview(horseName)
        
    }
    
    func moveToDetail(gestureRecognizer: UITapGestureRecognizer){
        horseSelected!.itemSelected(horseData)
        print(horseData)
    }
}

////
//  ContactListing.swift
//  Demo
//
//  Created by KOMAL SHARMA on 18/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
//import Quickblox

class ChatOwnerListing: UIView, UITableViewDelegate, UITableViewDataSource {
    var fromWhere: String = ""
    
    var tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var contactList:NSArray = NSArray()
    var cellIdendifier: String = "OwnerTableCell"
    var baseUrl: String = String()
    var contactDict:NSDictionary=NSDictionary()
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    var sendMessageButton:UIButton?
    var horseID: String = ""
    var scrollView:UIScrollView?
    var media_base_url : String?
    var horseData : NSDictionary = NSDictionary()
    var indicator = UIActivityIndicatorView()
    var horseArray:NSArray = NSArray()
    var filtered:NSMutableArray = NSMutableArray()
    var horseList : NSDictionary = NSDictionary()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        scrollView = UIScrollView.init(frame: self.frame)
        scrollView!.contentSize = self.frame.size
        scrollView!.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        
        self.addSubview(scrollView!)
        
       // AppConstants.CurrentScreen = appConstants.OwnerScreenFromMessageHorseListing
        
        
         AppConstants.CurrentScreen = appConstants.MessagingScreen
        
        
        sendMessageButton = UIButton.init(frame: CGRectMake(0, self.frame.size.height - 30 * appConstants.ScreenFactor, self.frame.size.width , 30 * appConstants.ScreenFactor))
        sendMessageButton?.layer.cornerRadius = appConstants.CornerRadius
        sendMessageButton?.setTitle("SEND MESSAGE TO ALL", forState: UIControlState.Normal)
        sendMessageButton?.titleLabel?.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        sendMessageButton?.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        sendMessageButton?.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        sendMessageButton!.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(sendMessage(_:)))
        sendMessageButton!.addGestureRecognizer(tapRecognizer)
        scrollView!.addSubview(sendMessageButton!)
        
        tableView.frame             =   CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30 * appConstants.ScreenFactor)
        tableView.delegate          =   self
        tableView.dataSource        =   self
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.None;
        tableView.separatorColor    = UIColor.clearColor();
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        // self.addSubview(tableView)
        scrollView!.addSubview(tableView)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func sendMessage(gestureRecognizer: UITapGestureRecognizer) {
        self.subviews.forEach({ $0.removeFromSuperview() })

        contactDict = self.horseList
        AppConstants.CurrentScreen = appConstants.MessagingScreen
        
        print("contactDict = \(horseList)")

        
        let userDetail:ChatListing = ChatListing(frame : self.frame)
         userDetail.isFromOwnerPage = "otherpage"
        userDetail.backgroundColor = UIColor.clearColor()
        userDetail.contactChatDict = contactDict
        userDetail.getChatUserFromTrainer(contactDict)
       
        
        //chatLogin()
        //userDetail.getUserDetail(contactDict.objectForKey(appConstants.userId) as! String, media_base_url: baseUrl)
        //userDetail.getUserDetail(contactDict.objectForKey(appConstants.userId) as! String, media_base_url: baseUrl)
        self.addSubview(userDetail)
 
    }
    
    func getOwnerDetail(horseID: String){
        showProgress()
        
        //send to server
       // let postString = "action=horsedetail&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&horse_id="+horseID; //pooja
        print(AppConstants.CurrentScreen)
        
        let postString = "action=horseowners"+"&horse_id="+horseID;

        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    self.appConstants.showAlert("Oops!", message: "Unable to fetch data...", controller: self.vc!)
                    return
                }
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        self.appConstants.showAlert("Oops!", message: "Unable to fetch data...", controller: self.vc!)
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        
                        print("mainList = \(mainList)")
                        
                        self.horseList = (mainList as NSDictionary)

                   //     self.horseList = (mainList[self.appConstants.horses])! as! NSDictionary

                        
                        print("horseList = \(self.horseList)")

                        
                   //     self.horseArray = mainList.valueForKey(self.appConstants.horses) as! NSArray
                        
                        self.horseArray = mainList.valueForKey("owners") as! NSArray
                        self.filtered = self.horseArray.mutableCopy() as! NSMutableArray
                        
                        print("horseArray = \(self.horseArray.count)")
                        print("filtered = \(self.filtered.count)")

                        self.media_base_url = mainList.objectForKey(self.appConstants.media_base_url) as? String
                       // self.horseData = (mainList[self.appConstants.horses])! as! NSDictionary
                        self.tableView.reloadData()
                        
                    }
                }
            }
        }
    }
    
    func setData() {

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40 * appConstants.ScreenFactor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = OwnerTableCellView(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        if(self.horseList.count > 0){
           // let contactDict: NSDictionary = self.horseList
         //   cell.setData(contactDict,media_base_url: baseUrl)
            
            cell.setData(self.filtered[indexPath.row] as! NSDictionary,media_base_url: baseUrl)
        }
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return horseArray.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    

    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        self.subviews.forEach({ $0.removeFromSuperview() })
//        
//        print("You selected cell #\(indexPath.row)!")
//        
//    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }

    
    func chatLogin()
    {
        /*let user = QBUUser()
        user.ID = UInt(SharedPreferenceUtils.sharedInstance.getIntValue(self.appConstants.CHAT_USER_ID_SERVER))
        user.password = self.appConstants.passwordChat
        
        QBChat.instance().connectWithUser(user) { (error: NSError?) -> Void in
            
            print(error)
            
        }
        
        QBRequest.logInWithUserEmail(SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.CHAT_USER_ID), password: self.appConstants.passwordChat, successBlock: { (response, user) in
            
            print(response)
            }, errorBlock: { (error) in
                print(error)
        })
        */
        
    }
    
    
}

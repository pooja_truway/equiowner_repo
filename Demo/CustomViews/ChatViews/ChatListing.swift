//
//  DailyActivityListing.swift
//  Demo
//
//  Created by KOMAL SHARMA on 14/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import CoreData
//import Quickblox

class ChatListing: UIView, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate//, QBChatDelegate
{
    var fromWhere: String = ""
    var addButton: UIButton = UIButton()
    var addMessageTextField: UITextField = UITextField()
    
    var tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var messageList:NSMutableArray = NSMutableArray()
    var ownerIdList:NSMutableArray = NSMutableArray()
     var populatedDictionary:NSMutableDictionary = NSMutableDictionary()
    var cellIdendifier: String = "ChatCell"
    var horseID: String = ""
    var scrollView:UIScrollView?
    var padding:CGFloat=20
    var contactChatDict:NSDictionary=NSDictionary()
    var chatUser : String!
    var messageTime : String!
    let messageWebService: MessageSycWebService = MessageSycWebService()
    private let notificationManager = NotificationManager()
    var isServiceRunning: Bool = false
    var isFromOwnerPage: NSString = ""

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isServiceRunning = false
        addNotifications()
        NSNotificationCenter.defaultCenter().postNotificationName(self.appConstants.kN_StartTimerNotification, object: "")
        
        
        scrollView = UIScrollView.init(frame: self.frame)
        scrollView!.contentSize = self.frame.size
        scrollView!.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        
        self.addSubview(scrollView!)
        
        
        let bottomLayout =  UIView.init(frame: CGRectMake(0, self.frame.size.height - 30 * appConstants.ScreenFactor, self.frame.size.width , 30 * appConstants.ScreenFactor))
        bottomLayout.backgroundColor = UIColor.whiteColor()
        
        
        let userType = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.user_type)
        
      //  let animals = NSUserDefaults .standardUserDefaults () .objectForKey ( "user_type" )
        print( "User Type is : \( userType )" )
        
        if(userType == "2")
        {}
        else
        {
        scrollView!.addSubview(bottomLayout)
        }
        // self.addSubview(bottomLayout)
        
        let hline: UIView = UIView(frame : CGRectMake(0, self.frame.size.height - 30 * appConstants.ScreenFactor, self.frame.size.width, 1))
        hline.backgroundColor = UIColor.blackColor()
        //self.addSubview(hline)
        scrollView!.addSubview(hline)
        
        addButton = UIButton.init(frame: CGRectMake(bottomLayout.frame.size.width - 30 * appConstants.ScreenFactor ,0, 20 * appConstants.ScreenFactor,20 * appConstants.ScreenFactor))
        addButton.center = CGPointMake(addButton.center.x, bottomLayout.frame.size.height/2)
        addButton.setImage(UIImage.init(imageLiteral: "send_icon.png"), forState: UIControlState.Normal)
        addButton.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(addNewMessage(_:)))
        addButton.addGestureRecognizer(tapRecognizer)
        
        bottomLayout.addSubview(addButton)
        
        addMessageTextField = UITextField.init(frame: CGRectMake(10, 0, self.frame.size.width - addButton.frame.size.width - 20 * appConstants.ScreenFactor, 30 * appConstants.ScreenFactor))
        addMessageTextField.textColor = UIColor.blackColor()
        addMessageTextField.placeholder = "Message"
        addMessageTextField.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
        addMessageTextField.backgroundColor = UIColor.clearColor()
        bottomLayout.addSubview(addMessageTextField)
        
        
        tableView.frame             =   CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30 * appConstants.ScreenFactor)
        tableView.delegate          =   self
        tableView.dataSource        =   self
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.None;
        tableView.separatorColor    = UIColor.clearColor();
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        // self.addSubview(tableView)
        scrollView!.addSubview(tableView)
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.addGestureRecognizer(tap)
        
        print("send message screen \(AppConstants.CurrentScreen)")
        
        //QBChat.instance().addDelegate(self)
        
       
    }
    
//    
//    func checkScreen() {
//       
//        print(isFromOwnerPage)
//        if(isFromOwnerPage .isEqualToString("POOJA"))
//        {
//            AppConstants.CurrentScreen == appConstants.MessagingScreenFromOwnerListing
//            print(AppConstants.CurrentScreen)
//        }
//            
//        else{
//            AppConstants.CurrentScreen = appConstants.MessagingScreen
//            print( AppConstants.CurrentScreen)
//        }
//        
//
//       }

    
    func getChatUser(dict: NSDictionary) {
        
        contactChatDict = dict.objectForKey(appConstants.chat) as! NSDictionary
        print("dict = \(contactChatDict)")
        chatUser = (contactChatDict.objectForKey(appConstants.userName) as? String)!
        //chatUser = "19490140";
        
        
     //   getMessageList(chatUser);  //pooja to remov echat history page
    }
    
    
    func getChatUserFromTrainer(dict: NSDictionary) {
     //   contactChatDict = dict.objectForKey(appConstants.chat) as! NSDictionary
    //    print("dict = \(contactChatDict)")
       // chatUser = (dict.objectForKey(appConstants.owner_id) as? String)!
        print(dict)
       
       let  ownerList = dict.objectForKey("owners") as! NSArray
       let filtered = ownerList.mutableCopy() as! NSMutableArray
        
        print(filtered)
        
         for j in 0  ..< filtered.count  {
            
       // if(filtered.count > 0){
            
         chatUser = (dict.objectForKey("owners")! as AnyObject).valueForKey("userId")!.objectAtIndex(j) as? String
            
         populatedDictionary = ["chatUserList": chatUser]
         print(populatedDictionary)
            
        ownerIdList.insertObject(chatUser, atIndex: j)
        }
        
        print(chatUser)
        print(ownerIdList)
        chatUser = ownerIdList.componentsJoinedByString(",")
        print(chatUser)

       
      //  getMessageList(chatUser);  //pooja to remove chat screen
    }

    
    func addNewMessage(gestureRecognizer: UITapGestureRecognizer){
        if(!addMessageTextField.text!.isEmpty) {
            let time: NSTimeInterval = NSDate().timeIntervalSince1970 * 1000
            messageTime = String(NSInteger(time))
            addMessageToAdapter()
            addMessageTextField.text=""
        }
    }
    
    
    
    /*func chatDidReceiveMessage(message: QBChatMessage)
     {
     
     
     let messageDict: NSMutableDictionary = NSMutableDictionary()
     
     messageDict.setValue(message.text, forKey: appConstants.CHAT_MESSAGE_TEXT)
     messageDict.setValue(2, forKey: appConstants.MESSAGE_Direction)
     messageDict.setValue(String(NSDate().timeIntervalSince1970 * 1000), forKey: appConstants.MESSAGE_TIME)
     messageDict.setValue(message.senderID, forKey: appConstants.MESSAGE_OPPID)
     messageList.addObject(messageDict)
     tableView.reloadData()
     
     scrollListToLastPosition()
     
     saveInDatabase(messageDict)
     
     }*/
    
    func addMessageToAdapter()
    {
        
        let messageDict: NSMutableDictionary = NSMutableDictionary()
        
        messageDict.setValue(addMessageTextField.text, forKey: appConstants.CHAT_MESSAGE_TEXT)
        messageDict.setValue(1, forKey: appConstants.MESSAGE_Direction)
        messageDict.setValue(messageTime
            , forKey: appConstants.MESSAGE_TIME)
        messageDict.setValue(ownerIdList, forKey: appConstants.MESSAGE_OPPID)
        messageDict.setValue(appConstants.randomStringWithLength(32), forKey: appConstants.MESSAGE_UID)
        
        print(messageDict)
        
        messageList.addObject(messageDict)
        tableView.reloadData()
        
        scrollListToLastPosition()
        saveInDatabase(messageDict)
        
    }
    
    func scrollListToLastPosition()
    {
        self.tableView.setContentOffset(CGPointMake(0, (self.tableView.contentSize.height - self.tableView.frame.size.height)), animated: true)
        /*if(self.tableView.numberOfSections>2)
         {
         let lastSectionIndex = self.tableView.numberOfSections - 1
         
         let lastRowIndex = self.tableView.numberOfRowsInSection(lastSectionIndex) - 1
         
         let pathToLastRow = NSIndexPath(forRow: lastRowIndex, inSection: lastSectionIndex)
         
         // Make the last row visible
         self.tableView.scrollToRowAtIndexPath(pathToLastRow, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
         }*/
        dismissKeyboard()
        
    }
    
    func saveInDatabase(messageDict : NSMutableDictionary)
    {
        
        let entityDescription =
            NSEntityDescription.entityForName("MESSAGETABLE",inManagedObjectContext: appConstants.getManagedObjectContext())
        
        let contact = MESSAGETABLE(entity: entityDescription!,insertIntoManagedObjectContext: appConstants.getManagedObjectContext())
        
        contact.message_content =  messageDict.objectForKey(appConstants.CHAT_MESSAGE_TEXT) as? String
        contact.message_direction = messageDict.objectForKey(appConstants.MESSAGE_Direction) as? NSNumber
        contact.message_number = messageDict.objectForKey(appConstants.MESSAGE_OPPID) as? String
        contact.message_time = messageDict.objectForKey(appConstants.MESSAGE_TIME) as? String
        contact.message_uid = messageDict.objectForKey(appConstants.MESSAGE_UID) as? String
        
        var error: NSError?
        
        do
        {
            try  appConstants.getManagedObjectContext().save()
        }
        catch{
            
        }
        
        if let err = error {
            print(err.localizedFailureReason)
            //status.text = err.localizedFailureReason
        } else {
            //addMessageTextField.text = ""
            print("Contact Saved")
        }
        if(contact.message_direction==1)
        {
            sendMessageToServer(messageDict)
        }
        
    }
    
    
    func sendMessageToServer(messageDict : NSMutableDictionary)
    {
        print(messageDict)
        
         for i in 0 ..< ownerIdList.count
         {
        let json = NSMutableDictionary()
      //  json.setValue(messageDict.objectForKey(appConstants.MESSAGE_UID), forKey: "messageUid") //pooja - 4-9
       json.setValue(messageDict.objectForKey(appConstants.CHAT_MESSAGE_TEXT), forKey: "message")
        
      json.setValue(ownerIdList.objectAtIndex(i), forKey: "toUser")
    //json.setValue(messageDict.objectForKey(appConstants.MESSAGE_OPPID), forKey: "toUser")
    //    json.setValue(SharedPreferenceUtils._sharedInstance.getStringValue(appConstants.CHAT_USER_ID), forKey: "fromUser") //pooja - to add memberId,ie 69 in place of 29 to key "fromUser"
        
      json.setValue(SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID), forKey: "fromUser")
            
            json.setValue("sendMessage", forKey: "action")
        
        print(SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID))
      //  json.setValue(messageDict.objectForKey(appConstants.MESSAGE_TIME), forKey: "messageTime") //pooja-4-9
        
        print("json to send message = \(json)")

        let data = self.appConstants.convertDictionaryToString(json)
        
        print (data)
       
    let messageStr : NSString = (messageDict.objectForKey(appConstants.CHAT_MESSAGE_TEXT) as? String)!
            
            print("message str : \(messageStr)")
            
    let postString = "action=sendMessage&fromUser="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&toUser="+(ownerIdList.objectAtIndex(i) as! String)+"&message="+(messageDict.objectForKey(appConstants.CHAT_MESSAGE_TEXT) as? String)!;
            
        print("message text : \(appConstants.CHAT_MESSAGE_TEXT)")

        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
           // dispatch_async(dispatch_get_main_queue()) { [unowned self] in //pooja
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in

                if error != nil{
                    return
                }
                
                //let data = self.appConstants.convertStringToDictionary(response)! as NSDictionary
                print("response send Message : = \(response)")
                // print("response = \(data)")
                
            })
        }
        }
        
        /* if(QBChat.instance().isConnected)
         {
         let message: QBChatMessage = QBChatMessage()
         message.recipientID = UInt((messageDict.objectForKey(appConstants.MESSAGE_OPPID) as? String)!)!
         message.text = messageDict.objectForKey(appConstants.CHAT_MESSAGE_TEXT) as? String
         
         let params : NSMutableDictionary = NSMutableDictionary()
         //params["save_to_history"] = true
         params["send_to_chat"] = "1"
         message.customParameters = params
         
         QBRequest.createMessage(message, successBlock: {(response: QBResponse!, createdMessage: QBChatMessage!) in
         NSLog("success: %@", createdMessage)
         }, errorBlock: {(response: QBResponse!) in
         
         })
         
         
         }
         else{
         print("not connected to server")
         }*/
        
    }
    
    
    
    func getMessageList(chatUserId : String)
    {
        let entityDescription =
            NSEntityDescription.entityForName("MESSAGETABLE",inManagedObjectContext: appConstants.getManagedObjectContext())
        
        if(entityDescription != nil){
            let request = NSFetchRequest()
            request.entity = entityDescription
            let sortDescriptor = NSSortDescriptor(key: "message_time", ascending: true)
            let sortDescriptors = [sortDescriptor]
            request.sortDescriptors = sortDescriptors
            
            let pred = NSPredicate(format: "(message_number = %@)", chatUserId)
            request.predicate = pred
            
            do{
                var objects =  try appConstants.getManagedObjectContext().executeFetchRequest(request)
                
                if objects.count > 0 {
                    var i:Int = 0
                    while(i < objects.count)
                    {
                        let match = objects[i] as! NSManagedObject
                        let messageDict: NSMutableDictionary = NSMutableDictionary()
                        
                        messageDict.setValue(match.valueForKey("message_content") as! String, forKey: appConstants.CHAT_MESSAGE_TEXT)
                        messageDict.setValue(match.valueForKey("message_number") as! String, forKey: appConstants.MESSAGE_OPPID)
                        messageDict.setValue(match.valueForKey("message_time") as! String, forKey: appConstants.MESSAGE_TIME)
                        messageDict.setValue(match.valueForKey("message_direction") as! NSNumber, forKey: appConstants.MESSAGE_Direction)
                        messageDict.setValue(match.valueForKey("message_uid") as! String, forKey: appConstants.MESSAGE_UID)
                        
                        messageList.addObject(messageDict)
                        tableView.reloadData()
                        print(match.valueForKey("message_content") as! String)
                        i=i+1
                    }
                    
                } else {
                    print("No match")
                    //status.text = "No Match"
                }
            }
            catch{
                
            }
            
            scrollListToLastPosition()
        }
        
        do{
          //  try getMessageFromServerOnTheBasisOfLastUID()
            print("get msg list from server called here")
        } catch {
            
        }
        
    }
    
    func getMessageFromServerOnTheBasisOfLastUID() throws{
        //get the last packet ID
        if(!isServiceRunning){
            isServiceRunning = true
            
            var lastMessagePacketIDTosend = ""
            if(self.messageList.count > 0){
                let data = self.messageList.lastObject as! NSDictionary
                lastMessagePacketIDTosend = data.objectForKey(appConstants.MESSAGE_UID) as! String
            }
            
            
            let json = NSMutableDictionary()
            json.setValue(lastMessagePacketIDTosend, forKey: "lastPId")
            json.setValue(chatUser, forKey: "from")
            json.setValue(SharedPreferenceUtils._sharedInstance.getStringValue(appConstants.CHAT_USER_ID), forKey: "to")
            json.setValue("sendMessage", forKey: "action")
            
            let data = self.appConstants.convertDictionaryToString(json)
            webService.HTTPPostJSON(appConstants.LOGIN, data: data!) { (response, error) -> Void in
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    self.isServiceRunning = false
                    if error != nil{
                        return
                    }
                    
                    //let data = self.appConstants.convertStringToDictionary(response)! as NSDictionary
                    print("response = \(response)")
                    // print("response = \(data)")
                    
                    if(!response.isEmpty){
                        let data = self.appConstants.convertStringToDictionary(response)! as NSDictionary
                        let error: NSNumber = data.objectForKey("error") as! NSNumber
                        if(error == 1000){
                            let arr = data.objectForKey("messages") as! NSArray
                            if arr.count > 0 {
                                var i:Int = 0
                                while(i < arr.count)
                                {
                                    let dict = arr[i] as! NSDictionary
                                    
                                    let messageDict: NSMutableDictionary = NSMutableDictionary()
                                    
                                    messageDict.setValue(dict.valueForKey("message") as! String, forKey: self.appConstants.CHAT_MESSAGE_TEXT)
                                    messageDict.setValue(2, forKey: self.appConstants.MESSAGE_Direction)
                                    messageDict.setValue(dict.valueForKey("messageTime"), forKey: self.appConstants.MESSAGE_TIME)
                                    messageDict.setValue(dict.valueForKey("fromUser"), forKey: self.appConstants.MESSAGE_OPPID)
                                    messageDict.setValue(dict.valueForKey("messageUid"), forKey: self.appConstants.MESSAGE_UID)
                                    
                                    self.messageList.addObject(messageDict)
                                    self.tableView.reloadData()
                                    
                                    self.saveInDatabase(messageDict)
                                    i=i+1
                                }
                                self.scrollListToLastPosition()
                            }
                        }
                    }
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatListing.keyboardWillShow(_:)), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatListing.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: nil);
        notificationManager.registerObserver(appConstants.kN_FetchTimerNotification) { note in
            do{
                //try self.getMessageFromServerOnTheBasisOfLastUID()
            } catch {
                
            }
        }
    }
    func keyboardWillShow(sender: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        scrollView!.scrollEnabled = true
        let info : NSDictionary = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        scrollView!.contentInset = contentInsets
        scrollView!.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.frame
        aRect.size.height -= keyboardSize!.height
        
        //        var aRect : CGRect = self.frame
        //        aRect.size.height -= keyboardSize!.height
        
        if (!CGRectContainsPoint(aRect, addMessageTextField.frame.origin))
        {
            scrollView!.scrollRectToVisible(addMessageTextField.frame, animated: true)
        }
        
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let info : NSDictionary = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        scrollView!.contentInset = UIEdgeInsetsZero
        scrollView!.scrollIndicatorInsets = contentInsets
        self.endEditing(true)
        scrollView!.scrollEnabled = false
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let dict: NSMutableDictionary = self.messageList[indexPath.row] as! NSMutableDictionary
        let  cellSize:CGSize = heightForCell(withMessage: (dict.objectForKey(appConstants.CHAT_MESSAGE_TEXT) as? String)!, withTime: "20/11/12", is1To1Chat: true)
        return cellSize.height;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = MyTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        
        cell.setData(self.messageList[indexPath.row] as! NSMutableDictionary)
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageList.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
    }
    
    func itemSelected(model: NSDictionary) {
        self.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    
    //TextField Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        // called when 'return' key pressed. return NO to ignore.
        self.endEditing(true)
        return true;
    }
    
    func textFieldDidBeginEditing(textField: UITextField){
        
    }
    
    func textFieldDidEndEditing(textField: UITextField){
    }
    
    func heightForCell(withMessage message: String, withTime: String, is1To1Chat: Bool) -> CGSize {
        var textBoxSize1: CGSize = CGSize()
        
        self.appConstants.getCurrentDate()
        
        /* var dateFormatter1 = NSDateFormatter()
         var time1 = dateFormatter1.string(fromDate: withTime)*/
        var date1: String = ""
        date1 = self.appConstants.getCurrentDate()
        let messageFont = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        
        
        let attributedString = NSMutableAttributedString(string: message)
        let spacing: Float = 0.0
        attributedString.addAttribute(NSKernAttributeName, value: (spacing), range: NSRange(location: 0, length: message.characters.count))
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5.0
        style.paragraphSpacingBefore = 5.0
        
        attributedString.addAttributes([NSParagraphStyleAttributeName: style], range: NSRange(location: 0, length: message.characters.count))
        var frame = CGRect.zero
        let textSize: CGSize = CGSizeMake(self.frame.size.width - 80, 100000.0)
        let options: NSStringDrawingOptions = [.UsesLineFragmentOrigin, .UsesFontLeading]
        
        frame = attributedString.boundingRectWithSize(textSize, options: options, context: nil)
        //frame = attributedString.boundingRect(withSize: textSize, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        
        // var dateSize = date1.size(withAttributes: [NSFontAttributeName: messageFont])
        frame.size.height += (15 + 30)
        textBoxSize1 = CGSize(width: frame.size.width, height: frame.size.height)
        return textBoxSize1
    }
    
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
}



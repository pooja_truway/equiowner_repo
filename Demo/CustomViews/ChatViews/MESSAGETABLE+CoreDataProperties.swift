//
//  MESSAGETABLE+CoreDataProperties.swift
//  Demo
//
//  Created by jyoti on 10/14/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MESSAGETABLE {

    @NSManaged var message_content: String?
    @NSManaged var message_direction: NSNumber?
    @NSManaged var message_number: String?
    @NSManaged var message_paket_id: String?
    @NSManaged var message_sent_status: NSNumber?
    @NSManaged var message_time: String?
    @NSManaged var message_type: NSNumber?
    @NSManaged var message_uid: String?


}

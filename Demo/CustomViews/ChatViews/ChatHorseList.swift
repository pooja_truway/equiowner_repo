//
//  ChatHorseList.swift
//  Demo
//
//  Created by New User on 1/20/17.
//  Copyright © 2017 BreakfastBay. All rights reserved.
//

import UIKit

class ChatHorseList: UIView, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate,ListItemCallback {
    var fromWhere: String = ""
    
    var tableView: UITableView  =   UITableView()
    var searchBar: UISearchBar  =   UISearchBar()
    
    var MessageHistoryButton : UIButton = UIButton()
    
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var horseList:NSArray = NSArray()
    var cellIdendifier: String = "HorseCell"
    var filtered:NSMutableArray = NSMutableArray()
    var indicator = UIActivityIndicatorView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        searchBar.frame             =   CGRectMake(0, 0, self.frame.size.width, 35 * appConstants.ScreenFactor)
        searchBar.showsCancelButton = false
        searchBar.backgroundColor = UIColor.clearColor()
        searchBar.tintColor = UIColor.blackColor()
        searchBar.translucent = true
        searchBar.alpha = 1
        searchBar.backgroundImage = UIImage()
        searchBar.barTintColor = UIColor.clearColor()
        
        self.addSubview(searchBar)
        
        
        print(AppConstants.CurrentScreen)
        
        AppConstants.CurrentScreen = appConstants.MessagingContactListScreen
        
        MessageHistoryButton.frame = CGRectMake(0,self.frame.size.height - 25 * appConstants.ScreenFactor,self.frame.size.width,35)
        MessageHistoryButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        
        MessageHistoryButton.layer.cornerRadius = appConstants.CornerRadius
        MessageHistoryButton.setTitle("Get Message History", forState: UIControlState.Normal)
        MessageHistoryButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        MessageHistoryButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        MessageHistoryButton.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(getHistory(_:)))
        MessageHistoryButton.addGestureRecognizer(tapRecognizer)
        self.addSubview(MessageHistoryButton)
        
        
        
        
        
        
        tableView.frame             =   CGRectMake(0,35 * appConstants.ScreenFactor , self.frame.size.width,self.frame.size.height - 70 * appConstants.ScreenFactor)
        tableView.delegate          =   self
        tableView.dataSource        =   self
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.SingleLine;
        tableView.separatorColor    = UIColor.whiteColor().colorWithAlphaComponent(0.3);
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        self.addSubview(tableView)
        searchBar.delegate = self
        
        getHorseListFromServer()
    }
    
    func dismissKeyboard() {
        self.endEditing(true)
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func getHistory(gestureRecognizer: UITapGestureRecognizer) {
        
self.subviews.forEach({ $0.removeFromSuperview() })
        
        let trainerMessgagelist :TrainerMessageFragment=TrainerMessageFragment(frame: CGRectMake(0 , 0 , self.frame.size.width, self.frame.size.height * 0.8 ))
        self.addSubview(trainerMessgagelist)
    }
    
    func getHorseListFromServer() {
        //send to server
        showProgress()
        let postString = "action=horses&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID);
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        self.horseList = mainList.objectForKey(self.appConstants.horses) as! NSArray
                        self.filtered = self.horseList.mutableCopy() as! NSMutableArray
                        
                        print("horseList = \(self.horseList)")
                        let count: String = String(self.horseList.count)
                        SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.horse_count, value: count)
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 35 * appConstants.ScreenFactor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = HorseTableCellView(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        
        if(self.filtered.count > 0){
            cell.horseSelected = self
            cell.setData(self.filtered[indexPath.row] as! NSDictionary)
        }
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        dismissKeyboard()
    }
    
    
    func itemSelected(model: NSDictionary) {
        self.subviews.forEach({ $0.removeFromSuperview() })
        AppConstants.CurrentHorseID = model.objectForKey(appConstants._id) as! String
        dismissKeyboard()
        
//        AppConstants.CurrentScreen = appConstants.HorseDetailingScreen
       // AppConstants.CurrentScreen = appConstants.OwnerListingScreen//pooja
        AppConstants.CurrentScreen = appConstants.MessagingContactListScreen

        let ownerDetail:ChatOwnerListing = ChatOwnerListing(frame : self.frame)
        ownerDetail.backgroundColor = UIColor.clearColor()
        ownerDetail.getOwnerDetail(model.objectForKey(appConstants._id) as! String)
        self.addSubview(ownerDetail)

    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        dismissKeyboard()
    }
    
    //Search bar delegate methods
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        filtered.removeAllObjects()
        if searchText.isEmpty {
            filtered = self.horseList.mutableCopy() as! NSMutableArray
        }else {
            for dict in self.horseList {
                let tmp: NSString = dict.objectForKey(self.appConstants.name) as! String
                let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
                if range.location != NSNotFound {
                    //add in filter array
                    filtered.addObject(dict)
                }
            }
        }
        self.tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarBookmarkButtonClicked(searchBar: UISearchBar){
        //Tells the delegate that the bookmark button was tapped.
        
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar){
        //Tells the delegate that the cancel button was tapped.
        dismissKeyboard()
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        //Tells the delegate that the search button was tapped.
        dismissKeyboard()
    }
    func searchBarResultsListButtonClicked(searchBar: UISearchBar){
        //Tells the delegate that the search results list button was tapped.
        dismissKeyboard()
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
}

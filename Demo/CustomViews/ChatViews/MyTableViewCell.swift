//
//  MyTableViewCell.swift
//  Demo
//
//  Created by jyoti on 10/17/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {
    var textBoxSize: CGSize = CGSize()
    
    var mainView: UIView!
    var myLabelContent: UITextView!
    var myLabelDate: UITextView!
    var myLabelTime: UITextView!
    var sender:NSString!
    var direction : NSNumber!
    //    var initialY:Int;
    
    var appConstants: AppConstants = AppConstants()
    var chatData: NSMutableDictionary = NSMutableDictionary()
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor.clearColor()
        mainView = UIView()
        contentView.addSubview(mainView)
        
        
        myLabelContent = UITextView()
        myLabelContent.textColor = UIColor.whiteColor()
        myLabelContent.backgroundColor = UIColor.clearColor()
        mainView.addSubview(myLabelContent)
        
        
        
        myLabelDate = UITextView()
        myLabelDate.textColor = UIColor.whiteColor()
        myLabelDate.backgroundColor = UIColor.clearColor()
        mainView.addSubview(myLabelDate)
        
        myLabelTime = UITextView()
        
        myLabelTime.textColor = UIColor.whiteColor()
        myLabelTime.backgroundColor = UIColor.clearColor()
        
        mainView.addSubview(myLabelTime)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func setData(model: NSMutableDictionary)  {
        chatData = model
        myLabelContent.text = chatData.objectForKey(appConstants.CHAT_MESSAGE_TEXT) as? String
        
        direction = chatData.objectForKey(appConstants.MESSAGE_Direction) as? NSNumber
        
        let messageDateString : String = (chatData.objectForKey(appConstants.MESSAGE_TIME) as? String)!
        if(messageDateString.characters.count>0)
        {
            myLabelDate.text = appConstants.getMessageDate(messageDateString)
            myLabelTime.text = appConstants.getMessageTime(messageDateString)
        }
        
        
        
        let msg:String = myLabelContent.text!
        //
        var size: CGSize = UIFont().sizeOfString(msg, constrainedToWidth: 260)
        size.width += (20/2);
        
        myLabelContent.text = msg;
        mainView.layer.cornerRadius=5
        
        
        var  cellSize:CGSize = heightForCell(withMessage: msg, withTime: "20/11/12", is1To1Chat: true)
        
        let varItems : CGFloat = 50
        
        if(cellSize.width < 40*appConstants.ScreenWidthFactor)
        {
            textBoxSize = CGSizeMake(cellSize.width + 115*appConstants.ScreenWidthFactor + varItems, cellSize.height);
        }
        else if(cellSize.width > 40*appConstants.ScreenWidthFactor && cellSize.width < 80*appConstants.ScreenWidthFactor)
        {
            textBoxSize = CGSizeMake(cellSize.width + 90*appConstants.ScreenWidthFactor + varItems, cellSize.height);
        }
        else if (cellSize.width > 80*appConstants.ScreenWidthFactor && cellSize.width < 120*appConstants.ScreenWidthFactor )
        {
            textBoxSize = CGSizeMake(cellSize.width + 70*appConstants.ScreenWidthFactor + varItems , cellSize.height);
        }
        else if (cellSize.width > 120*appConstants.ScreenWidthFactor && cellSize.width < 150*appConstants.ScreenWidthFactor )
        {
            textBoxSize = CGSizeMake(cellSize.width + 30*appConstants.ScreenWidthFactor + varItems, cellSize.height);
        }
            
        else
        {
            
            // NSLog(@"messageTextView.frame.size.height  %f", cellSize.height);
            textBoxSize = CGSizeMake(contentView.frame.width - 50*appConstants.ScreenWidthFactor + varItems, cellSize.height);
        }
        
        let font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        
        var frame = CGRect(x: 0, y: 0, width: cellSize.width, height: cellSize.height - 20*appConstants.ScreenFactor)
        myLabelContent.textContainerInset = UIEdgeInsetsMake(4, 0, 0, 0)
        
        
        if (direction == 2) {
            
            //contentView.backgroundColor = UIColor().HexToColor(appConstants.reciever_bubble_color, alpha: 1.0)
            
            mainView.backgroundColor = UIColor().HexToColor(appConstants.reciever_bubble_color, alpha: 1.0)
            
            mainView.frame = CGRect(x: 3*appConstants.ScreenWidthFactor, y: 0, width: textBoxSize.width, height: textBoxSize.height+10*appConstants.ScreenFactor)
            
            mainView.layer.cornerRadius = 5
            mainView.clipsToBounds = true
            myLabelContent.frame = CGRect(x: 3*appConstants.ScreenFactor, y: self.mainView.frame.origin.y + 2*appConstants.ScreenFactor, width: mainView.frame.size.width-3*appConstants.ScreenFactor , height: frame.size.height)
            myLabelContent.sizeToFit()
            myLabelDate.frame = CGRect(x: 3*appConstants.ScreenFactor, y: myLabelContent.frame.size.height + 1*appConstants.ScreenFactor, width: mainView.frame.width, height: 15*appConstants.ScreenFactor)
            myLabelDate.sizeToFit()
            
            myLabelTime.frame = CGRect(x: mainView.frame.width-37*appConstants.ScreenFactor, y: myLabelContent.frame.size.height + 1*appConstants.ScreenFactor, width: mainView.frame.width-5, height: 15*appConstants.ScreenFactor)
            myLabelTime.sizeToFit()
            
            
            
        }
        else
        {
            //contentView.backgroundColor = UIColor().HexToColor(appConstants.sender_bubble_color, alpha: 1.0)
            mainView.backgroundColor = UIColor().HexToColor(appConstants.sender_bubble_color, alpha: 1.0)
            self.mainView.frame = CGRect(x: varItems + contentView.frame.size.width - textBoxSize.width - 40*appConstants.ScreenFactor, y: 0, width: textBoxSize.width+3*appConstants.ScreenFactor, height: textBoxSize.height + 10*appConstants.ScreenFactor)
            self.mainView.layer.cornerRadius = 5
            self.mainView.clipsToBounds = true
            
            myLabelContent.frame = CGRect(x: 3*appConstants.ScreenFactor, y:  self.mainView.frame.origin.y + 2*appConstants.ScreenFactor, width: self.mainView.frame.size.width - 3*appConstants.ScreenFactor, height: frame.size.height)
            myLabelContent.sizeToFit()
            
            self.myLabelDate.frame = CGRect(x: 3*appConstants.ScreenFactor, y: myLabelContent.frame.size.height + 1*appConstants.ScreenFactor, width: mainView.frame.width, height: 15*appConstants.ScreenFactor)
            self.myLabelDate.sizeToFit()
            print(myLabelContent.frame.size.height + 1*appConstants.ScreenFactor)
            print(15*appConstants.ScreenFactor)
            
            
            myLabelTime.frame = CGRect(x: mainView.frame.width-37*appConstants.ScreenFactor, y: myLabelContent.frame.size.height + 1*appConstants.ScreenFactor, width: mainView.frame.width-5*appConstants.ScreenFactor, height: 15*appConstants.ScreenFactor)
            myLabelTime.sizeToFit()
            
            
            
            
        }
        
        //myLabelDate.backgroundColor = UIColor.blueColor()
        //myLabelContent.backgroundColor = UIColor.yellowColor()
        //mainView.backgroundColor = UIColor.brownColor()
    }
    // The output below is limited by 1 KB.
    // Please Sign Up (Free!) to remove this limitation.
    
    
    func heightForCell(withMessage message: String, withTime: String, is1To1Chat: Bool) -> CGSize {
        var textBoxSize1: CGSize = CGSize()
        
        self.appConstants.getCurrentDate()
        
        /* var dateFormatter1 = NSDateFormatter()
         var time1 = dateFormatter1.string(fromDate: withTime)*/
        var date1 : String = ""
        date1 = self.appConstants.getCurrentDate()
        let messageFont = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        
        
        let attributedString = NSMutableAttributedString(string: message)
        let spacing: Float = 0.0
        attributedString.addAttribute(NSKernAttributeName, value: (spacing), range: NSRange(location: 0, length: message.characters.count))
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5.0
        style.paragraphSpacingBefore = 5.0
        
        attributedString.addAttributes([NSParagraphStyleAttributeName: style], range: NSRange(location: 0, length: message.characters.count))
        var frame = CGRect.zero
        let textSize: CGSize = CGSizeMake(self.frame.size.width - 80, 100000.0)
        let options: NSStringDrawingOptions = [.UsesLineFragmentOrigin, .UsesFontLeading]
        
        frame = attributedString.boundingRectWithSize(textSize, options: options, context: nil)
        //frame = attributedString.boundingRect(withSize: textSize, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        
        // var dateSize = date1.size(withAttributes: [NSFontAttributeName: messageFont])
        frame.size.height += (15)
        textBoxSize1 = CGSize(width: frame.size.width, height : frame.size.height)
        return textBoxSize1
    }
}

//
//  ContactListing.swift
//  Demo
//
//  Created by KOMAL SHARMA on 18/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
//import Quickblox

class ChatContactListing: UIView, UITableViewDelegate, UITableViewDataSource {
    var fromWhere: String = ""
    
    var tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var contactList:NSArray = NSArray()
    var cellIdendifier: String = "ContactCell"
    var baseUrl: String = String()
    var contactDict:NSDictionary=NSDictionary()
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableView.frame             =   self.frame;
        tableView.delegate          =   self
        tableView.dataSource        =   self
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.SingleLine;
        tableView.separatorColor    = UIColor.whiteColor().colorWithAlphaComponent(0.3);
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        self.addSubview(tableView)
        getContactListFromServer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getContactListFromServer() {
        //send to server
        let postString = "action=contacts&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID);
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        self.baseUrl = mainList.objectForKey(self.appConstants.media_base_url) as! String
                       /* let current_id_chat_main : String = mainList.objectForKey(self.appConstants.current_id_chat) as! String
                        let  current_id_chat :String = SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.CHAT_CURRENT_ID)
                        
                        if current_id_chat_main == current_id_chat
                        {*/
                            self.contactList = mainList.objectForKey(self.appConstants.contacts) as! NSArray
                            print("contactList = \(self.contactList)")
                            self.tableView.reloadData()
                            
                       /* }
                        else
                        {
                            self.appConstants.showAlert("Whoa!", message: "You are already registered to another place.LOGIN Again",controller: self.vc!)
                            
                            
                        }*/
                        
                    }
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40 * appConstants.ScreenFactor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = ContactTableCellView(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        if(self.contactList.count > 0){
            let contactDict: NSDictionary = self.contactList[indexPath.row] as! NSDictionary
            cell.setData(contactDict,media_base_url: baseUrl)
        }
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactList.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.subviews.forEach({ $0.removeFromSuperview() })
        
        contactDict = self.contactList[indexPath.row] as! NSDictionary
        AppConstants.CurrentScreen = appConstants.MessagingScreen

        let userDetail:ChatListing = ChatListing(frame : self.frame)
        userDetail.backgroundColor = UIColor.clearColor()
        
        print("contactDict = \(contactDict)")

        userDetail.contactChatDict = contactDict
        userDetail.getChatUser(contactDict)
        //chatLogin()
        //userDetail.getUserDetail(contactDict.objectForKey(appConstants.userId) as! String, media_base_url: baseUrl)
        //userDetail.getUserDetail(contactDict.objectForKey(appConstants.userId) as! String, media_base_url: baseUrl)
        self.addSubview(userDetail)
        
    }
    
    func chatLogin()
    {
        /*let user = QBUUser()
        user.ID = UInt(SharedPreferenceUtils.sharedInstance.getIntValue(self.appConstants.CHAT_USER_ID_SERVER))
        user.password = self.appConstants.passwordChat
        
        QBChat.instance().connectWithUser(user) { (error: NSError?) -> Void in
            
            print(error)
            
        }
        
        QBRequest.logInWithUserEmail(SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.CHAT_USER_ID), password: self.appConstants.passwordChat, successBlock: { (response, user) in
            
            print(response)
            }, errorBlock: { (error) in
                print(error)
        })
        */
        
    }
    
    
}

//
//  HorseTableCellView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 06/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation
import UIKit

class HorseTableCellView: UITableViewCell {
    var horseData: NSDictionary = NSDictionary()
    var viewButton: UIButton = UIButton()
    var horseName: UILabel = UILabel()
    var appConstants: AppConstants = AppConstants()
    var horseSelected: ListItemCallback?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        viewButton = UIButton.init(frame: CGRectZero)
        viewButton.layer.cornerRadius = appConstants.CornerRadius
        viewButton.setTitle("View", forState: UIControlState.Normal)
        viewButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
        viewButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        viewButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        viewButton.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(moveToDetail(_:)))
        viewButton.addGestureRecognizer(tapRecognizer)
    
        contentView.addSubview(viewButton)
        
        horseName = UILabel.init(frame: CGRectZero)
        horseName.textColor = UIColor.whiteColor()
        horseName.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
        horseName.backgroundColor = UIColor.clearColor()
        contentView.addSubview(horseName)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        viewButton.frame = CGRectMake(frame.size.width - 57 * appConstants.ScreenFactor ,0, 50 * appConstants.ScreenFactor,20 * appConstants.ScreenFactor)
        viewButton.center = CGPointMake(viewButton.center.x, self.bounds.size.height/2)

        horseName.frame = CGRectMake(10, 5, frame.size.width - viewButton.frame.size.width - 20 * appConstants.ScreenFactor, 30 * appConstants.ScreenFactor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setData(model: NSDictionary)  {
        horseData = model
        horseName.text = horseData.objectForKey(appConstants.name) as? String
    }

    func moveToDetail(gestureRecognizer: UITapGestureRecognizer){
        horseSelected!.itemSelected(horseData)
        print(horseData)
    }
}

//
//  NavigationMainView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 07/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class NavigationMainView: UIView {
    var appConstants: AppConstants = AppConstants()
    var menuSelected: MenuSelectionCallback?
    var nameArray: [String] = ["Horse", "Recent", "Messages", "Calender", "Picture","Owner Group","Settings"] //changed from daily activity

    var itemCount:CGFloat = 7
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addView() {
        var yy:CGFloat = 0
        var counter:Int = 1
        let barItemheight:CGFloat = self.frame.size.height/itemCount
        
        for name in nameArray {
            let counterNoti: UILabel = UILabel.init(frame: CGRectMake(self.frame.size.width - 20 * appConstants.ScreenFactor ,0, 10 * appConstants.ScreenFactor,10 * appConstants.ScreenFactor))
            counterNoti.center = CGPointMake(counterNoti.center.x, yy + barItemheight/2)
            counterNoti.textColor = UIColor.whiteColor()
            counterNoti.font = UIFont.init(name: "Helvetica", size: 7 * appConstants.ScreenFactor)
            
            if counter == 1 && SharedPreferenceUtils._sharedInstance.getStringValue(appConstants.horse_count) != "0"{
                counterNoti.text = SharedPreferenceUtils._sharedInstance.getStringValue(appConstants.horse_count)
                counterNoti.textAlignment = NSTextAlignment.Center
                counterNoti.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
                counterNoti.layer.masksToBounds = false
                counterNoti.layer.cornerRadius = counterNoti.frame.size.width/2
                counterNoti.clipsToBounds = true
            }
            self.addSubview(counterNoti)
            
            let nameLabel: UILabel = UILabel.init(frame: CGRectMake(10, yy, frame.size.width - counterNoti.frame.size.width - 20 * appConstants.ScreenFactor, barItemheight))
            nameLabel.backgroundColor = UIColor.clearColor()
            nameLabel.userInteractionEnabled = true
            nameLabel.text = name
            nameLabel.textColor = UIColor.whiteColor()
            nameLabel.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
            nameLabel.tag = counter
            self.addSubview(nameLabel)
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectedMenuItem(_:)))
            nameLabel.addGestureRecognizer(tapRecognizer)
            
            counter+=1
            yy += barItemheight
            
            let line: UIView = UIView()
            if counter == 3 || counter == 4 || counter == 5 {
                line.frame = CGRectMake(-1 , yy - 2.5, self.frame.size.width + 1, 1)
            }else {
                line.frame = CGRectMake(-1 , yy - 2.25, self.frame.size.width + 1, 1)
            }
            line.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
            self.addSubview(line)
        }
    }
    
    func selectedMenuItem(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!
        menuSelected?.menuSelected(viewTouched.tag)
    }
}


//
//  ContactDetailView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 20/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import MessageUI

class ContactDetailView: UIView,MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    var yy:CGFloat = 0, margin:CGFloat = 0
    var isScreenOwnerMessage: Bool = true
    
    var webService: WebService = WebService()
    var appConstants: AppConstants = AppConstants()
    var media_base_url : String?
    var userData : NSDictionary = NSDictionary()
    
    var userNameContainer: LoginInputViews?
    var addressContainer: LoginInputViews?
    var emailContainer: LoginInputViews?
    var phoneContainer: LoginInputViews?
    
    var userImage : UIImageView = UIImageView()
    var nameArray: [String] = ["Email", "Message", "Phone"]
    var indicator = UIActivityIndicatorView()
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController

    var chatDictionary: NSDictionary = NSDictionary()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeVariables()
        setBottomButtons()
        AppConstants.CurrentScreen = appConstants.OwnerDetailScreen
         print(AppConstants.CurrentScreen)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeVariables() {
        yy = 20
        margin = 5 * appConstants.ScreenFactor
        
        userImage = UIImageView.init(frame: CGRectMake(0,yy,50 * appConstants.ScreenFactor,50 * appConstants.ScreenFactor))
        userImage.image = UIImage.init(imageLiteral: "newProfilePic.png")
        userImage.center = CGPointMake(self.frame.size.width/2, userImage.frame.size.height/2 + yy)
        userImage.backgroundColor = UIColor.clearColor()
        userImage.layer.masksToBounds = false
        userImage.layer.cornerRadius = userImage.frame.size.width/2
        userImage.clipsToBounds = true
        self.addSubview(userImage)
        
        yy += userImage.frame.size.height + 10
        
        userNameContainer = LoginInputViews(frame: CGRectMake(margin,yy ,self.frame.size.width - 2*margin,25 * appConstants.ScreenFactor))
        userNameContainer!.addView(1)
        userNameContainer?.entryView.enabled = false
        self.addSubview(userNameContainer!)
        
        yy += userNameContainer!.frame.size.height + 6

        addressContainer = LoginInputViews(frame: CGRectMake(margin,yy,self.frame.size.width - 2*margin,25 * appConstants.ScreenFactor))
        addressContainer!.addView(3)
        addressContainer?.entryView.enabled = false
        self.addSubview(addressContainer!)
        
        yy += addressContainer!.frame.size.height + 6
        
        emailContainer = LoginInputViews(frame: CGRectMake(margin,yy,self.frame.size.width - 2*margin,25 * appConstants.ScreenFactor))
        emailContainer!.addView(4)
        emailContainer?.entryView.enabled = false
        self.addSubview(emailContainer!)
        
        yy += emailContainer!.frame.size.height + 6
        
        phoneContainer = LoginInputViews(frame: CGRectMake(margin,yy,self.frame.size.width - 2*margin,25 * appConstants.ScreenFactor))
        phoneContainer!.addView(5)
        phoneContainer?.entryView.enabled = false
        self.addSubview(phoneContainer!)
        
    }
    
    func getUserDetail(userID: String, media_base_url: String, chatDict: NSDictionary){
        chatDictionary = chatDict
        print(chatDict)
        
        let defaults = NSUserDefaults.standardUserDefaults()
      //  defaults.setValue(chatDict, forKey: "dictForOwnerDetail")//pooja1
      //  defaults.setObject(media_base_url, forKey: "media_base_url") //pooja
        //send to server
        showProgress()
        
        let postString = "action=contactdetail&userId="+userID;
       // self.media_base_url = media_base_url
                
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        print(mainList)
                        self.userData = mainList.objectForKey(self.appConstants.contacts) as! NSDictionary
                        self.media_base_url = mainList.objectForKey(self.appConstants.media_base_url)as! NSString as String
                        print(self.media_base_url)
                        print("userData = \(self.userData)")
                        self.setData()
                    }
                }
            }
        }
        
       // AppConstants.CurrentScreen = appConstants.OwnerScreenDetailByPooja
        print(AppConstants.CurrentScreen)
    }
    
    func styleLabels(label: UILabel){
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.init(name: "Helvetica", size: 7 * appConstants.ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
    }
    
    func setData() {
        userNameContainer?.entryView.text = userData.objectForKey(appConstants.name) as? String
        addressContainer?.entryView.text = userData.objectForKey(appConstants.address) as? String
        emailContainer?.entryView.text = userData.objectForKey(appConstants.emailID) as? String
        phoneContainer?.entryView.text = userData.objectForKey(appConstants.phone) as? String
        
        let mediaSource:String = (userData.objectForKey(appConstants.profilePic) as? String)!
        
        let media_url = appConstants.media_base_url
        print(self.media_base_url)
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(self.media_base_url, forKey: "media_base_url")
        
        userImage.downloadedFrom(self.media_base_url!+mediaSource)
        
    }
    
    func setBottomButtons(){
        yy = self.frame.size.height * 0.91
        let bottomItemWidth:CGFloat = self.frame.size.width/3
        
        var counter:Int = 0
        for name in nameArray {
            
            let bottomButton = UIButton.init()
            switch counter {
            case 0:
                bottomButton.frame = CGRectMake(margin,yy,bottomItemWidth - 1.2*margin,22 * appConstants.ScreenFactor)
                break;
            case 1:
                bottomButton.frame = CGRectMake(bottomItemWidth + 0.4*margin,yy,bottomItemWidth - margin,22 * appConstants.ScreenFactor)
                break;
            case 2:
                bottomButton.frame = CGRectMake(2*bottomItemWidth,yy,bottomItemWidth - 1.2*margin,22 * appConstants.ScreenFactor)
                break;
            default:
                break;
            }
            bottomButton.layer.cornerRadius = appConstants.CornerRadius
            bottomButton.setTitle(name, forState: UIControlState.Normal)
            bottomButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
            bottomButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            bottomButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
            bottomButton.userInteractionEnabled = true
            bottomButton.tag = counter + 1
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(buttonPressed(_:)))
            bottomButton.addGestureRecognizer(tapRecognizer)
            self.addSubview(bottomButton)
            counter += 1
        }
    }
    
    func buttonPressed(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!
        switch viewTouched.tag {
        case 1:
            sendEmailButtonTapped()
            /*let emailReceiver = (emailContainer?.entryView.text)! as String

            let url = NSURL(string: "mailto:" + emailReceiver + "?subject=EquiOwner message")
            UIApplication.sharedApplication().openURL(url!)
            */
            /*let url = NSURL(string: "mailto:")
            UIApplication.sharedApplication().openURL(url!)*/
            break
//        case 2:
//            //UIApplication.sharedApplication().openURL(NSURL(string: "sms:")!)
//            let chat = chatDictionary.objectForKey(self.appConstants.chat) as! NSDictionary
//            print(chat)
//          
//        let userName1 = appConstants.nullToNil(chat["userName"])
//            print(userName1)
//            
//            if(userName1 == nil)
//            {
//                print("dict is nil")
//                // TO Do -show a pop up in case when userName is nil
//                //02-05-2017
//                
//                    if (MFMessageComposeViewController.canSendText()) {
//                        let controller = MFMessageComposeViewController()
//                        
//                        let strCallNo = (phoneContainer?.entryView.text)! as String
//                        let trimmedString = strCallNo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//                        controller.messageComposeDelegate = self
//                        
//                        controller.recipients = [trimmedString]
//                        self.vc!.presentViewController(controller, animated: true, completion: nil)
//                    }
//                
//            }
//            else{
//            
//            let userName = chat.objectForKey(self.appConstants.userName) as! String
//            print("chatDictionary = \(chatDictionary)")
//            print("chat = \(chat)")
//
//             if(appConstants.nullToNil(phoneContainer?.entryView.text) != nil){
//               // if(userName.isEmpty){
//                if(userName1 == nil){
//                    if (MFMessageComposeViewController.canSendText()) {
//                        let controller = MFMessageComposeViewController()
//                        
//                        let strCallNo = (phoneContainer?.entryView.text)! as String
//                        let trimmedString = strCallNo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//                        controller.messageComposeDelegate = self
//                        
//                        controller.recipients = [trimmedString]
//                        self.vc!.presentViewController(controller, animated: true, completion: nil)
//                    }
//                }else{
//                    self.subviews.forEach({ $0.removeFromSuperview() })
//                    AppConstants.CurrentScreen = self.appConstants.MessagingScreenFromOwnerListing //pooja 27/4
//                    
//                   // AppConstants.CurrentScreen = appConstants.OwnerScreenDetailByPooja
//                    
//                    let userDetail:ChatListing = ChatListing(frame : self.frame)
//                    
//                    userDetail.isFromOwnerPage = "POOJA"
//                    userDetail.checkScreen()//pooja
//                    userDetail.backgroundColor = UIColor.clearColor()
//                    userDetail.contactChatDict = chatDictionary
//                    userDetail.getChatUser(chatDictionary)
//                    
//                    self.addSubview(userDetail)
//                }
//             }
//            }
            
            
            
        case 2:
            //UIApplication.sharedApplication().openURL(NSURL(string: "sms:")!)
            let chat = chatDictionary.objectForKey(self.appConstants.chat) as! NSDictionary
            print(chat)
            
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                
                let strCallNo = (phoneContainer?.entryView.text)! as String
                let trimmedString = strCallNo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                controller.messageComposeDelegate = self
                
                controller.recipients = [trimmedString]
                self.vc!.presentViewController(controller, animated: true, completion: nil)
            }

            break
        case 3:
            if(appConstants.nullToNil(phoneContainer?.entryView.text) != nil){
                let strCallNo = (phoneContainer?.entryView.text)! as String
                let trimmedString = strCallNo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                let telUrl:NSURL? = NSURL(string: "telprompt://"+trimmedString)
                if ((telUrl) != nil){
                    if(UIApplication.sharedApplication().canOpenURL(telUrl!)){
                        UIApplication.sharedApplication().openURL(telUrl!)
                    }else{
                        appConstants.showAlert("Oops!", message: "Call services not available.", controller: vc!)
                    }
                }
            }
            break
        default:
            break
        }
    }
    
    func sendEmailButtonTapped() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            vc!.presentViewController(mailComposeViewController, animated: true, completion: nil)
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let emailReceiver = (emailContainer?.entryView.text)! as String

        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients([emailReceiver])
        mailComposerVC.setSubject("EquiOwner message")
        mailComposerVC.setMessageBody("", isHTML: false)
        return mailComposerVC
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        //... handle sms screen actions
        vc!.dismissViewControllerAnimated(true, completion: nil)
    }

    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?){
        vc!.dismissViewControllerAnimated(true, completion: nil)
    }
    
   }

//
//  ContactTableCellView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 18/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation
import UIKit

class ContactTableCellView: UITableViewCell {
    var contactData: NSDictionary = NSDictionary()
    var contactImage: UIImageView = UIImageView()
    var contactName: UILabel = UILabel()
    var appConstants: AppConstants = AppConstants()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        contactImage.frame = CGRectMake(2*appConstants.ScreenFactor,0,30 * appConstants.ScreenFactor,30 * appConstants.ScreenFactor)
        contactImage.image = UIImage.init(imageLiteral: "newProfilePic.png")
        contactImage.center = CGPointMake(contactImage.center.x, contentView.frame.size.height/2)
        contactImage.backgroundColor = UIColor.clearColor()
        contactImage.layer.masksToBounds = false
        contactImage.layer.borderWidth = 1
        contactImage.layer.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.4).CGColor
        contactImage.layer.cornerRadius = contactImage.frame.size.width/2
        contactImage.clipsToBounds = true
        contentView.addSubview(contactImage)
        
        contactName.frame = CGRectZero
        contactName.textColor = UIColor.whiteColor()
        contactName.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
        contactName.backgroundColor = UIColor.clearColor()
        contentView.addSubview(contactName)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contactImage.frame = CGRectMake(2*appConstants.ScreenFactor, 5*appConstants.ScreenFactor,30 * appConstants.ScreenFactor,30 * appConstants.ScreenFactor)
        contactImage.center = CGPointMake(contactImage.center.x, contentView.frame.size.height/2)
        
        contactName.frame = CGRectMake(contactImage.frame.origin.x + contactImage.frame.size.width + 5*appConstants.ScreenFactor, 5, frame.size.width - (contactImage.frame.origin.x + contactImage.frame.size.width + 6*appConstants.ScreenFactor), 40 * appConstants.ScreenFactor)
    }    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setData(model: NSDictionary, media_base_url: String)  {
        contactData = model
        contactName.text = contactData.objectForKey(appConstants.name) as? String
        let profilePic: String = contactData.objectForKey(appConstants.profilePic) as! String!
        contactImage.downloadedFrom(media_base_url+profilePic)
    }
}

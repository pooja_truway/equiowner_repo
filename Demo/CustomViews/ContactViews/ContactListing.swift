//
//  ContactListing.swift
//  Demo
//
//  Created by KOMAL SHARMA on 18/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import MessageUI

class ContactListing: UIView, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate {
    var fromWhere: String = ""
    
    var tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var contactList:NSMutableArray = NSMutableArray()
    var cellIdendifier: String = "ContactCell"
    var baseUrl: String = String()
    var indicator = UIActivityIndicatorView()
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    


    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableView.frame             =   self.frame;
        tableView.delegate          =   self
        tableView.dataSource        =   self
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.SingleLine;
        tableView.separatorColor    = UIColor.whiteColor().colorWithAlphaComponent(0.3);
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        self.addSubview(tableView)
        
        print("contactListingScreen \(AppConstants.CurrentScreen)")
        //AppConstants.CurrentScreen = appConstants.OwnerDetailScreen //pooja 2/5
        AppConstants.CurrentScreen = appConstants.OwnerListingScreen
          print(AppConstants.CurrentScreen)
        getContactListFromServer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getContactListFromServer() {
        //send to server
        showProgress()
        let postString = "action=contacts&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID);
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        self.baseUrl = mainList.objectForKey(self.appConstants.media_base_url) as! String
                        let contactList11: NSArray = mainList.objectForKey(self.appConstants.contacts) as! NSArray
                        self.contactList = contactList11.mutableCopy() as! NSMutableArray
                        print("contactList = \(self.contactList)")
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40 * appConstants.ScreenFactor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = ContactTableCellView(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        
        if(self.contactList.count > 0){
            let contactDict: NSDictionary = self.contactList[indexPath.row] as! NSDictionary
            cell.setData(contactDict,media_base_url: baseUrl)
        }
        cell.separatorInset = UIEdgeInsetsZero
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactList.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // you need to implement this method too or you can't swipe to display the actions
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let messageAction = UITableViewRowAction(style: .Normal, title: "Message", handler: { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            
            let contactDict = self.contactList[indexPath.row] as! NSDictionary
            let chatDict = contactDict.objectForKey(self.appConstants.chat) as! NSDictionary
            let userName = chatDict.objectForKey(self.appConstants.userName) as! String
            
            if(userName.isEmpty){
                if (MFMessageComposeViewController.canSendText()) {
                    let controller = MFMessageComposeViewController()
                    let strCallNo = contactDict.objectForKey(self.appConstants.phone) as! String
                    let trimmedString = strCallNo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                    controller.messageComposeDelegate = self

                    controller.recipients = [trimmedString]
                    self.vc!.presentViewController(controller, animated: true, completion: nil)
                }
            }else{
                 self.subviews.forEach({ $0.removeFromSuperview() })
                 AppConstants.CurrentScreen = self.appConstants.OwnerDetailScreen
                 
                 let userDetail:ChatListing = ChatListing(frame : self.frame)
                 userDetail.backgroundColor = UIColor.clearColor()
                 userDetail.contactChatDict = contactDict
                 userDetail.getChatUser(contactDict)
                 self.addSubview(userDetail)
            }
        })
        messageAction.backgroundColor = UIColor.grayColor()
        
        let callAction = UITableViewRowAction(style: .Normal, title: "Call", handler: { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
                let contactDict: NSDictionary = self.contactList[indexPath.row] as! NSDictionary

                let strCallNo = contactDict.objectForKey(self.appConstants.phone) as! String
                let trimmedString = strCallNo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                let telUrl:NSURL? = NSURL(string: "telprompt://"+trimmedString)
                if ((telUrl) != nil){
                    if(UIApplication.sharedApplication().canOpenURL(telUrl!)){
                        UIApplication.sharedApplication().openURL(telUrl!)
                    }else
                    {
                        print("Call not available")
                    }
                }
            })
        callAction.backgroundColor = UIColor.darkGrayColor()
        return [messageAction, callAction]
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.subviews.forEach({ $0.removeFromSuperview() })
        AppConstants.CurrentScreen = appConstants.OwnerDetailScreen
        
        let contactDict: NSDictionary = self.contactList[indexPath.row] as! NSDictionary

        let userDetail:ContactDetailView = ContactDetailView(frame : self.frame)
        userDetail.backgroundColor = UIColor.clearColor()
        userDetail.getUserDetail(contactDict.objectForKey(appConstants.userId) as! String, media_base_url: baseUrl, chatDict: contactDict)
        print ("contactDict>> \(contactDict)")
        let defaults = NSUserDefaults.standardUserDefaults()
        let chat = appConstants.nullToNil(contactDict.valueForKey("chat")?.valueForKey("userName"))
         print(chat)
        if chat == nil
        {
          print(chat)
        }
        else{
        defaults.setValue(contactDict, forKey:"dictForOwnerDetail")
        }
        self.addSubview(userDetail)
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        //... handle sms screen actions
        vc!.dismissViewControllerAnimated(true, completion: nil)
    }
}

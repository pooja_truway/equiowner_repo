//
//  HeaderView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 06/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    var image: UIImageView = UIImageView()
    var LogoImage: UIImageView = UIImageView()

    var name: UILabel = UILabel()
    var appConstants: AppConstants = AppConstants()
    var viewNotif: UIView = UIView()
    var notifButton: UIButton = UIButton()
    var notifLbl: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addView() {
//        image = UIImageView.init(frame:CGRectMake(10 * appConstants.ScreenFactor,50,self.frame.size.height - 20 * appConstants.ScreenFactor,self.frame.size.height - 20 * appConstants.ScreenFactor))
        
        print("height \(self.frame.size.height)")
         print("screen factor \(appConstants.ScreenFactor)")
        
         image = UIImageView.init(frame:CGRectMake(10 * appConstants.ScreenFactor,40,self.frame.size.height - 50 * appConstants.ScreenFactor,self.frame.size.height - 50 * appConstants.ScreenFactor))
        image.center = CGPointMake(image.center.x, self.frame.size.height/2 + 30)
        image.backgroundColor = UIColor.clearColor()
        image.layer.masksToBounds = false
        image.layer.cornerRadius = image.frame.size.width/2
        image.layer.borderWidth = 2
        image.clipsToBounds = true
        image.layer.borderColor = UIColor.grayColor().CGColor
        image.image = UIImage.init(imageLiteral: "newProfilePic.png")
        self.addSubview(image)
        
        
        LogoImage = UIImageView.init(frame:CGRectMake(55 * appConstants.ScreenFactor,14,self.frame.size.height  * appConstants.ScreenFactor,self.frame.size.height - 55 * appConstants.ScreenFactor))
        LogoImage.backgroundColor = UIColor.clearColor()
        LogoImage.layer.masksToBounds = false
     
        LogoImage.clipsToBounds = true
        LogoImage.image = UIImage.init(imageLiteral: "equiowner_logo.png")
        self.addSubview(LogoImage)

        
        
        
        name = UILabel.init(frame:CGRectMake(image.frame.origin.x + image.frame.size.width + 15 * appConstants.ScreenFactor, 30 , self.frame.size.width - (image.frame.origin.x + image.frame.size.width + 18 * appConstants.ScreenFactor) - 5 * appConstants.ScreenFactor, self.frame.size.height))
        name.numberOfLines = 2
        name.text = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.name)
        name.textColor = UIColor.whiteColor()
        name.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
        
        name.backgroundColor = UIColor.clearColor()
        self.addSubview(name)
       
//        viewNotif = UIView.init(frame:CGRectMake(name.frame.origin.x + 160,60,70,70))
        
         viewNotif = UIView.init(frame:CGRectMake(name.frame.origin.x + image.frame.size.width + 60 * appConstants.ScreenFactor,50,100,100))
        
               viewNotif.backgroundColor = UIColor.clearColor()
        
        self.addSubview(viewNotif)
        notifButton = UIButton.init(frame:CGRectMake(65 ,5,40,40))
        notifButton.backgroundColor = UIColor.clearColor()
        let imageNotif = UIImage(named: "notification.png") as UIImage?
        notifButton.setImage(imageNotif, forState: .Normal)

        viewNotif.addSubview(notifButton)
        
        notifLbl = UILabel.init(frame:CGRectMake(notifButton.frame.origin.x + 25 ,7,15,15))
        notifLbl.backgroundColor = UIColor.redColor()
        notifLbl.layer.masksToBounds = true
        notifLbl.layer.cornerRadius = notifLbl.frame.size.width/2
        notifLbl.font = UIFont.init(name: "Helvetica", size: 10)
        notifLbl.text = ""
        notifLbl.textColor = UIColor.whiteColor()
        notifLbl.textAlignment = .Center;
        viewNotif.addSubview(notifLbl)
        
    }
}


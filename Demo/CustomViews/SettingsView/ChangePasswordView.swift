//
//  ChangePasswordView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 27/09/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordView: UIView, UITextFieldDelegate{
    var yy:CGFloat = 0, margin:CGFloat = 0, sYY:CGFloat = 0
    var saveButton: UIButton = UIButton()
    
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    
    let mainLayout : UIView = UIView()
    var oldPasswordLabel:SkyFloatingLabelTextField = SkyFloatingLabelTextField()
    var newPasswordLabel:SkyFloatingLabelTextField = SkyFloatingLabelTextField()
    var confirmPasswordLabel:SkyFloatingLabelTextField = SkyFloatingLabelTextField()
    
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    var indicator = UIActivityIndicatorView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeVariables()
        setTopLayer()
        setBottomButtons()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeVariables() {
        yy = 5
        margin = 5 * appConstants.ScreenFactor
    }
    
    func setTopLayer(){
        let eventLabel : UILabel = UILabel()
        eventLabel.frame = CGRectMake(margin,yy,self.frame.size.width,13 * appConstants.ScreenFactor)
        eventLabel.text = "Change Password"
        appConstants.styleLabels(eventLabel)
        eventLabel.textColor = UIColor.whiteColor()
        self.addSubview(eventLabel)
        
        yy+=eventLabel.frame.size.height + 5
        
        mainLayout.frame = CGRectMake(margin,yy,self.frame.size.width - 2*margin,83 * appConstants.ScreenFactor)
        mainLayout.backgroundColor = UIColor.whiteColor();
        self.addSubview(mainLayout)
        
        let vline1: UIView = UIView(frame : CGRectMake(0 , 0 , 1, mainLayout.frame.size.height))
        vline1.backgroundColor = UIColor().HexToColor(appConstants.button_color)
        mainLayout.addSubview(vline1)
        
        oldPasswordLabel = SkyFloatingLabelTextField(frame: CGRectMake(margin, 3 * appConstants.ScreenFactor,mainLayout.frame.size.width - 2*margin, 24 * appConstants.ScreenFactor))
        oldPasswordLabel.placeholder = "Enter Old Password"
        oldPasswordLabel.title = "Enter Old Password"
        oldPasswordLabel.delegate = self
        appConstants.styleFloatingFields(oldPasswordLabel)
        mainLayout.addSubview(oldPasswordLabel)
        
        newPasswordLabel = SkyFloatingLabelTextField(frame: CGRectMake(margin, 30 * appConstants.ScreenFactor,mainLayout.frame.size.width - 2*margin, 24 * appConstants.ScreenFactor))
        newPasswordLabel.placeholder = "Enter New Password"
        newPasswordLabel.title = "Enter New Password"
        newPasswordLabel.delegate = self
        appConstants.styleFloatingFields(newPasswordLabel)
        mainLayout.addSubview(newPasswordLabel)
       
        confirmPasswordLabel = SkyFloatingLabelTextField(frame: CGRectMake(margin, 56 * appConstants.ScreenFactor,mainLayout.frame.size.width - 2*margin, 24 * appConstants.ScreenFactor))
        confirmPasswordLabel.placeholder = "Confirm Password"
        confirmPasswordLabel.title = "Confirm Password"
        confirmPasswordLabel.delegate = self
        appConstants.styleFloatingFields(confirmPasswordLabel)
        mainLayout.addSubview(confirmPasswordLabel)
    }
    
    func setBottomButtons(){
        yy = self.frame.size.height * 0.91
        
        saveButton = UIButton.init(frame: CGRectMake(margin,yy,self.frame.size.width - 2*margin,22 * appConstants.ScreenFactor))
        saveButton.layer.cornerRadius = appConstants.CornerRadius
        saveButton.setTitle("Save", forState: UIControlState.Normal)
        saveButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        saveButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        saveButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        saveButton.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(savePassword(_:)))
        saveButton.addGestureRecognizer(tapRecognizer)
        self.addSubview(saveButton)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        self.endEditing(true)
        return true;
    }
    
    func savePassword(gestureRecognizer: UITapGestureRecognizer) {
        let newPassord: String = newPasswordLabel.text!
        let oldPassword: String = oldPasswordLabel.text!
        let confirmPassword: String = confirmPasswordLabel.text!

        var isError:Bool = false
        if (oldPassword.isEmpty) {
            isError = true
            appConstants.showAlert("Warning", message: "Please enter old password", controller: vc!)
        }else if (newPassord.isEmpty) {
            isError = true
            appConstants.showAlert("Warning", message: "Please enter new password", controller: vc!)
        }else if (confirmPassword.isEmpty) {
            isError = true
            appConstants.showAlert("Warning", message: "Please confirm password", controller: vc!)
        }else if (confirmPassword != newPassord) {
            isError = true
            appConstants.showAlert("Warning", message: "Password does not match", controller: vc!)
        }
        
        
        if(!isError)
        {
            showProgress()
            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.PASSWORD, value: newPassord)
            let postString = "action=changepassword&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&password="+oldPassword+"&confirm_pass="+newPassord;
            
            webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    self.hideProgress()
                    if error != nil{
                        self.appConstants.showAlert("Whoa!", message: error! ,controller: self.vc!)
                        return
                    }
                    print("response = \(response)")
                    let data = self.appConstants.convertStringToDictionary(response)
                    if data != nil {
                        if ((data?[self.appConstants.ErrorCode]) != nil) {
                            //Error
                            self.appConstants.showAlert("Whoa!", message: (data?[self.appConstants.Message])! as! String,controller: self.vc!)
                            return
                        }else{
                            //back
                            self.appConstants.showAlert("", message: "Password Changes Successfully!",controller: self.vc!)

                        }
                    }
                }
            }
        }
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
}
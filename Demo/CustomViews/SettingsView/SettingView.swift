//
//  SettingView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 27/09/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class SettingView: UIView , UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate{
    var scrollView:UIScrollView?
    var yy:CGFloat = 0, margin:CGFloat = 0
    
    var webService: WebService = WebService()
    var appConstants: AppConstants = AppConstants()
    
    var userImage : UIImageView = UIImageView()
    var nameArray: [String] = ["Save", "Signout"]
    
    var userNameContainer: LoginInputViews?
    var lastNameContainer: LoginInputViews?
    var addressContainer: LoginInputViews?
    var phoneContainer: LoginInputViews?
    var activeView: UIView?

    var media_base_url : String?
    var userData : NSDictionary = NSDictionary()
    
    var onOffSwitch : UISwitch = UISwitch()
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    let imagePickerController = UIImagePickerController()
    var indicator = UIActivityIndicatorView()


    override init(frame: CGRect) {
        super.init(frame: frame)
        addNotifications()
        initializeVariables()
        getUserDetail()
        setBottomButtons()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: nil);
        
    }
    
    func keyboardWillShow(sender: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        scrollView!.scrollEnabled = true
        let info : NSDictionary = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        scrollView!.contentInset = contentInsets
        scrollView!.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.frame
        aRect.size.height -= keyboardSize!.height
        if activeView != nil {
            if (!CGRectContainsPoint(aRect, activeView!.frame.origin))
            {
                scrollView!.scrollRectToVisible(activeView!.frame, animated: true)
            }
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let info : NSDictionary = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        scrollView!.contentInset = UIEdgeInsetsZero
        scrollView!.scrollIndicatorInsets = contentInsets
        dismissKeyboard()
        scrollView!.scrollEnabled = false
    }
    
    func initializeVariables() {
        yy = 20
        margin = 5 * appConstants.ScreenFactor
        
        scrollView = UIScrollView.init(frame: self.frame)
        scrollView!.contentSize = self.frame.size
        scrollView!.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        
        self.addSubview(scrollView!)
   
        
        userImage = UIImageView.init(frame: CGRectMake(0,yy,50 * appConstants.ScreenFactor,50 * appConstants.ScreenFactor))
        userImage.image = UIImage.init(imageLiteral: "newProfilePic.png")
        userImage.center = CGPointMake(self.frame.size.width/2, userImage.frame.size.height/2 + yy)
        userImage.backgroundColor = UIColor.whiteColor()
        userImage.layer.masksToBounds = false
        userImage.layer.cornerRadius = userImage.frame.size.width/2
        userImage.clipsToBounds = true
        userImage.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(changeImage(_:)))
        userImage.addGestureRecognizer(tapRecognizer)
        scrollView!.addSubview(userImage)
        
        let imageString: String = SharedPreferenceUtils._sharedInstance.getStringValue(self.appConstants.PROFILE_IMAGE)
        if !imageString.isEmpty {
            let dataDecoded:NSData = NSData(base64EncodedString: imageString, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
            userImage.image = UIImage(data: dataDecoded)!

        }
        yy += userImage.frame.size.height + 10
        
        userNameContainer = LoginInputViews(frame: CGRectMake(margin,yy ,self.frame.size.width - 2*margin,25 * appConstants.ScreenFactor))
        userNameContainer!.addView(1)
        userNameContainer!.entryView.delegate = self
        scrollView!.addSubview(userNameContainer!)
        
        yy += userNameContainer!.frame.size.height + 6
        
        lastNameContainer = LoginInputViews(frame: CGRectMake(margin,yy,self.frame.size.width - 2*margin,25 * appConstants.ScreenFactor))
        lastNameContainer!.addView(6)
        lastNameContainer!.entryView.delegate = self
        scrollView!.addSubview(lastNameContainer!)
        
        yy += lastNameContainer!.frame.size.height + 6
        
        addressContainer = LoginInputViews(frame: CGRectMake(margin,yy,self.frame.size.width - 2*margin,25 * appConstants.ScreenFactor))
        addressContainer!.addView(3)
        addressContainer!.entryView.delegate = self
        scrollView!.addSubview(addressContainer!)
        
        yy += addressContainer!.frame.size.height + 6
        
        phoneContainer = LoginInputViews(frame: CGRectMake(margin,yy,self.frame.size.width - 2*margin,25 * appConstants.ScreenFactor))
        phoneContainer!.addView(5)
        phoneContainer!.entryView.delegate = self
        scrollView!.addSubview(phoneContainer!)
        
        yy += phoneContainer!.frame.size.height + 10

        drawContainer(1)
        drawContainer(2)
        scrollView!.contentSize = CGSize.init(width: self.frame.size.width, height: yy)
    }
    
    func getUserDetail(){
        showProgress()
        //send to server
        let postString = "action=getdetails&userId="+SharedPreferenceUtils._sharedInstance.getStringValue(appConstants.memberID);
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        self.userData = (data?[self.appConstants.data])! as! NSDictionary
                        self.media_base_url = self.userData.objectForKey(self.appConstants.media_base_url) as? String
                        self.setData()
                    }
                }
            }
        }
    }
    
    func styleLabels(label: UILabel){
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.init(name: "Helvetica", size: 7 * appConstants.ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
    }
    
    func setData() {
        userNameContainer?.entryView.text = userData.objectForKey(appConstants.firstName) as? String
        addressContainer?.entryView.text = userData.objectForKey(appConstants.address) as? String
        lastNameContainer?.entryView.text = userData.objectForKey(appConstants.lastName) as? String
        phoneContainer?.entryView.text = userData.objectForKey(appConstants.phone) as? String
        
        let mediaSource:String = (userData.objectForKey(appConstants.profilePic) as? String)!
        userImage.downloadedFrom(media_base_url!+mediaSource)
    }
    
    func drawContainer(tag: Int){
        let container = UIView.init(frame: CGRectMake(margin,yy ,self.frame.size.width - 2*margin,30 * appConstants.ScreenFactor))
        scrollView!.addSubview(container)
        
        let titleView = UILabel.init(frame: CGRectMake(0, 2 * appConstants.ScreenFactor, self.frame.size.width/2, container.frame.size.height/2))
        titleView.textColor = UIColor.whiteColor()
        titleView.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        if tag == 1 {
            titleView.text = "Password"
        }else{
            titleView.text = "Receive Notification"
        }
        titleView.backgroundColor = UIColor.clearColor()
        container.addSubview(titleView)
        
        let subtitleView = UILabel.init(frame: CGRectMake(0,container.frame.size.height/2, self.frame.size.width * 0.7, container.frame.size.height/2))
        subtitleView.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        subtitleView.font = UIFont.init(name: "Helvetica", size: 7 * appConstants.ScreenFactor)

        if tag == 1 {
            subtitleView.text = "*********"
        }else{
            subtitleView.text = "Will pop up on home screen"
        }
        subtitleView.backgroundColor = UIColor.clearColor()
        container.addSubview(subtitleView)
        
        if tag == 1 {
            let bottomButton = UIButton.init()
            bottomButton.frame = CGRectMake(self.frame.size.width - self.frame.size.width * 0.35,0.2 * container.frame.size.height,self.frame.size.width * 0.3,container.frame.size.height * 0.6)
            bottomButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
            
            bottomButton.layer.cornerRadius = appConstants.CornerRadius
            bottomButton.setTitle("Change", forState: UIControlState.Normal)
            bottomButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
            bottomButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            bottomButton.userInteractionEnabled = true
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(changePassword(_:)))
            bottomButton.addGestureRecognizer(tapRecognizer)
            container.addSubview(bottomButton)
            yy += container.frame.size.height + 5

        }else{
            onOffSwitch.frame = CGRectMake(self.frame.size.width * 0.75,0.2 * container.frame.size.height,self.frame.size.width * 0.15,container.frame.size.height * 0.3)
            if SharedPreferenceUtils._sharedInstance.getBoolValue(self.appConstants.ISNOTIFICATIONSON) {
                onOffSwitch.setOn(true, animated: true)
            }else{
                onOffSwitch.setOn(false, animated: true)
            }

            container.addSubview(onOffSwitch)
            yy += container.frame.size.height + 10

        }
        
        let line = UIView.init(frame: CGRectMake(0, yy, self.frame.size.width, 1))
        line.backgroundColor=UIColor.whiteColor().colorWithAlphaComponent(0.6)
        scrollView!.addSubview(line)
       
        yy += 10
    }
    
    func changeImage(gestureRecognizer: UITapGestureRecognizer) {
        self.imagePickerController.sourceType = .PhotoLibrary
        self.imagePickerController.delegate = self
        self.vc!.presentViewController(self.imagePickerController, animated: true, completion: nil)
    }

    func changePassword(gestureRecognizer: UITapGestureRecognizer) {
        dismissKeyboard()
        self.subviews.forEach({ $0.removeFromSuperview() })
        self.gestureRecognizers?.forEach(self.removeGestureRecognizer)
        AppConstants.CurrentScreen = appConstants.ChangePasswordScreen
        let view:ChangePasswordView = ChangePasswordView(frame : self.frame)
        self.addSubview(view)
    }
    
    func setBottomButtons(){
        yy = self.frame.size.height * 0.91
        let bottomItemWidth:CGFloat = self.frame.size.width/2
        
        var counter:Int = 0
        for name in nameArray {
            
            let bottomButton = UIButton.init()
            switch counter {
            case 0:
                bottomButton.frame = CGRectMake(margin,yy,bottomItemWidth - 1.2*margin,22 * appConstants.ScreenFactor)
                bottomButton.backgroundColor = UIColor().HexToColor(appConstants.green, alpha: 1.0)

                break;
            case 1:
                bottomButton.frame = CGRectMake(bottomItemWidth + 0.4*margin,yy,bottomItemWidth - margin,22 * appConstants.ScreenFactor)
                bottomButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
                break;
            default:
                break;
            }
            bottomButton.layer.cornerRadius = appConstants.CornerRadius
            bottomButton.setTitle(name, forState: UIControlState.Normal)
            bottomButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
            bottomButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            bottomButton.userInteractionEnabled = true
            bottomButton.tag = counter + 1
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(buttonPressed(_:)))
            bottomButton.addGestureRecognizer(tapRecognizer)
            self.addSubview(bottomButton)
            counter += 1
        }
    }
    
    func buttonPressed(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!
        switch viewTouched.tag {
        case 1:
            //Save
            showProgress()
            SharedPreferenceUtils._sharedInstance.saveBoolValue(self.appConstants.ISNOTIFICATIONSON, value: onOffSwitch.on)
            var isOn = "off"
            if SharedPreferenceUtils._sharedInstance.getBoolValue(self.appConstants.ISNOTIFICATIONSON) {
                isOn = "on"
            }
            
            let json = NSMutableDictionary()
            json.setValue("updatedetails", forKey: self.appConstants.action)
            json.setValue(SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID), forKey: self.appConstants.userId)
            json.setValue(isOn, forKey: self.appConstants.notification)
            json.setValue(userNameContainer?.entryView.text, forKey: self.appConstants.firstName)
            json.setValue(lastNameContainer?.entryView.text, forKey: self.appConstants.lastName)
            json.setValue(addressContainer?.entryView.text, forKey: self.appConstants.address)
            json.setValue(phoneContainer?.entryView.text, forKey: self.appConstants.phone)
            json.setValue("jpg", forKey: "ext")
            json.setValue("notificationsave", forKey: "action")

            
            if SharedPreferenceUtils._sharedInstance.getStringValue(self.appConstants.PROFILE_IMAGE).isEmpty {
                 json.setValue(SharedPreferenceUtils._sharedInstance.getStringValue(self.appConstants.PROFILE_IMAGE), forKey: self.appConstants.profilePic)
            }else{
                 json.setValue("", forKey: self.appConstants.profilePic)
            }
            
            let postString = self.appConstants.convertDictionaryToString(json)! as String
            
          
            webService.HTTPPostJSON(appConstants.DAILY_ACTIVITY, data: postString) { (response, error) -> Void in
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    self.hideProgress()
                    if error != nil{
                        self.appConstants.showAlert("Whoa!", message: error! ,controller: self.vc!)
                        return
                    }
                    print("response = \(response)")
                    let data = self.appConstants.convertStringToDictionary(response)
                    if data != nil {
                        if ((data?[self.appConstants.ErrorCode]) != nil) {
                            //Error
                            self.appConstants.showAlert("Whoa!", message: (data?[self.appConstants.Message])! as! String,controller: self.vc!)
                            return
                        }else{
                            let selectedImage = self.appConstants.RBSquareImage(self.userImage.image!)
                            let imageData:NSData = UIImageJPEGRepresentation(selectedImage,0.5)!
                            let strBase64:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.PROFILE_IMAGE, value: strBase64)
                            
                            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.name, value: (self.userNameContainer?.entryView.text)! + " " + (self.lastNameContainer?.entryView.text)!)
                            NSNotificationCenter.defaultCenter().postNotificationName(self.appConstants.kN_UpdateProfilePicNotification, object: "")
                           //back
                        }
                    }
                }
            }
            break
        case 2:
            //signout
            
            let alert = UIAlertController(title: "Warning", message: "Are you sure you want to exit?", preferredStyle: UIAlertControllerStyle.Alert)
            
            func yesHandler(actionTarget: UIAlertAction){
                dismissKeyboard()
                SharedPreferenceUtils._sharedInstance.deleteAllData()
                SharedPreferenceUtils._sharedInstance.saveBoolValue(self.appConstants.IS_LOGIN, value: false)
                UIApplication.sharedApplication().keyWindow!.rootViewController = LoginViewController()
            }
            //event handler with predefined function
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: yesHandler));
            
            func noHandler(actionTarget: UIAlertAction){
                
            }
            //event handler with predefined function
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: noHandler));
            
            self.vc!.presentViewController(alert, animated: true, completion: nil);
            
            break
        default:
            break
        }
    }
    
    //Picture controller delegates
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePickerController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.imagePickerController.dismissViewControllerAnimated(true) {
            let selectedImage = self.appConstants.RBSquareImage(info[UIImagePickerControllerOriginalImage] as! UIImage)
            self.userImage.image = selectedImage
            let imageData:NSData = UIImageJPEGRepresentation(selectedImage,0.5)!
            let strBase64:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.PROFILE_IMAGE, value: strBase64)
            
            NSNotificationCenter.defaultCenter().postNotificationName(self.appConstants.kN_UpdateProfilePicNotification, object: strBase64)
        }
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    //TextField Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        dismissKeyboard()
        return true;
    }
    
    func textFieldDidBeginEditing(textField: UITextField){
        switch textField.tag {
        case 1:
            activeView = userNameContainer
            break
        case 6:
            activeView = lastNameContainer
            break
        case 3:
            activeView = addressContainer
            break
        case 5:
            activeView = phoneContainer
            break
        default:
            break
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField){
        activeView = nil
    }
}

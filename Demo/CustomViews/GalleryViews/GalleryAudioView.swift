//
//  GalleryAudioView.swift
//  Demo
//
//  Created by New User on 12/26/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import AVFoundation

class GalleryAudioView: UIView,AVAudioRecorderDelegate {

    
    var recordButton: UIButton!
    var saveButton: UIButton!
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioFileURL: NSURL!
    var appConstants: AppConstants = AppConstants()

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                dispatch_async(dispatch_get_main_queue()) {
                    if allowed {
                        self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }


    }
    
  
    
    func loadRecordingUI() {
        recordButton = UIButton(frame: CGRect(x: 30, y: 64, width: 128, height: 64))
        recordButton.layer.cornerRadius = appConstants.CornerRadius
        recordButton.setTitle("Tap to Record", forState: UIControlState.Normal)
        recordButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        recordButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        recordButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        recordButton.userInteractionEnabled = true
        recordButton.tag = 1
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(recordTapped(_:)))
        recordButton.addGestureRecognizer(tapRecognizer)
        self.addSubview(recordButton)
        
        
        saveButton = UIButton(frame: CGRect(x: 170, y: 64, width: 128, height: 64))
        saveButton.layer.cornerRadius = appConstants.CornerRadius
        saveButton.setTitle("Save Recording", forState: UIControlState.Normal)
        saveButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        saveButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        saveButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        saveButton.userInteractionEnabled = true
        saveButton.tag = 1
        let tapRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(saveTapped(_:)))
        saveButton.addGestureRecognizer(tapRecognizer1)
        self.addSubview(saveButton)

        
    }
 
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func saveTapped(gestureRecognizer: UITapGestureRecognizer) {
        
        
        
        

        
    }
    func startRecording() {
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let audioFilename = documentsPath.stringByAppendingPathComponent("recording.m4a")
        
        audioFileURL = NSURL(fileURLWithPath: audioFilename)

        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.High.rawValue
        ]
        
        do {
            try! audioRecorder = AVAudioRecorder(URL: audioFileURL, settings: settings)

            
            audioRecorder.delegate = self
            audioRecorder.record()
            recordButton.setTitle("Tap to Stop", forState: UIControlState.Normal)
        } catch {
            finishRecording(false)
        }
    }

    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        
        if success {
            recordButton.setTitle("Tap to Re-record", forState: UIControlState.Normal)

        } else {
            recordButton.setTitle("Tap to Record", forState: UIControlState.Normal)
            // recording failed :(
        }
    }
    func recordTapped(gestureRecognizer: UITapGestureRecognizer) {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(true)
        }
    }
 
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(false)
        }
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

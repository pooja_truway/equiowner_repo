//
//  collectionGallery.swift
//  Demo
//
//  Created by Truway India on 18/04/17.
//  Copyright © 2017 BreakfastBay. All rights reserved.
//

/*import UIKit
import AVKit
import AVFoundation
import MediaPlayer
import MobileCoreServices
import Photos



class collectionGallery: UIView,UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,SwiftPhotoGalleryDelegate, SwiftPhotoGalleryDataSource,AudioRecorderViewControllerDelegate,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    var yy:CGFloat = 0, margin:CGFloat = 0
    
    var audioRecorder:AVAudioRecorder!
    var  playerAudio: AVAudioPlayer!
    
    var webService: WebService = WebService()
    var appConstants: AppConstants = AppConstants()
    var media_base_url : String?
    var galleryData : NSMutableArray = NSMutableArray()
    //var scrollView:UIScrollView?
    var hID:String?
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    let imagePickerController = UIImagePickerController()
    var imagesDownloaded: NSMutableArray = NSMutableArray()
    var backButton:UILabel?
    var galleryRootController = UIApplication.sharedApplication().keyWindow!.rootViewController
    var indicator = UIActivityIndicatorView()
    
    var collectionView: UICollectionView
    
    // var data : NSData //pooja
    
    enum AppError : ErrorType {
        case InvalidResource(String, String)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clearColor()
        initializeVariables()
        drawView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeVariables() {
        yy = 0
        margin = 5 * appConstants.ScreenFactor
    }
    
    func getGalleryDetail(horseID: String){
        //send to server
        hID = horseID
        SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.horseIdForAudio, value: horseID)
        
        showProgress()
        let postString = "action=gallery&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&horse_id="+horseID;
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    return
                }
                
                print("response = \(response)") //getting all videos,audios n images from server here
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        print("mainList = \(mainList)")
                        self.media_base_url = mainList.objectForKey(self.appConstants.media_base_url) as? String
                        let galleryData11: NSArray = (mainList[self.appConstants.gallery])! as! NSArray
                        self.galleryData = galleryData11.mutableCopy() as! NSMutableArray
                        
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(self.galleryData, forKey: "savedArray")
                        
                        print("array count get gallery detail \(self.galleryData.count)")
                        
                        do{
                            try self.setData1()
                        } catch {
                        }
                        
                        self.collectionView.reloadData()
                        
                    }
                }
            }
        }
    }
    
    
    
    func setData1() throws{
        self.collectionView.reloadData()

    }
    
    func setData() throws {
        //Remove every other view if added in scrollview
       // scrollView!.subviews.forEach({ $0.removeFromSuperview() })
        
        var isFrameChanged : Bool = false
        var counter:Int = 0
        var itemSize:CGFloat = self.frame.size.width/2
        if itemSize == 0
        {
            itemSize = 158.0
        }
        print ( "itemsize \(itemSize)")
        
        var xx:CGFloat = 0
        var yy:CGFloat = 0
        
        print("array count in set data \(galleryData.count)")
        for dict in galleryData{
            let dataImage:UIImageView = UIImageView.init(frame: CGRectMake(xx + 5, yy + 5, itemSize - 10 , itemSize - 10))
            dataImage.backgroundColor = UIColor.init(white: 1.0, alpha: 0.3)
            dataImage.userInteractionEnabled = true
            dataImage.tag = counter;
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(moveToDetail(_:)))
            dataImage.addGestureRecognizer(tapRecognizer)
           // scrollView!.addSubview(dataImage)
            
            let mediaSource:String = (dict.objectForKey(appConstants.files) as? String)!
            print(mediaSource)
            let isImage:String = (dict.objectForKey(appConstants.source) as? String)!
            
            if isImage == "1" {
                //Image
                dataImage.downloadedFrom(media_base_url! + mediaSource) //pooja - image
            }else{
                //Audio
                if mediaSource.containsString(".m4a") {
                    
                    
                    let firstElement = galleryData.firstObject
                    //  print("first object \(firstElement)")
                    
                    let file  = firstElement!.objectForKey("files")
                    // print("1st \(file)")
                    let str = file as! String
                    if str.containsString("m4a") && (isFrameChanged == false)
                    {
                        isFrameChanged = true
                        xx = 0.0
                        yy = 0.0
                        print(" frame changed")
                        
                    }
                    else{
                        print(" no change on frame")
                    }
                    
                    dataImage.image = UIImage.init(imageLiteral: "ic_mic.png")
              //      scrollView!.addSubview(dataImage)//pooja
                    
                }else
                { //Video
                    dataImage.image = UIImage.init(imageLiteral: "playbtn_i_new.png")
                    let videoImage: UIImageView = UIImageView.init(frame: CGRectMake(xx + 10 ,yy + 10 ,20 ,20))
                    videoImage.image = UIImage.init(imageLiteral: "playbtn_i_new.png")
               //     scrollView!.addSubview(videoImage)
                    
                    let arr = mediaSource.characters.split{$0 == "/"}.map(String.init)
                    if self.appConstants.isVideoSaved(arr[arr.count - 1]){
                        //Play video by default
                        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0];
                        let filePath="\(documentsPath)/\(arr[arr.count - 1])";
                        self.appConstants.showVideoThumbnail(filePath, imageView: dataImage)
                    }else{
                        do{
                            try self.downloadVideo(media_base_url! + mediaSource, fileName: arr[arr.count - 1], imageView: dataImage)
                        } catch {
                            
                        }
                    }
                }
            }
            if counter%2 == 0 {
                xx += itemSize
                print(" even counter xx,\(xx)")
                print(" odd counter yy,\(yy)")
            }else {
                xx = 0
                yy += itemSize
                print(" even counter xx,\(xx)")
                print(" even counter yy,\(yy)")
            }
            counter += 1
            print("counter\(counter)")
        }
       // scrollView!.contentSize = CGSizeMake(self.frame.size.width ,yy + 1.5*itemSize)
    }
    
    
    func drawView() {
        var xx:CGFloat = 0
        var counter:Int = 1
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 60, height: 60)
        
        //    let myCollectionView:UICollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView = UICollectionView.init(frame: CGRectMake(0, self.frame.size.height * 0.14, self.frame.size.width , self.frame.size.height * 0.89))
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
        collectionView.backgroundColor = UIColor.whiteColor()
        self.addSubview(collectionView)
        
        
        let topItemWidth:CGFloat = self.frame.size.width/4
        let imageArray: [String] = ["ic_picture.png","ic_picture.png","ic_mic.png","ic_share.png"]
        let nameArray: [String] = ["Photo","Video","Audio","Share"]
        for imageName in imageArray {
            let itemLayout =  UIView.init(frame: CGRectMake(xx, 0, topItemWidth , self.frame.size.height * 0.1))
            itemLayout.backgroundColor = UIColor.clearColor()
            itemLayout.userInteractionEnabled = true
            itemLayout.tag = counter
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(itemTapped(_:)))
            itemLayout.addGestureRecognizer(tapRecognizer)
            self.addSubview(itemLayout)
            
            let itemImage: UIImageView = UIImageView.init(frame: CGRectMake(0,0,self.frame.size.height * 0.05 ,self.frame.size.height * 0.05))
            itemImage.center = CGPointMake(topItemWidth/2, itemLayout.center.y)
            itemImage.image = UIImage.init(imageLiteral: imageName)
            itemLayout.addSubview(itemImage)
            
            let itemLabel : UILabel = UILabel()
            itemLabel.frame = CGRectMake(0,itemImage.frame.origin.y + itemImage.frame.size.height ,topItemWidth,10 * appConstants.ScreenFactor)
            itemLabel.textAlignment = NSTextAlignment.Center
            itemLabel.text = nameArray[counter - 1]
            styleLabels(itemLabel)
            itemLayout.addSubview(itemLabel)
            counter+=1
            xx += topItemWidth
        }
        
        let line: UIView = UIView()
        line.frame = CGRectMake(0 ,  self.frame.size.height * 0.13 , self.frame.size.width, 1)
        line.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        self.addSubview(line)
        
    }
    
    func itemTapped(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!
        switch viewTouched.tag {
        case 1:
            //"Photo"
            self.showPhotoSelectionOptions()
            break
        case 2:
            //"Video"
            self.showVideoSelectionOptions()
            break
        case 3:
            //"Video"
            self.showAudioSelectionOptions()
            break
        case 4:
            //"Share"
            self.subviews.forEach({ $0.removeFromSuperview() })
            
            if AppConstants.CurrentScreen == appConstants.GalleryScreen {
                AppConstants.CurrentScreen = appConstants.ShareScreenFromGalleryScreen
            }else  {
                AppConstants.CurrentScreen = appConstants.ShareScreenFromGalleryScreenFromHorseDetail
            }
            
            let shareGalleryItemsView:ShareGalleryItemsView = ShareGalleryItemsView(frame : self.frame)
            shareGalleryItemsView.backgroundColor = UIColor.clearColor()
            shareGalleryItemsView.media_base_url = self.media_base_url!
            shareGalleryItemsView.galleryData = self.galleryData
            shareGalleryItemsView.setData(self.galleryData)
            self.addSubview(shareGalleryItemsView)
            break
        default:
            break
        }
    }
    
    func showAudioSelectionOptions()
        
    {
        let actionSheetController: UIAlertController = UIAlertController(title: "Option to select", message: nil, preferredStyle: .ActionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let takeAudioActionButton: UIAlertAction = UIAlertAction(title: "Take Audio", style: .Default)
        { action -> Void in
            
            let controller = AudioRecorderViewController()
            //  playerAudio: AVAudioPlayer?
            // self.playerAudio!.stop()
            controller.audioRecorderDelegate = self
            self.vc!.presentViewController(controller, animated: true, completion: nil)
            
            //  self.subviews.forEach({ $0.removeFromSuperview() })
            //  AppConstants.CurrentScreen = self.appConstants.GalleryScreen
            //   let addGalleryAudioView : GalleryAudioView = GalleryAudioView(frame: self.frame)
            //   self.addSubview(addGalleryAudioView)
            
            
        }
        actionSheetController.addAction(takeAudioActionButton)
        
        
        //        let audioActionButton: UIAlertAction = UIAlertAction(title: "Audio from Gallery", style: .Default)
        //        { action -> Void in
        //            self.imagePickerController.sourceType = .SavedPhotosAlbum
        //            self.imagePickerController.mediaTypes = [kUTTypeMPEG4Audio as NSString as String]
        //            self.imagePickerController.allowsEditing = true
        //            self.imagePickerController.delegate = self
        //            self.vc!.presentViewController(self.imagePickerController, animated: true, completion: nil)
        //        }
        //        actionSheetController.addAction(audioActionButton)
        
        vc!.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    func audioRecorderViewControllerDismissed(withFileURL fileURL: NSURL?) {
        // do something with fileURL
        self.vc!.dismissViewControllerAnimated(true, completion: nil)
        
        
    }
    
    func showPhotoSelectionOptions(){
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Option to select", message: nil, preferredStyle: .ActionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let takePicActionButton: UIAlertAction = UIAlertAction(title: "Take Picture", style: .Default)
        { action -> Void in
            
            if (UIImagePickerController.isSourceTypeAvailable(.Camera)) {
                if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
                    self.imagePickerController.allowsEditing = false
                    self.imagePickerController.sourceType = .Camera
                    self.imagePickerController.delegate = self
                    self.imagePickerController.cameraCaptureMode = .Photo
                    self.vc!.presentViewController(self.imagePickerController, animated: true, completion: nil)
                } else {
                    self.appConstants.showAlert("Rear camera doesn't exist", message:"Application cannot access the camera.", controller: self.vc!)
                }
            } else {
                self.appConstants.showAlert("Camera inaccessable", message:"Application cannot access the camera.", controller: self.vc!)
            }
        }
        actionSheetControllerIOS8.addAction(takePicActionButton)
        
        let PicActionButton: UIAlertAction = UIAlertAction(title: "Picture from Gallery", style: .Default)
        { action -> Void in
            self.imagePickerController.sourceType = .PhotoLibrary
            self.imagePickerController.delegate = self
            self.vc!.presentViewController(self.imagePickerController, animated: true, completion: nil)
            
        }
        actionSheetControllerIOS8.addAction(PicActionButton)
        
        vc!.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func showVideoSelectionOptions(){
        let actionSheetController: UIAlertController = UIAlertController(title: "Option to select", message: nil, preferredStyle: .ActionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let takeVideoActionButton: UIAlertAction = UIAlertAction(title: "Take Video", style: .Default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.Camera) == false {
                self.appConstants.showAlert("Camera inaccessable", message:"Application cannot access the camera.", controller: self.vc!)
                return
            }
            
            self.imagePickerController.sourceType = .Camera
            self.imagePickerController.mediaTypes = [kUTTypeMovie as NSString as String]
            self.imagePickerController.allowsEditing = false
            self.imagePickerController.videoQuality = UIImagePickerControllerQualityType.TypeMedium//pooja
            self.imagePickerController.delegate = self
            self.imagePickerController.showsCameraControls = true
            self.vc!.presentViewController(self.imagePickerController, animated: true, completion: nil)
        }
        actionSheetController.addAction(takeVideoActionButton)
        
        
        let videoActionButton: UIAlertAction = UIAlertAction(title: "Video from Gallery", style: .Default)
        { action -> Void in
            self.imagePickerController.sourceType = .SavedPhotosAlbum
            self.imagePickerController.mediaTypes = [kUTTypeMovie as NSString as String]
            self.imagePickerController.allowsEditing = true
            self.imagePickerController.delegate = self
            self.vc!.presentViewController(self.imagePickerController, animated: true, completion: nil)
        }
        actionSheetController.addAction(videoActionButton)
        
        vc!.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    func moveToDetail(gestureRecognizer: UITapGestureRecognizer){
        let viewTouched: UIView = gestureRecognizer.view!
        imagesDownloaded.removeAllObjects()
        
        for i in 0  ..< galleryData.count  {
            let dict:NSDictionary = galleryData[i] as! NSDictionary
            let mediaSource:String = (dict.objectForKey(appConstants.files) as? String)!
            if !mediaSource.containsString(".mp4") && !mediaSource.containsString(".m4a") {
                imagesDownloaded[i] = UIImage.init(imageLiteral: "logo.png")
                //  loadImageFromUrl(media_base_url! + mediaSource, index: i)
                loadImageFromUrl(media_base_url! + mediaSource, index: i)
            }else{
                imagesDownloaded[i] = UIImage.init(named: "playbtn_i.png")!
            }
        }
        
        let gallery = SwiftPhotoGallery(delegate: self, dataSource: self)
        
        gallery.backgroundColor = UIColor.blackColor()
        gallery.pageIndicatorTintColor = UIColor.grayColor().colorWithAlphaComponent(0.5)
        gallery.currentPageIndicatorTintColor = UIColor.whiteColor()
        gallery.currentPage = viewTouched.tag
        self.galleryRootController = gallery
        
        vc!.presentViewController(gallery, animated: false, completion: { () -> Void in
            gallery.currentPage = viewTouched.tag
        })
        
        if backButton != nil {
            backButton?.removeFromSuperview()
        }
        
        backButton = UILabel.init(frame: CGRectMake(gallery.view.frame.size.width - 80 * appConstants.ScreenFactor, 21 * appConstants.ScreenFactor, 80 * appConstants.ScreenFactor,20 * appConstants.ScreenFactor))
        backButton!.text = "Cancel"
        backButton!.font = UIFont.init(name: "Helvetica", size: 13 * appConstants.ScreenFactor)
        backButton!.backgroundColor = UIColor.clearColor()
        backButton!.textAlignment = NSTextAlignment.Center
        backButton?.textColor = UIColor.whiteColor()
        backButton!.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(cancel(_:)))
        backButton!.addGestureRecognizer(tapRecognizer)
        gallery.view.addSubview(backButton!)
    }
    
    func cancel(gestureRecognizer: UITapGestureRecognizer){
        if backButton != nil {
            backButton?.removeFromSuperview()
        }
        vc!.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    private func playVideo(path : String) throws {
        let player = AVPlayer(URL: NSURL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.galleryRootController!.presentViewController(playerController, animated: true) {
            player.play()
        }
    }
    
    func styleLabels(label: UILabel){
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.init(name: "Helvetica", size: 7 * appConstants.ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
    }
    
    func downloadVideo(videoImageUrl: String, fileName: String, imageView: UIImageView) throws{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            let url = NSURL(string: videoImageUrl);
            let urlData = NSData(contentsOfURL: url!);
            if(urlData != nil)
            {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0];
                let filePath="\(documentsPath)/\(fileName)";
                dispatch_async(dispatch_get_main_queue(), {
                    urlData?.writeToFile(filePath, atomically: true);
                    PHPhotoLibrary.sharedPhotoLibrary().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideoAtFileURL(NSURL(fileURLWithPath: filePath))
                    }) { completed, error in
                        if completed {
                            if (AppConstants.CurrentScreen == self.appConstants.GalleryScreen){
                                self.appConstants.showVideoThumbnail(filePath, imageView: imageView)
                                print("Video is saved!")
                            }
                        }
                    }
                })
            }
        })
    }
    
    //Picture controller delegates
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePickerController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        self.imagePickerController.dismissViewControllerAnimated(true) {
            // 3
            if mediaType == kUTTypeMovie {
                
                guard let path = (info[UIImagePickerControllerMediaURL] as! NSURL).path else { return }
                
                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path) {
                    UISaveVideoAtPathToSavedPhotosAlbum(path, self, #selector(GalleryView.video(_:didFinishSavingWithError:contextInfo:)), nil)
                    
                }
            }else{
                //Image
                let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
                let imageData:NSData = UIImageJPEGRepresentation(selectedImage,0.5)!
                var strBase64:String = imageData.base64EncodedStringWithOptions(.EncodingEndLineWithLineFeed)
                strBase64 = strBase64.stringByReplacingOccurrencesOfString("+", withString: "%2B")
                self.uploadGalleryDataInServer("1", ext: "jpeg", content: strBase64)
            }
        }
    }
    
    func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        if let _ = error {
        }else{
            print("videoPath = \(videoPath)")
            let data = NSData(contentsOfFile: videoPath as String)!
            
            var fileSize : UInt64 = 0
            
            do {
                let attr : NSDictionary? = try NSFileManager.defaultManager().attributesOfItemAtPath(videoPath as String)
                
                if let _attr = attr {
                    fileSize = _attr.fileSize();
                    
                    print("bytes = \(fileSize)")
                    NSLog("bytes")
                    
                }
            } catch {
                print("Error: \(error)")
            }
            
            var strBase64:String = NSData(contentsOfFile: videoPath as String)!.base64EncodedStringWithOptions(.EncodingEndLineWithLineFeed)
            strBase64 = strBase64.stringByReplacingOccurrencesOfString("+", withString: "%2B")
            self.uploadGalleryDataInServer("2", ext: ".mp4", content: strBase64)
        }
    }
    
    // MARK: SwiftPhotoGalleryDataSource Methods
    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {
        return galleryData.count
    }
    
    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {
        return self.imagesDownloaded[forIndex] as? UIImage
    }
    
    // MARK: SwiftPhotoGalleryDelegate Methods
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        let dict:NSDictionary = galleryData[gallery.currentPage] as! NSDictionary
        let mediaSource:String = (dict.objectForKey(appConstants.files) as? String)!
        if mediaSource.containsString(".mp4") {
            
            let arr = mediaSource.characters.split{$0 == "/"}.map(String.init)
            if self.appConstants.isVideoSaved(arr[arr.count - 1]){
                do {
                    try playVideo(self.appConstants.fileInDocumentsDirectory(arr[arr.count - 1]))
                } catch AppError.InvalidResource(let name, let type) {
                    debugPrint("Could not find resource \(name).\(type)")
                } catch {
                    debugPrint("Generic error")
                }
            }else{
                let viewController = PlayVideoViewController()
                viewController.moviePath = media_base_url! + mediaSource
                self.galleryRootController!.presentViewController(viewController, animated: true, completion: nil)
            }
        }
        if mediaSource.containsString(".m4a") {
            let controller = AudioRecorderViewController()
            controller.audioRecorderDelegate = self
            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.audioString, value: media_base_url! + mediaSource)
            self.galleryRootController!.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    func loadImageToView(data: NSData, index: Int) throws {
        if (AppConstants.CurrentScreen == appConstants.GalleryScreen){
            
            if let imageUrl = UIImage(data: data) {
                self.imagesDownloaded[index] = UIImage(data: data)!
                (self.galleryRootController! as! SwiftPhotoGallery).reload()
                imageUrl  // you can use your imageUrl UIImage (note: imageUrl it is not an optional here)
            }
            
        }
    }
    func loadImageFromUrl(url: String, index: Int){
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
            if let data = responseData{
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if( data.length > 100){
                        do {
                            try self.loadImageToView(data, index: index)
                        } catch {
                            debugPrint("Generic error")
                        }
                    }
                })
            }
        }
        
        // Run task
        task.resume()
    }
    
    func uploadGalleryDataInServer(source: String, ext: String, content: String){
        showProgress()
        //send to server
        var postString : NSString
    postString = "action=galleryadd&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&horse_id="+(SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.horseIdForAudio))+"&source="+source+"&ext="+ext+"&content="+content;
        
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                
                if error != nil{
                    return
                }
                print("response image byte= \(response)")
                NSLog("this will print with dates- bytes")
                print("time taken to upload video (bytes)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        
                        print("mainList = \(mainList)")
                        self.media_base_url = mainList.objectForKey(self.appConstants.media_base_url) as? String
                        let toAdd:NSDictionary = (mainList[self.appConstants.gallery])! as! NSDictionary
                        
                        self.galleryData = self.galleryData.mutableCopy() as! NSMutableArray
                        self.galleryData.insertObject(toAdd, atIndex: 0)
                        //Update the list by removing all view and adding new one...
                        print("array count \(self.galleryData.count)")
                        do{
                            try self.setData1()
                        } catch {
                        }
                        //  }//pooja
                        
                    }
                }
            }
        }
    }
    
    func uploadGalleryDataInServerAudio(source: String, ext: String, content: String){
      //  scrollView!.subviews.forEach({ $0.removeFromSuperview() })
        showProgress()
        //send to server
        let postString = "action=galleryadd&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&horse_id="+(SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.horseIdForAudio))+"&source="+source+"&ext="+ext+"&content="+content;
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            
            
            dispatch_async(dispatch_get_main_queue()) { Void in
                self.hideProgress()
                
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        let defaults = NSUserDefaults.standardUserDefaults()
                        let finalArray = defaults.valueForKey("savedArray")
                        
                        print("array  \(finalArray!.count)")
                        
                        //   User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        print("mainList = \(mainList)")
                        self.media_base_url = mainList.objectForKey(self.appConstants.media_base_url) as? String
                        let toAdd:NSDictionary = (mainList[self.appConstants.gallery])! as! NSDictionary
                        self.galleryData = finalArray!.mutableCopy() as! NSMutableArray
                        self.galleryData.insertObject(toAdd, atIndex: 0)
                        
                        print("array count \(self.galleryData.count)")
                        print("galleryData = \(self.galleryData)")
                        
                        
                        do{
                            try self.setData1()
                        } catch {
                        }
                        
                    }
                }
            }
        }
    }
    
    
    func audioUploadToView(horseID: String){
        //send to server
        hID = horseID
        SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.horseIdForAudio, value: horseID)
        
        showProgress()
        let postString = "action=gallery&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&horse_id="+horseID;
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    return
                }
                
                print("response = \(response)") //getting all videos,audios n images from server here
                
                let data = self.appConstants.convertStringToDictionaryAudio(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        print("mainList = \(mainList)")
                        self.media_base_url = mainList.objectForKey(self.appConstants.media_base_url) as? String
                        let galleryData11: NSArray = (mainList[self.appConstants.gallery])! as! NSArray
                        self.galleryData = galleryData11.mutableCopy() as! NSMutableArray
                        
                    do{
                            try self.setData1()
                        } catch {
                        }
                        
                    }
                }
            }
        }
    }
    
    
    
    
    func showProgress() {
        // indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.galleryData.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        cell.backgroundColor = UIColor.orangeColor()
        return cell
    }
}*/


    
    


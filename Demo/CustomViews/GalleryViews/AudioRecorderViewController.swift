//
//  AudioRecorderViewController.swift
//  AudioRecorderViewControllerExample
//
//  Created by Ben Dodson on 19/10/2015.
//  Copyright © 2015 Dodo Apps. All rights reserved.
//

import UIKit
import AVFoundation

protocol AudioRecorderViewControllerDelegate: class {
    func audioRecorderViewControllerDismissed(withFileURL fileURL: NSURL?)
}


class AudioRecorderViewController: UINavigationController {
    
    internal let childViewController = AudioRecorderChildViewController()
    weak var audioRecorderDelegate: AudioRecorderViewControllerDelegate?
    var statusBarStyle: UIStatusBarStyle = .Default
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        statusBarStyle = UIApplication.sharedApplication().statusBarStyle
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().setStatusBarStyle(statusBarStyle, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blackColor()
        childViewController.audioRecorderDelegate = audioRecorderDelegate
        viewControllers = [childViewController]
        
        navigationBar.barTintColor = UIColor.blackColor()
        navigationBar.tintColor = UIColor.whiteColor()
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    
    
    // MARK: AudioRecorderChildViewController
    
    internal class AudioRecorderChildViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
        
        var saveButton: UIBarButtonItem!
        @IBOutlet weak var timeLabel: UILabel!
        @IBOutlet weak var recordButton: UIButton!
        @IBOutlet weak var recordButtonContainer: UIView!
        @IBOutlet weak var playButton: UIButton!
        weak var audioRecorderDelegate: AudioRecorderViewControllerDelegate?
        var doChangeDefaultRoute: UInt32 = 1

        var timeTimer: NSTimer?
        var milliseconds: Int = 0
        var appConstants: AppConstants = AppConstants()
        var webService: WebService = WebService()
        var hID:String?
        var media_base_url : String?
        var galleryData : NSMutableArray = NSMutableArray()
        var recorderURLString : String?
        var recorder: AVAudioRecorder!
        var player: AVAudioPlayer?
        var outputURL: NSURL
        var audioRecorder:AVAudioRecorder!
        
        init() {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
            let outputPath = documentsPath.stringByAppendingPathComponent("\(NSUUID().UUIDString).m4a")
            outputURL = NSURL(fileURLWithPath: outputPath)
            super.init(nibName: "AudioRecorderViewController", bundle: nil)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func viewDidLoad() {
            title = "Audio Recorder"
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(AudioRecorderChildViewController.dismiss(_:)))
            edgesForExtendedLayout = .None
            
            saveButton = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: #selector(AudioRecorderChildViewController.saveAudio(_:)))
            navigationItem.rightBarButtonItem = saveButton
            saveButton.enabled = false
            
            let settings = [AVFormatIDKey: NSNumber(unsignedInt: kAudioFormatMPEG4AAC), AVSampleRateKey: NSNumber(integer: 44100), AVNumberOfChannelsKey: NSNumber(integer: 2),AVEncoderAudioQualityKey: AVAudioQuality.High.rawValue]
            try! recorder = AVAudioRecorder(URL: outputURL, settings: settings)
            recorder.delegate = self
            recorder.prepareToRecord()
            
            recordButton.layer.cornerRadius = 4
            recordButtonContainer.layer.cornerRadius = 25
            recordButtonContainer.layer.borderColor = UIColor.whiteColor().CGColor
            recordButtonContainer.layer.borderWidth = 3
            
            
            let haveAudio = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.audioString)
            
          //  print( "We saved this audio: \( haveAudio )" )
            if(haveAudio == "")
            {
                let url1 = NSURL(string: haveAudio)
                print(url1)
            }
            else{
                recordButton.enabled = false
                recordButtonContainer.alpha = 0.25
                saveButton.enabled = false
                
                let url = NSURL(string: haveAudio)
                
                if (url == nil)
                {
                    downloadFileFromURL(outputURL)
                }
                else{//here url is coming nil-issue 1
                downloadFileFromURL(url!)
                }
                
              //  downloadFileFromURL(outputURL)

                print(outputURL)
                print("the url = \(url)")
            }
        }
        
        func downloadFileFromURL(url:NSURL){
            var downloadTask:NSURLSessionDownloadTask
             let haveAudio = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.audioString)
            let url2 = NSURL(string: haveAudio)
            
            downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(url, completionHandler: { (URL, response, error) -> Void in
                
                 self.playAudio(URL!)
                
            })
            
            downloadTask.resume()
            
        }
         
        func playAudio(url:NSURL) {
            print("playing \(url)")
            
            do {
                
                try player = AVAudioPlayer(contentsOfURL: url)
            }
            catch let error as NSError {
                NSLog("error: \(error)")
            }
            
            player?.delegate = self
            player?.play()

            

        }
        
        override func viewDidAppear(animated: Bool) {
            super.viewDidAppear(animated)
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                try AVAudioSession.sharedInstance().setActive(true)
                try! AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.Speaker)

            }
            catch let error as NSError {
                NSLog("Error: \(error)")
            }
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "stopRecording:", name: UIApplicationDidEnterBackgroundNotification, object: nil)
        }
        
        override func viewWillDisappear(animated: Bool) {
            super.viewWillDisappear(animated)
            NSNotificationCenter.defaultCenter().removeObserver(self)
        }
        
        func dismiss(sender: AnyObject) {
            cleanup()
            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.audioString, value: "")

            audioRecorderDelegate?.audioRecorderViewControllerDismissed(withFileURL: nil)
        }
        
        func saveAudio(sender: AnyObject) {
            self.audioRecorderDelegate?.audioRecorderViewControllerDismissed(withFileURL: outputURL)

            print("outputURL = \(outputURL)")
            let dataToUpload : NSData = NSData(contentsOfURL: outputURL)!
            if let strbase64 : String = dataToUpload.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                        {
                          //  print("base64Encoded:  \(strbase64)")
                          let  strBase64 = strbase64.stringByReplacingOccurrencesOfString("+", withString: "%2B")
                           
                            self.audioRecorderDelegate?.audioRecorderViewControllerDismissed(withFileURL: nil)
                            
                           let horseGalleryView:GalleryView = GalleryView()
                           horseGalleryView.uploadGalleryDataInServerAudio("2", ext: "m4a", content: strBase64)
                           
                    }
           // cleanup()
        }
        
        
        @IBAction func toggleRecord(sender: AnyObject) {
            
            timeTimer?.invalidate()
            
            if recorder.recording {
                recorder.stop()
            } else {
                milliseconds = 0
                timeLabel.text = "00:00.00"
                timeTimer = NSTimer.scheduledTimerWithTimeInterval(0.0167, target: self, selector: "updateTimeLabel:", userInfo: nil, repeats: true)
                recorder.deleteRecording()
                recorder.record()
            }
            
            updateControls()
            
        }
        
        func stopRecording(sender: AnyObject) {
            if recorder.recording {
                toggleRecord(sender)
            }
        }
        
        func cleanup() {
            timeTimer?.invalidate()
            if recorder.recording {
                recorder.stop()
                recorder.deleteRecording()
            }
            if let player = player {
                player.stop()
                self.player = nil
            }
            
        }
        
        @IBAction func play(sender: AnyObject) {
            
            if let player = player {
                player.stop()
                self.player = nil
                updateControls()
                return
            }
            
            do {
                
                print("outputURL to play is: \(outputURL)")

                try player = AVAudioPlayer(contentsOfURL: outputURL)
            }
            catch let error as NSError {
                NSLog("error: \(error)")
            }
            
            player?.delegate = self
            player?.play()
            
            updateControls()
        }
        
        
        func updateControls() {
            
            UIView.animateWithDuration(0.2) { () -> Void in
                self.recordButton.transform = self.recorder.recording ? CGAffineTransformMakeScale(0.5, 0.5) : CGAffineTransformMakeScale(1, 1)
            }
            
            if let _ = player {
                playButton.setImage(UIImage(named: "StopButton"), forState: .Normal)
                recordButton.enabled = false
                recordButtonContainer.alpha = 0.25
            } else {
                playButton.setImage(UIImage(named: "PlayButton"), forState: .Normal)
                recordButton.enabled = true
                recordButtonContainer.alpha = 1
            }
            
            playButton.enabled = !recorder.recording
            playButton.alpha = recorder.recording ? 0.25 : 1
            saveButton.enabled = !recorder.recording
            
        }
        
        
        
        
        // MARK: Time Label
        
        func updateTimeLabel(timer: NSTimer) {
            milliseconds += 1
            let milli = (milliseconds % 60) + 39
            let sec = (milliseconds / 60) % 60
            let min = milliseconds / 3600
            timeLabel.text = NSString(format: "%02d:%02d.%02d", min, sec, milli) as String
        }
        
        
        // MARK: Playback Delegate
        
        func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
            
            let haveAudio = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.audioString)
            
            print( "We saved this audio: \( haveAudio )" )
            if(haveAudio == "")
            {
                self.player = nil
                updateControls()

            }
            else{
                SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.audioString, value: "")
                
                self.audioRecorderDelegate?.audioRecorderViewControllerDismissed(withFileURL: nil)

            }
                   }
        
        func uploadGalleryDataInServer(source: String, ext: String, content: String){
            //send to server
            print(" audio call>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            let postString = "action=galleryadd&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&horse_id="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.horseIdForAudio)+"&source="+source+"&ext="+ext+"&content="+content;
            
            webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    
                    if error != nil{
                        return
                    }
                    
                    print("response = \(response)")
                    self.audioRecorderDelegate?.audioRecorderViewControllerDismissed(withFileURL: nil)
                    
                    let horseGalleryView:GalleryView = GalleryView()
                    
                    horseGalleryView.getGalleryDetail(SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.horseIdForAudio))
                    
                    let data = self.appConstants.convertStringToDictionary(response)
                    if data != nil {
                        if ((data?[self.appConstants.ErrorCode]) != nil) {
                            //Error
                            return
                        }else{
                            //User Data
                            let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                            print("mainList = \(mainList)")
                            self.media_base_url = mainList.objectForKey(self.appConstants.media_base_url) as? String
                            let toAdd:NSDictionary = (mainList[self.appConstants.gallery])! as! NSDictionary
                            self.galleryData = self.galleryData.mutableCopy() as! NSMutableArray
                            self.galleryData.insertObject(toAdd, atIndex: 0)
                            
                            self.audioRecorderDelegate?.audioRecorderViewControllerDismissed(withFileURL: nil)

                            let horseGalleryView:GalleryView = GalleryView()
                            horseGalleryView.getGalleryDetail(SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.horseIdForAudio))
                          //  self.addSubview(horseGalleryView)

                            //Update the list by removing all view and adding new one...
                                                       
                        }
                    }
                }
            }
        }
    }
    
    
    
    
}


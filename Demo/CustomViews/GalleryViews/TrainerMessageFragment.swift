//
//  TrainerMessageFragment.swift
//  Demo
//
//  Created by Truway India on 12/04/17.
//  Copyright © 2017 BreakfastBay. All rights reserved.
// This class will show all message from all trainer


import UIKit
//import Quickblox

class TrainerMessageFragment: UIView, UITableViewDelegate, UITableViewDataSource, ListItemCallback {
    var fromWhere: String = ""
    
    var tableView: UITableView  =   UITableView()
    
    
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var horseList:NSArray = NSArray()
    var cellIdendifier: String = "HorseCell"
    var filtered:NSMutableArray = NSMutableArray()
    var indicator = UIActivityIndicatorView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        searchBar.frame       =   CGRectMake(0, 0, self.frame.size.width, 35 * appConstants.ScreenFactor)
//        searchBar.showsCancelButton = false
//        searchBar.backgroundColor = UIColor.clearColor()
//        searchBar.tintColor = UIColor.blackColor()
//        searchBar.translucent = true
//        searchBar.alpha = 1
//        searchBar.backgroundImage = UIImage()
//        searchBar.barTintColor = UIColor.clearColor()
//        
//        self.addSubview(searchBar)
        
        tableView.frame             =   CGRectMake(0,2 * appConstants.ScreenFactor , self.frame.size.width,self.frame.size.height+30 * appConstants.ScreenFactor) //- 2 * appConstants.ScreenFactor)
        tableView.delegate          =   self
        tableView.dataSource        =   self
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.SingleLine;
        tableView.separatorColor    = UIColor.whiteColor().colorWithAlphaComponent(0.3);
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        self.addSubview(tableView)
        
        getOwnerMessageFromServer()
    }
    
    func dismissKeyboard() {
        self.endEditing(true)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getOwnerMessageFromServer() {
        //send to server
        showProgress()
        let postString = "action=getTrainerMessage&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID);
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        
                       	
                        //User Data
                        let mainList:NSDictionary = (data)! as NSDictionary
                        self.horseList = mainList.objectForKey("messages") as! NSArray
                        self.filtered = self.horseList.mutableCopy() as! NSMutableArray
                        print("messages = \(self.horseList)")
                        let count: String = String(self.horseList.count)
                        SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.horse_count, value: count)
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
        func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 35 * appConstants.ScreenFactor
            
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = MessageTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        
        if(self.filtered.count > 0){
            cell.horseSelected = self
            cell.setData(self.filtered[indexPath.row] as! NSDictionary)
        }
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        dismissKeyboard()
    }
    
    func itemSelected(model: NSDictionary) {
        self.subviews.forEach({ $0.removeFromSuperview() })
        AppConstants.CurrentHorseID = model.objectForKey(appConstants._id) as! String
        dismissKeyboard()
        
        if fromWhere == "HORSE_DETAIL" {
            AppConstants.CurrentScreen = appConstants.HorseDetailingScreen
            let horseDetail:HorseDetailView = HorseDetailView(frame : self.frame)
            //horseDetail.backgroundColor = UIColor().HexToColor(appConstants.font_black_color, alpha: 1.0)
            horseDetail.backgroundColor = UIColor.clearColor()
            horseDetail.getHorseDetail(model.objectForKey(appConstants._id) as! String)
            self.addSubview(horseDetail)
        }else if fromWhere == "GALLERY" {
            AppConstants.CurrentScreen = appConstants.GalleryScreen
            let horseGalleryView:GalleryView = GalleryView(frame : self.frame)
            horseGalleryView.getGalleryDetail(model.objectForKey(appConstants._id) as! String)
            self.addSubview(horseGalleryView)
        }else {
            AppConstants.CurrentScreen = appConstants.DailyActivityScreen
            let horseDailyActivity:DailyActivityListing = DailyActivityListing(frame : self.frame)
            //  horseDailyActivity.backgroundColor = UIColor.whiteColor()
            horseDailyActivity.backgroundColor = UIColor.clearColor()
            horseDailyActivity.fromWhere = "DAILY_ACT"
            horseDailyActivity.getHorseDetail(model.objectForKey(appConstants._id) as! String)
            horseDailyActivity.getHorseDailyActivityList(model.objectForKey(appConstants._id) as! String)
            self.addSubview(horseDailyActivity)
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        dismissKeyboard()
    }
    
    //Search bar delegate methods
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        filtered.removeAllObjects()
        if searchText.isEmpty {
            filtered = self.horseList.mutableCopy() as! NSMutableArray
        }else {
            for dict in self.horseList {
                let tmp: NSString = dict.objectForKey(self.appConstants.name) as! String
                let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
                if range.location != NSNotFound {
                    //add in filter array
                    filtered.addObject(dict)
                }
            }
        }
        self.tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarBookmarkButtonClicked(searchBar: UISearchBar){
        //Tells the delegate that the bookmark button was tapped.
        
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar){
        //Tells the delegate that the cancel button was tapped.
        dismissKeyboard()
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        //Tells the delegate that the search button was tapped.
        dismissKeyboard()
    }
    func searchBarResultsListButtonClicked(searchBar: UISearchBar){
        //Tells the delegate that the search results list button was tapped.
        dismissKeyboard()
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    
    
}

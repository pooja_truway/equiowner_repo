//
//  ShareGalleryItemsView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 06/10/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class ShareGalleryItemsView: UIView, UIScrollViewDelegate {
    var yy:CGFloat = 0, margin:CGFloat = 0
    
    var webService: WebService = WebService()
    var appConstants: AppConstants = AppConstants()
    var media_base_url : String?
    var galleryData : NSMutableArray = NSMutableArray()
    var scrollView:UIScrollView?
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    var shareButton:UIButton?
    var tickArray : NSMutableArray = NSMutableArray()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        initializeVariables()
        drawView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeVariables() {
        yy = 0
        margin = 5 * appConstants.ScreenFactor
    }
    
    func setData(galleryData : NSMutableArray) {
        self.galleryData = galleryData.mutableCopy() as! NSMutableArray
        var counter:Int = 0
        let itemSize:CGFloat = self.frame.size.width/2
        
        var xx:CGFloat = 0
        var yy:CGFloat = 0
        
        for dict in galleryData{
            let changedData: NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
            changedData.setValue(false, forKey: appConstants.selected)
            self.galleryData.replaceObjectAtIndex(counter, withObject: changedData)
            
            let dataImage:UIImageView = UIImageView.init(frame: CGRectMake(xx + 5, yy + 5, itemSize - 10 , itemSize - 10))
            dataImage.backgroundColor = UIColor.init(white: 1.0, alpha: 0.3)
            dataImage.userInteractionEnabled = true
            dataImage.tag = counter;
            dataImage.image = UIImage.init(imageLiteral: "playbtn_i_new.png")
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tickUntick(_:)))
            dataImage.addGestureRecognizer(tapRecognizer)
            scrollView!.addSubview(dataImage)
            
            let tick:UIImageView = UIImageView.init(frame: CGRectMake(xx + 10, yy + 10, 30 , 30))
            tick.image = UIImage.init(imageLiteral: "accept_i_white.png")
            tick.alpha = 0.0
            scrollView!.addSubview(tick)
            tickArray.addObject(tick)

            let mediaSource:String = (dict.objectForKey(appConstants.files) as? String)!
            let isImage:String = (dict.objectForKey(appConstants.source) as? String)!
            
            if isImage == "1" {
                //Image
                dataImage.downloadedFrom(media_base_url! + mediaSource)
            }else{
                //Video
                dataImage.image = UIImage.init(imageLiteral: "playbtn_i_new.png")
            }
            if counter%2 == 0 {
                xx += itemSize
            }else {
                xx = 0
                yy += itemSize
            }
            counter += 1
        }
        
        scrollView!.contentSize = CGSizeMake(self.frame.size.width ,yy + 1.5*itemSize)
    }
    
    func drawView() {
  
        shareButton = UIButton.init(frame: CGRectMake(margin,self.frame.size.height - 26 * appConstants.ScreenFactor - 1.5*margin,self.frame.size.width - 2*margin,26 * appConstants.ScreenFactor))
        shareButton!.layer.cornerRadius = appConstants.CornerRadius
        shareButton!.setTitle("Share", forState: UIControlState.Normal)
        shareButton!.titleLabel?.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        shareButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        shareButton!.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        shareButton!.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(share(_:)))
        shareButton!.addGestureRecognizer(tapRecognizer)
        self.addSubview(shareButton!)
     
        
        scrollView = UIScrollView.init(frame: CGRectMake(0, 0, self.frame.size.width , self.frame.size.height - shareButton!.frame.size.height - 1.5*margin))
        scrollView!.contentSize = self.frame.size
        scrollView!.backgroundColor = UIColor.clearColor()
        scrollView!.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        self.addSubview(scrollView!)
    }
    
    func share(share: UITapGestureRecognizer) {
        var selected : [String] = [String]()
        for dict in galleryData{
            let isSelected: Bool = dict.objectForKey(appConstants.selected) as! Bool
            if isSelected {
                let mediaSource:String = (dict.objectForKey(appConstants.files) as? String)!
                selected.append(media_base_url! + mediaSource)
            }
        }
        
        if selected.count > 0 {
            let activityVC = UIActivityViewController(activityItems: selected, applicationActivities: nil)
            //New Excluded Activities Code
            activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]            
            activityVC.popoverPresentationController?.sourceView = self
            vc!.presentViewController(activityVC, animated: true, completion: nil)
        }else{
            appConstants.showAlert("Whao!", message: "You haven't selected any item to share!", controller: vc!)
        }
    }
    
    func tickUntick(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!

        let changedData: NSMutableDictionary = galleryData.objectAtIndex(viewTouched.tag) as! NSMutableDictionary
        let isSelected: Bool = changedData.objectForKey(appConstants.selected) as! Bool
        
        if isSelected {
            //uselect it if it's selected
            (tickArray.objectAtIndex(viewTouched.tag) as! UIImageView).alpha = 0.0
        }else {
            (tickArray.objectAtIndex(viewTouched.tag) as! UIImageView).alpha = 1.0
        }
        changedData.setValue(!isSelected, forKey: appConstants.selected)
        galleryData.replaceObjectAtIndex(viewTouched.tag, withObject: changedData)
    }
    
    
}

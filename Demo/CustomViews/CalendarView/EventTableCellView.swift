//
//  EventTableCellView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 29/09/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation
import UIKit

class EventTableCellView: UITableViewCell {
    var eventData: NSDictionary = NSDictionary()
    var eventTitle: UILabel = UILabel()
    var eventTime: UILabel = UILabel()
    let timeIcon : UIImageView = UIImageView()
    var eventPlace: UILabel = UILabel()
    let placeIcon : UIImageView = UIImageView()
    let vline: UIView = UIView()
    let blankView: UIView = UIView()

    var appConstants: AppConstants = AppConstants()
    let m1 : UIImageView = UIImageView()
    let m2 : UIImageView = UIImageView()
    let m3 : UIImageView = UIImageView()
    let memberLayout : UIView = UIView()
    let baseUrl: String = "http://www.webapponline.mobi/"

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        
        vline.frame = CGRectZero
        vline.backgroundColor = UIColor().HexToColor(appConstants.button_color)
        contentView.addSubview(vline)
        
        eventTitle = UILabel.init(frame: CGRectZero)
        eventTitle.textColor = UIColor.whiteColor()
        eventTitle.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
        eventTitle.backgroundColor = UIColor.clearColor()
        contentView.addSubview(eventTitle)
       
        timeIcon.image = UIImage.init(imageLiteral: "clock.png")
        contentView.addSubview(timeIcon)
        
        placeIcon.image = UIImage.init(imageLiteral: "map.png")
        contentView.addSubview(placeIcon)
        
        eventTime = UILabel.init(frame: CGRectZero)
        eventTime.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.7)
        eventTime.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        eventTitle.backgroundColor = UIColor.clearColor()
        contentView.addSubview(eventTime)
        
        eventPlace = UILabel.init(frame: CGRectZero)
        eventPlace.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.7)
        eventPlace.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        eventPlace.backgroundColor = UIColor.clearColor()
        contentView.addSubview(eventPlace)
        
        contentView.addSubview(memberLayout)
        memberLayout.addSubview(m1)
        memberLayout.addSubview(m2)
        memberLayout.addSubview(m3)

        contentView.addSubview(blankView)
        
        contentView.backgroundColor = UIColor().HexToColor("#393941")
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        vline.frame = CGRectMake(0 , 0 , 5, self.bounds.size.height)
        
        memberLayout.frame = CGRectMake(self.bounds.size.width - 32 * appConstants.ScreenFactor, 2, 30 * appConstants.ScreenFactor, 9 * appConstants.ScreenFactor)
        
        m3.frame = CGRectMake(1, 0, 9 * appConstants.ScreenFactor, 9 * appConstants.ScreenFactor)
        m2.frame = CGRectMake(11 * appConstants.ScreenFactor, 0, 9 * appConstants.ScreenFactor, 9 * appConstants.ScreenFactor)
        m1.frame = CGRectMake(21 * appConstants.ScreenFactor, 0, 9 * appConstants.ScreenFactor, 9 * appConstants.ScreenFactor)

        m1.backgroundColor = UIColor.clearColor()
        m1.layer.masksToBounds = false
        m1.layer.cornerRadius = m1.frame.size.width/2
        m1.clipsToBounds = true
       
        m2.backgroundColor = UIColor.clearColor()
        m2.layer.masksToBounds = false
        m2.layer.cornerRadius = m1.frame.size.width/2
        m2.clipsToBounds = true
       
        m3.backgroundColor = UIColor.clearColor()
        m3.layer.masksToBounds = false
        m3.layer.cornerRadius = m1.frame.size.width/2
        m3.clipsToBounds = true
       
        
        eventTitle.frame = CGRectMake(15, 5, frame.size.width - 32 * appConstants.ScreenFactor, 10 * appConstants.ScreenFactor)
        timeIcon.frame = CGRectMake(15,eventTitle.frame.origin.y + eventTitle.frame.size.height + 5,9 * appConstants.ScreenFactor,9 * appConstants.ScreenFactor)

        eventTime.frame = CGRectMake(timeIcon.frame.origin.x + timeIcon.frame.size.width + 5 ,timeIcon.frame.origin.y ,self.bounds.size.width - (timeIcon.frame.origin.x + timeIcon.frame.size.width + 5),9 * appConstants.ScreenFactor)
        
        placeIcon.frame = CGRectMake(15,eventTime.frame.origin.y + eventTime.frame.size.height + 5 ,9 * appConstants.ScreenFactor,9 * appConstants.ScreenFactor)
        
        eventPlace.frame = CGRectMake(placeIcon.frame.origin.x + placeIcon.frame.size.width + 5 ,placeIcon.frame.origin.y ,self.bounds.size.width - (placeIcon.frame.origin.x + placeIcon.frame.size.width + 5),9 * appConstants.ScreenFactor)
        
        blankView.frame = CGRectMake(0 , eventPlace.frame.origin.y + eventPlace.frame.size.height + 5 , self.bounds.size.width, self.bounds.size.height - (eventPlace.frame.origin.y + eventPlace.frame.size.height + 5))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setData(model: NSDictionary, selectedColor: UIColor, membersArray: NSArray)  {
        
        print(model)
        print(membersArray)
        vline.backgroundColor = selectedColor
        eventTitle.text = model.objectForKey(appConstants.event_title) as? String
        eventTime.text = model.objectForKey(appConstants.event_time) as? String
        eventPlace.text = model.objectForKey(appConstants.location) as? String
        timeIcon.image = UIImage.init(imageLiteral: "clock.png")
        blankView.backgroundColor = UIColor().HexToColor(appConstants.font_black_color, alpha: 1.0)
        placeIcon.image = UIImage.init(imageLiteral: "city.png")

        if (eventPlace.text != nil && !(eventPlace.text?.isEmpty)!) {
            placeIcon.alpha = 1.0
        }else{
            placeIcon.alpha = 0.0
        }
        print(model.objectForKey("attending"))
        print(appConstants.attending)
        let attendingArray: NSArray = (model.objectForKey("attending") as? NSArray)!
        m1.alpha = 0.0
        m2.alpha = 0.0
        m3.alpha = 0.0

        for i in 0  ..< attendingArray.count  {
            let attendingMemberID = attendingArray.objectAtIndex(i) as! String
            for j in 0  ..< membersArray.count  {
                let memberDict = membersArray.objectAtIndex(j) as! NSDictionary
                let mID = memberDict.valueForKey(self.appConstants.member_id) as? String
                if attendingMemberID.isEqual(mID) {
                    let url: String = (memberDict.objectForKey(appConstants.profice_pic) as? String)!
                    if(i==0){
                        m1.downloadedFrom(baseUrl + url)
                        m1.alpha = 1.0
                    } else if(i==1){
                        m2.downloadedFrom(baseUrl + url)
                        m2.alpha = 1.0
                    } else if(i==2) {
                        m3.downloadedFrom(baseUrl + url)
                        m3.alpha = 1.0
                    }
                }
            }
        }
    }
    
}

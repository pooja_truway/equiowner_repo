//
//  AddNewEventView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 23/09/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Photos
import AVKit
import MobileCoreServices
import AVFoundation
import EventKit
import EventKitUI

class AddNewEventView: UIView, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource,UIActionSheetDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate ,UIScrollViewDelegate{
    
    
    var yy:CGFloat = 0, margin:CGFloat = 0, sYY:CGFloat = 0
    var alreadySetDictionary: NSDictionary = NSDictionary()
    let imagePickerController = UIImagePickerController()
    var galleryRootController = UIApplication.sharedApplication().keyWindow!.rootViewController
    var date = NSDate()

    var addButton: UIButton = UIButton()
    var zoomButton: UIButton = UIButton() //pooja
    var inviteButton: UIButton = UIButton()
    var uploadImageButton: UIButton = UIButton()
    var ImageViewPostClicked: UIImageView = UIImageView()
    
    
    var AttendyStr : NSString = ""
    
    var EnlargedImageView: UIImageView = UIImageView() //pooja
      let mainViewController: MainViewController = MainViewController()
     var scrollV : UIScrollView!
    
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    
    let eventLayout : UIView = UIView()
    var headingLabel:SkyFloatingLabelTextField = SkyFloatingLabelTextField()
    let dateData : UITextField = UITextField()
    let timeData : UITextField = UITextField()
    var eventAddressLabel:SkyFloatingLabelTextField = SkyFloatingLabelTextField()

    var isUpdate: Bool = false
    
    var isVerticalLineHidden: Bool = false

    var contactsDict: NSDictionary = NSDictionary()
    var contactsList: NSMutableArray = NSMutableArray()
    var cellIdendifier: String = "InviteContactCell"
    var membersArray: NSArray = NSArray()

    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    var indicator = UIActivityIndicatorView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.addGestureRecognizer(tap)
        
        scrollV=UIScrollView()
        scrollV.frame = CGRectMake(0, 0, EnlargedImageView.frame.width, EnlargedImageView.frame.height)
        scrollV.minimumZoomScale=1
        scrollV.maximumZoomScale=10
        scrollV.bounces=false
        scrollV.delegate=self;
        EnlargedImageView.addSubview(scrollV)
        
           }
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showUI(isUpdate: Bool, memberArray: NSArray){
        
        self.isUpdate = isUpdate
        self.membersArray = memberArray
        print("response = \(membersArray)")

        downloadContactList()
        initializeVariables()
        setTopLayer()
        setBottomButtons()
    }
    
    func initializeVariables() {
        yy = 5
        margin = 5 * appConstants.ScreenFactor
    }
    
    func downloadContactList(){
        let postString = "action=contacts&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                print("response = \(self.membersArray)")

                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                         //User Data
                        self.contactsDict = (data?[self.appConstants.data])! as! NSDictionary
                        
                       
                        self.contactsList = self.contactsDict.objectForKey(self.appConstants.contacts)!.mutableCopy() as! NSMutableArray
                        var counter: Int = 0
                        for dict in self.contactsList{
                            let changedData: NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                            changedData.setValue(false, forKey: self.appConstants.selected)
                            if(self.isUpdate){
                                //Check for already added members
                                let attendingMemberID = dict.objectForKey(self.appConstants.userId)
                                for j in 0  ..< self.membersArray.count  {
                                    let mID = self.membersArray.objectAtIndex(j) as! String
                                    if attendingMemberID!.isEqual(mID) {
                                        changedData.setValue(true, forKey: self.appConstants.selected)
                                    }
                                }
                            }
                            self.contactsList.replaceObjectAtIndex(counter, withObject: changedData)
                            counter += 1
                        }
                    }
                }
            }
        }
    }

    func setTopLayer(){
        let eventLabel : UILabel = UILabel()
        eventLabel.frame = CGRectMake(margin,yy,self.frame.size.width,13 * appConstants.ScreenFactor)
        if(isUpdate){
            eventLabel.text = "UPDATE NEW EVENT"
        }else{
            eventLabel.text = "ADD NEW EVENT"
        }
        appConstants.styleLabelsNew(eventLabel)
        self.addSubview(eventLabel)
    
        yy+=eventLabel.frame.size.height + 5
        
        eventLayout.frame = CGRectMake(margin,yy,self.frame.size.width - 2*margin,110 * appConstants.ScreenFactor)
        eventLayout.backgroundColor = UIColor.whiteColor();
        self.addSubview(eventLayout)
        
        let vline1: UIView = UIView(frame : CGRectMake(0 , 0 , 1, eventLayout.frame.size.height))
        vline1.backgroundColor = UIColor().HexToColor(appConstants.button_color)
        eventLayout.addSubview(vline1)
        
        headingLabel = SkyFloatingLabelTextField(frame: CGRectMake(margin, 3 * appConstants.ScreenFactor,eventLayout.frame.size.width - 2*margin, 24 * appConstants.ScreenFactor))
        headingLabel.title = "Enter Name"
        headingLabel.placeholder = "Enter Name"

        if(isUpdate){
            headingLabel.text = alreadySetDictionary.objectForKey(appConstants.event_title) as? String
        }
        
        headingLabel.delegate = self
        appConstants.styleFloatingFields(headingLabel)
        eventLayout.addSubview(headingLabel)
        
        
        
        uploadImageButton = UIButton.init(frame: CGRectMake(margin+170, 4 * appConstants.ScreenFactor+5,120, 30))
        uploadImageButton.layer.cornerRadius = appConstants.CornerRadius
            uploadImageButton.setTitle("Upload Image", forState: UIControlState.Normal)
        
        uploadImageButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        uploadImageButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        uploadImageButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        uploadImageButton.userInteractionEnabled = true
        uploadImageButton.tag = 1
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(addImage(_:)))
        uploadImageButton.addGestureRecognizer(tapRecognizer)
        eventLayout.addSubview(uploadImageButton)
        
      //  ImageViewPostClicked = UIImageView.init(frame: CGRectMake(margin+200, 4 * appConstants.ScreenFactor+60,80, 80))
        
         ImageViewPostClicked = UIImageView.init(frame: CGRectMake(margin, 4 * appConstants.ScreenFactor+235,self.frame.size.width - 2*margin,130 * appConstants.ScreenFactor)) //pooja
        
        
        ImageViewPostClicked.userInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        tapGesture.numberOfTapsRequired = 2
        ImageViewPostClicked.addGestureRecognizer(tapGesture)
        self.bringSubviewToFront(ImageViewPostClicked)
       // self.superview?.bringSubviewToFront(ImageViewPostClicked)

        
        if(isUpdate){
           
            let imageString: String = (alreadySetDictionary.objectForKey("profilePic") as? String)!
            if imageString=="" || imageString .isEmpty {
                
            }else
            {
             showProgress()
             let url = NSURL(string: "http://www.equiowner.club/" + imageString)!
          print( imageString)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let data:NSData = NSData(contentsOfURL: url)!
                        if UIImage(data: data) != nil {
                            self.hideProgress()
                            self.ImageViewPostClicked.image = UIImage(data: data)!
                        }
                    })
                }
        }
            // Run task
        
        
                self.addSubview(ImageViewPostClicked)
        
      /*  zoomButton = UIButton.init(frame: CGRectMake(margin+5,110 * appConstants.ScreenFactor+70,50, 20))  //pooja
        zoomButton.backgroundColor = .greenColor()
        zoomButton.setTitle("Test Button", forState: .Normal)
        zoomButton.addTarget(self, action: #selector(imageTapped(_:)), forControlEvents: .TouchUpInside)
        zoomButton.backgroundColor = UIColor.greenColor()
        
        self.ImageViewPostClicked.addSubview(zoomButton)*/

        
//        let itemImage: UIImageView = UIImageView.init(frame: CGRectMake(margin+200, 4 * appConstants.ScreenFactor+5,30, 30))
//        itemImage.image = UIImage.init(imageLiteral: "ic_mic.png")
//        itemImage.userInteractionEnabled=true;
//        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(addImage(_:)))
//        itemImage.addGestureRecognizer(tapRecognizer)
//        eventLayout.addSubview(itemImage)

        addDateTimeLayout(true)// Date layout
        addDateTimeLayout(false) // time Layout
        
        eventAddressLabel = SkyFloatingLabelTextField(frame: CGRectMake(margin, 83 * appConstants.ScreenFactor,eventLayout.frame.size.width - 2*margin, 24 * appConstants.ScreenFactor))
        eventAddressLabel.title = "Enter Event Address"
        eventAddressLabel.placeholder = "Enter Event Address"
        if(isUpdate){
            eventAddressLabel.text = alreadySetDictionary.objectForKey(appConstants.location) as? String
        }
        
        eventAddressLabel.delegate = self
        appConstants.styleFloatingFields(eventAddressLabel)
        eventLayout.addSubview(eventAddressLabel)
        
    }
    func addImage(gestureRecognizer: UITapGestureRecognizer) {
        
        self.showPhotoSelectionOptions()
 
    }
    
    func imageTapped(sender: UIImageView)
    {
        print(" tap gesture is working")
        
        EnlargedImageView = UIImageView(frame: CGRectMake(-50, 7 * appConstants.ScreenFactor,self.frame.size.width+45 ,340 * appConstants.ScreenFactor))
        
        // EnlargedImageView = UIImageView.init(frame: CGRectMake(-50, 7 * appConstants.ScreenFactor,self.frame.size.width+45 ,340 * appConstants.ScreenFactor)) //pooja
        
        EnlargedImageView.contentMode = .ScaleToFill
        
               EnlargedImageView.userInteractionEnabled = true
        EnlargedImageView.backgroundColor = UIColor.whiteColor()

        
            let imageString: String = (alreadySetDictionary.objectForKey("profilePic") as? String)!
            if imageString=="" || imageString .isEmpty {
                
            }else
            {
                showProgress()
                let url = NSURL(string: "http://www.equiowner.club/" + imageString)!
                print( imageString)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let data:NSData = NSData(contentsOfURL: url)!
                    if UIImage(data: data) != nil {
                        self.hideProgress()
                        self.EnlargedImageView.image = UIImage(data: data)!
                    }
                })
            }
        
        EnlargedImageView.backgroundColor = UIColor.whiteColor()
        
        self.addSubview(EnlargedImageView)
        self.superview?.bringSubviewToFront(EnlargedImageView)

       
        //viewForZoomingInScrollView(scrollV)
        eventLayout.userInteractionEnabled=false
        ImageViewPostClicked.userInteractionEnabled=false
        
        // Your action
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?
    {
        print("hello")
        return EnlargedImageView
    }

    
    func showPhotoSelectionOptions(){
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Option to select", message: nil, preferredStyle: .ActionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let takePicActionButton: UIAlertAction = UIAlertAction(title: "Take Picture", style: .Default)
        { action -> Void in
            
            if (UIImagePickerController.isSourceTypeAvailable(.Camera)) {
                if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
                    self.imagePickerController.allowsEditing = false
                    self.imagePickerController.sourceType = .Camera
                    self.imagePickerController.delegate = self
                    self.imagePickerController.cameraCaptureMode = .Photo
                    self.vc!.presentViewController(self.imagePickerController, animated: true, completion: nil)
                } else {
                    self.appConstants.showAlert("Rear camera doesn't exist", message:"Application cannot access the camera.", controller: self.vc!)
                }
            } else {
                self.appConstants.showAlert("Camera inaccessable", message:"Application cannot access the camera.", controller: self.vc!)
            }
        }
        actionSheetControllerIOS8.addAction(takePicActionButton)
        
        let PicActionButton: UIAlertAction = UIAlertAction(title: "Picture from Gallery", style: .Default)
        { action -> Void in
            self.imagePickerController.sourceType = .PhotoLibrary
            self.imagePickerController.delegate = self
            self.vc!.presentViewController(self.imagePickerController, animated: true, completion: nil)
            
        }
        actionSheetControllerIOS8.addAction(PicActionButton)
        
        vc!.presentViewController(actionSheetControllerIOS8, animated: true, completion: nil)
    }

    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            ImageViewPostClicked.contentMode = .ScaleAspectFit
            ImageViewPostClicked.image = pickedImage
            
            let imageData:NSData = UIImageJPEGRepresentation(pickedImage,0.5)!
            var strBase64:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)

            strBase64 = strBase64.stringByReplacingOccurrencesOfString("+", withString: "%2B")
            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.EVENT_IMAGE_STRING, value: strBase64)
        }
        
        self.imagePickerController.dismissViewControllerAnimated(true, completion: nil)
    }
    func addDateTimeLayout(isDate: Bool){
        let timeLayout : UIView = UIView()
         if !isDate {
            timeLayout.frame = CGRectMake(margin, 55 * appConstants.ScreenFactor,eventLayout.frame.size.width - 2*margin, 25 * appConstants.ScreenFactor)
         }else{
            timeLayout.frame = CGRectMake(margin, 29 * appConstants.ScreenFactor,eventLayout.frame.size.width - 2*margin, 25 * appConstants.ScreenFactor)
         }

        timeLayout.backgroundColor = UIColor.clearColor();
        eventLayout.addSubview(timeLayout)
        
        let timeLabel : UILabel = UILabel()
        timeLabel.frame = CGRectMake(0,0,self.frame.size.width,12 * appConstants.ScreenFactor)
        if !isDate {
            timeLabel.text = "Time"
        }else{
            timeLabel.text = "Date"
        }
        appConstants.styleLabels(timeLabel)
        timeLayout.addSubview(timeLabel)
        
        let timeIcon : UIImageView = UIImageView()
        timeIcon.frame = CGRectMake(0,14 * appConstants.ScreenFactor,9 * appConstants.ScreenFactor,9 * appConstants.ScreenFactor)
        timeIcon.image = UIImage.init(imageLiteral: "clock.png")
        timeLayout.addSubview(timeIcon)
        
        if !isDate {
            timeData.frame = CGRectMake(timeIcon.frame.size.width + 5,13 * appConstants.ScreenFactor,self.frame.size.width - (timeIcon.frame.size.width + 10),10 * appConstants.ScreenFactor)
            
            if(isUpdate){
                timeData.text = alreadySetDictionary.objectForKey(appConstants.event_time) as? String
            }else{
                timeData.text = "Tap to Add Time"
            }
            appConstants.styleInnerLabels(timeData)
            timeLayout.addSubview(timeData)
            let starttimedatePicker = UIDatePicker()
            starttimedatePicker.datePickerMode = UIDatePickerMode.Time

            timeData.inputView = starttimedatePicker
            starttimedatePicker.addTarget(self, action: #selector(startTimeDiveChanged(_:)), forControlEvents: .ValueChanged)
        }else{
            dateData.frame = CGRectMake(timeIcon.frame.size.width + 5,13 * appConstants.ScreenFactor,self.frame.size.width - (timeIcon.frame.size.width + 10),10 * appConstants.ScreenFactor)
            if(isUpdate){
                dateData.text = alreadySetDictionary.objectForKey(appConstants.event_date) as? String
            }else{
                dateData.text = "Tap to Add Date"
            }
            appConstants.styleInnerLabels(dateData)
            timeLayout.addSubview(dateData)
            let starttimedatePicker = UIDatePicker()
            starttimedatePicker.datePickerMode = UIDatePickerMode.Date
            starttimedatePicker.locale = NSLocale(localeIdentifier: "en_GB") // using Great Britain for 24 hr format
            
            dateData.inputView = starttimedatePicker
            starttimedatePicker.addTarget(self, action: #selector(startDateDiveChanged(_:)), forControlEvents: .ValueChanged)
        }
        
    }
    
    func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_GB") // using Great Britain for 24 hr format
        formatter.timeStyle = .ShortStyle
        timeData.text = appConstants.hour24To12HourTimeConverter(formatter.stringFromDate(sender.date))
    }
    
    func startDateDiveChanged(sender: UIDatePicker) {
        let formatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_GB") // using Great Britain for 24 hr format
        formatter.dateStyle = .ShortStyle
        dateData.text = appConstants.getAddEventFormattedDate(formatter.stringFromDate(sender.date))
        date=sender.date;
        
     //   date=formatter.stringFromDate(sender.date) as NSDate!
      //  let dateValue = dateFormatter.dateFromString(dataString) as NSDate!

        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        self.endEditing(true)
        return true;
    }
    
    func setBottomButtons(){
        yy = self.frame.size.height * 0.91
        let bottomItemWidth:CGFloat = self.frame.size.width/2
        
        addButton = UIButton.init(frame: CGRectMake(margin,yy,bottomItemWidth - 1.5*margin,22 * appConstants.ScreenFactor))
        addButton.layer.cornerRadius = appConstants.CornerRadius
        if !isUpdate {
            addButton.setTitle("Add", forState: UIControlState.Normal)
        }else{
            addButton.setTitle("Update", forState: UIControlState.Normal)
        }
        addButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        addButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        addButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        addButton.userInteractionEnabled = true
        addButton.tag = 1
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(buttonPressed(_:)))
        addButton.addGestureRecognizer(tapRecognizer)
        self.addSubview(addButton)
        
        inviteButton = UIButton.init(frame: CGRectMake(bottomItemWidth,yy,bottomItemWidth - margin,22 * appConstants.ScreenFactor))
        inviteButton.layer.cornerRadius = appConstants.CornerRadius
        inviteButton.setTitle("Invite", forState: UIControlState.Normal)
        inviteButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        inviteButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        inviteButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        inviteButton.userInteractionEnabled = true
        inviteButton.tag = 2
        let tapRecognizerSubmit = UITapGestureRecognizer(target: self, action: #selector(buttonPressed(_:)))
        inviteButton.addGestureRecognizer(tapRecognizerSubmit)
        self.addSubview(inviteButton)
    }
    
    func buttonPressed(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!
        switch viewTouched.tag {
        case 1:
            //Add/Update Event
            addUpdateEventInServer()
            break
        case 2:
            //Invite
            if contactsList.count > 0 {
                showChoiceForContacts();
            } else {
                appConstants.showAlert("Warning!", message: "No contacts found", controller: vc!)
            }
            break
        default:
            break
        }
    }
    
    func showChoiceForContacts() {
        
        let controller = UIViewController()
        var rect:CGRect?
        if (contactsList.count < 4) {
            rect = CGRectMake(0, 0, 272, 100)
            controller.preferredContentSize = rect!.size
        }
        else if (contactsList.count < 6){
            rect = CGRectMake(0, 0, 272, 150);
            controller.preferredContentSize = rect!.size
        }
        else if (contactsList.count < 8){
            rect = CGRectMake(0, 0, 272, 200);
            controller.preferredContentSize = rect!.size
        }
        else{
            rect = CGRectMake(0, 0, 272, 250);
            controller.preferredContentSize = rect!.size
        }
        let alertTableView  = UITableView(frame: rect!)
        alertTableView.delegate = self
        alertTableView.dataSource = self
        alertTableView.tableFooterView = UIView(frame: CGRectZero)
        alertTableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        alertTableView.separatorInset    = UIEdgeInsetsZero
        alertTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        
        controller.view.addSubview(alertTableView)
        controller.view.bringSubviewToFront(alertTableView)
        controller.view.userInteractionEnabled = true
        alertTableView.userInteractionEnabled = true
        alertTableView.allowsSelection = true
        let alertController = UIAlertController(title: "Select Contact", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.setValue(controller, forKey: "contentViewController")
    
        let doneAction = UIAlertAction(title: "Done", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
        })
        
        alertController.addAction(doneAction)
        
        vc!.presentViewController(alertController, animated: true, completion:{})
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 25 * appConstants.ScreenFactor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdendifier)! as UITableViewCell
        let userDict = self.contactsList[indexPath.row] as! NSMutableDictionary
        cell.textLabel?.text = userDict.valueForKey(self.appConstants.name) as? String
        cell.separatorInset = UIEdgeInsetsZero
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        let isSelected: Bool = userDict.objectForKey(appConstants.selected) as! Bool
        if !isSelected {
            cell.accessoryType = .None
        }else{
            cell.accessoryType = .Checkmark
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactsList.count
       
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        updateSelectedInvites(indexPath.row)
        tableView.reloadData()
    }
    
    func updateSelectedInvites(index: Int) {
        let changedData: NSMutableDictionary = self.contactsList.objectAtIndex(index) as! NSMutableDictionary
        let isSelected: Bool = changedData.objectForKey(appConstants.selected) as! Bool
        changedData.setValue(!isSelected, forKey: appConstants.selected)
        self.contactsList.replaceObjectAtIndex(index, withObject: changedData)
    }
    
    func addUpdateEventInServer() {
        let eventDate = dateData.text! as String
        let eventTime = timeData.text! as String
        
        if headingLabel.text!.isEmpty {
            appConstants.showAlert("Whoa!", message: "You missed the Event Heading.",controller: vc!)
            return
        }else if (eventTime.isEmpty && eventTime.isEqual("Tap to Add Time")) {
            appConstants.showAlert("Whoa!", message: "You missed the Event Timings.",controller: vc!)
            return
        }else if (eventDate.isEmpty && eventDate.isEqual("Tap to Add Date")) {
            appConstants.showAlert("Whoa!", message: "You missed the Event Date.",controller: vc!)
            return
        }else{
            var eventAddressString = ""
            if (!eventAddressLabel.text!.isEmpty) {
                eventAddressString = eventAddressLabel.text!
            }
            
            var selected : [String] = [String]()
            for dict in self.contactsList{
                let isSelected: Bool = dict.objectForKey(appConstants.selected) as! Bool
                if isSelected {
                    selected.append(dict.objectForKey(appConstants.userId) as! String)
                // self.AttendyStr=self.AttendyStr+","(dict.objectForKey(appConstants.userId) as! String)
                }
            }
            //Webservice
            showProgress()
            
            //send to server
            let json = NSMutableDictionary()
            
            json.setValue("saveEventToios", forKey: self.appConstants.action)
            
             json.setValue(selected, forKey: self.appConstants.attending)
//            json.setValue(SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID), forKey: self.appConstants.userId) //1
//
//            json.setValue(headingLabel.text, forKey: self.appConstants.event_title) //2
//            json.setValue(eventTime, forKey: self.appConstants.event_time) //3
//            json.setValue(eventDate, forKey: self.appConstants.event_date) //4
//            json.setValue(headingLabel.text, forKey: self.appConstants.description) //5
//            json.setValue(eventAddressString, forKey: self.appConstants.location) //6
            
// json.setValue("jpg", forKey: "profilePic_extension") //8
            
//            if SharedPreferenceUtils._sharedInstance.getStringValue(self.appConstants.EVENT_IMAGE_STRING).isEmpty {
//                json.setValue(SharedPreferenceUtils._sharedInstance.getStringValue(self.appConstants.EVENT_IMAGE_STRING), forKey: self.appConstants.EVENT_IMAGE)
//            }else{
//                json.setValue("", forKey: self.appConstants.EVENT_IMAGE)
//            }
            
// json.setValue(SharedPreferenceUtils._sharedInstance.getStringValue(self.appConstants.EVENT_IMAGE_STRING), forKey: self.appConstants.EVENT_IMAGE) //pooja- to remove image
            let eventIdisUpdate:String
            if(self.isUpdate){
              //  json.setValue(alreadySetDictionary.objectForKey(appConstants.event_id) as? String, forKey: self.appConstants.event_id)
               eventIdisUpdate = (alreadySetDictionary.objectForKey(appConstants.event_id) as? String)!
                
            }else{
              //  json.setValue("0", forKey: self.appConstants.event_id)
                eventIdisUpdate = "0"
            }
            
            let  address :String = headingLabel.text!;
            
             let postString = "action=saveEvent&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&profilePic_code="+SharedPreferenceUtils._sharedInstance.getStringValue(self.appConstants.EVENT_IMAGE_STRING)+"&profilePic_extension="+"jpg"+"&event_time="+eventTime+"&event_date="+eventDate+"&location="+eventAddressString+"&event_title="+address+"&event_id="+eventIdisUpdate+"&attendyID="
            
            webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
                
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    self.hideProgress()
                    
                    if error != nil{
                        return
                    }
                    let eventStore : EKEventStore = EKEventStore()
                    // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
                    eventStore.requestAccessToEntityType(EKEntityType.Event, completion: {
                        (granted, error) in
                        
                        if (granted) && (error == nil) {
                            print("granted \(granted)")
                            print("error \(error)")
                            
                            let event:EKEvent = EKEvent(eventStore: eventStore)
                            
                            event.title = self.headingLabel.text!
                            event.startDate = self.date
                            event.endDate = self.date
                            event.notes = "Dummy notes of Horse App"
                            event.calendar = eventStore.defaultCalendarForNewEvents 
                            
                            do {
                                try eventStore.saveEvent(event, span: .ThisEvent)
                            print("Saved Event")
                            } catch {
                                print("Error Saving")
                            }
                        }
                    }) 

                    print("responseImage = \(response)")
                                        
               
                    let GetActivityIdDict : NSDictionary = self.appConstants.convertStringToDictionary(response)!
                    let res_event_id = GetActivityIdDict.valueForKey("data")?.valueForKey("activity")
                    print (" res_event_id \(res_event_id)")
                    print(GetActivityIdDict.valueForKey("data")?.valueForKey("activity"))
                    json.setValue(res_event_id, forKey: "res_event_id")
                   
                    
                    let data1 = self.appConstants.convertDictionaryToString(json)
                    print("data1 for saveToIos  \(data1)")
                    //2nd hit to send attendy ID in json object
                    self.webService.HTTPPostJSON(self.appConstants.DAILY_ACTIVITY, data: data1!){ (response, error) -> Void in
                        
                        print("responseAttendyId = \(response)")
                    }

                    let data = self.appConstants.convertStringToDictionary(response)
                    if data != nil {
                        if ((data?[self.appConstants.ErrorCode]) != nil) {
                            //Error
                            return
                        }else{
                            self.gestureRecognizers?.forEach(self.removeGestureRecognizer)
                            self.subviews.forEach({ $0.removeFromSuperview() })
                            AppConstants.CurrentScreen = self.appConstants.CalendarScreen
                            let calendarDisplay : ShowCalendarInAppView = ShowCalendarInAppView(frame: self.frame)
                         //   calendarDisplay.backgroundColor = UIColor().HexToColor(self.appConstants.font_black_color, alpha: 1.0)
                            calendarDisplay.backgroundColor = UIColor.clearColor()
                            self.addSubview(calendarDisplay)
                        }
                    }
                }
            }
            
                   }
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
}

//
//  ShowCalendarInAppView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 17/09/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import EventKit
import FSCalendar
import AVKit
import AVFoundation
import MediaPlayer
import MobileCoreServices
import Photos

class ShowCalendarInAppView: UIView, FSCalendarDelegate, FSCalendarDataSource, UITableViewDelegate, UITableViewDataSource{
    var myCalendar: FSCalendar!
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    
    var startDateOfMonth: String = ""
    var endDateOfMonth: String = ""
    
    var monthWiseEventsArray: NSArray = NSArray()
    var dayWiseEventsArray: NSMutableArray = NSMutableArray()

    var eventMembersArray: NSArray = NSArray()

    var eventColorArray: NSMutableArray = NSMutableArray()
    
    var eventsTableView: UITableView  =   UITableView()
    var cellIdendifier: String = "EventCell"

    var selectedColorForShowedEvent: UIColor = UIColor()
    var selectedDate: NSDate = NSDate()
    var indicator = UIActivityIndicatorView()

    lazy var nextMonth : UIImageView = {
        let image = UIImageView()
        image.image = UIImage.init(imageLiteral: "rightarrow.png")
        image.userInteractionEnabled = true
        self.addSubview(image)
        return image
    }()
    
    lazy var previousMonth : UIImageView = {
        let image = UIImageView()
        image.image = UIImage.init(imageLiteral: "leftarrow.png")
        image.userInteractionEnabled = true
        self.addSubview(image)
        return image
    }()
    
    lazy var addEvent : UIImageView = {
        let image = UIImageView()
        image.image = UIImage.init(imageLiteral: "add_btn.png")
        image.userInteractionEnabled = true
        self.addSubview(image)
        return image
    }()

    lazy var gregorian : NSCalendar = {
        let cal = NSCalendar(identifier: NSCalendarIdentifierGregorian)!
        cal.timeZone = NSTimeZone(abbreviation: "UTC")!
        return cal
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        
        myCalendar = FSCalendar.init(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height * 0.7))
        myCalendar?.dataSource = self
        myCalendar?.delegate = self
        
        self.addSubview(myCalendar!)
        
        self.addEvent.frame = CGRect(x: 6 * appConstants.ScreenFactor, y: 5 * appConstants.ScreenFactor, width: 20 * appConstants.ScreenFactor, height: 20 * appConstants.ScreenFactor)
        self.nextMonth.frame = CGRect(x: self.bounds.size.width - 15 * appConstants.ScreenFactor, y: 5 * appConstants.ScreenFactor, width: 12 * appConstants.ScreenFactor, height: 19 * appConstants.ScreenFactor)
        self.previousMonth.frame = CGRect(x: self.bounds.size.width - 40 * appConstants.ScreenFactor, y: 5 * appConstants.ScreenFactor, width: 12 * appConstants.ScreenFactor, height: 19 * appConstants.ScreenFactor)
      
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(addNewEvent(_:)))
        addEvent.addGestureRecognizer(tapRecognizer)
        
        let nextMonthtapRecognizer = UITapGestureRecognizer(target: self, action: #selector(nextMonth(_:)))
        nextMonth.addGestureRecognizer(nextMonthtapRecognizer)
        
        let previousMonthtapRecognizer = UITapGestureRecognizer(target: self, action: #selector(previousMonth(_:)))
        previousMonth.addGestureRecognizer(previousMonthtapRecognizer)
    
        myCalendar?.appearance.headerTitleColor = UIColor.whiteColor()//for Month title
        myCalendar?.tintColor = UIColor.whiteColor()
        
        myCalendar?.appearance.weekdayTextColor = UIColor.whiteColor()//for sat-sun dates
        myCalendar?.appearance.titleWeekendColor = UIColor.whiteColor()//for Week titles
        myCalendar?.appearance.subtitleWeekendColor = UIColor.whiteColor()
        myCalendar?.appearance.subtitleDefaultColor = UIColor.whiteColor()
        myCalendar?.appearance.selectionColor = UIColor().HexToColor(appConstants.selectedColor)//for selected date
        myCalendar?.appearance.titleDefaultColor = UIColor.whiteColor()//For Mon-Fri cureent dates
        myCalendar?.appearance.todayColor = UIColor().HexToColor(appConstants.button_color)//Today's date
        
        myCalendar?.appearance.headerTitleTextSize =  12 * appConstants.ScreenFactor//for Month title
        myCalendar?.appearance.titleTextSize =  10 * appConstants.ScreenFactor//for Week titles
        myCalendar?.appearance.weekdayTextSize =  9 * appConstants.ScreenFactor//for sat-sun dates
        myCalendar?.appearance.eventDefaultColor = UIColor.clearColor()
        myCalendar?.appearance.eventSelectionColor = UIColor.clearColor()
        
        myCalendar?.appearance.caseOptions = [.HeaderUsesUpperCase,.WeekdayUsesUpperCase]
        myCalendar?.appearance.headerMinimumDissolvedAlpha = 0.0;
        myCalendar?.appearance.headerDateFormat = "MMMM,yy";
        
       /* myCalendar?.headerTitleColor = UIColor.whiteColor()//for Month title
        myCalendar?.tintColor = UIColor.whiteColor()

        myCalendar?.weekdayTextColor = UIColor.whiteColor()//for sat-sun dates
        myCalendar?.titleWeekendColor = UIColor.whiteColor()//for Week titles
        myCalendar?.subtitleWeekendColor = UIColor.whiteColor()
        myCalendar?.subtitleDefaultColor = UIColor.whiteColor()
        myCalendar?.selectionColor = UIColor().HexToColor(appConstants.selectedColor)//for selected date
        myCalendar?.titleDefaultColor = UIColor.whiteColor()//For Mon-Fri cureent dates
        myCalendar?.appearance.todayColor = UIColor().HexToColor(appConstants.button_color)//Today's date

        myCalendar?.headerTitleTextSize =  12 * appConstants.ScreenFactor//for Month title
        myCalendar?.titleTextSize =  10 * appConstants.ScreenFactor//for Week titles
        myCalendar?.weekdayTextSize =  9 * appConstants.ScreenFactor//for sat-sun dates
        myCalendar?.appearance.eventDefaultColor = UIColor.clearColor()
        myCalendar?.appearance.eventSelectionColor = UIColor.clearColor()

        myCalendar?.appearance.caseOptions = [.HeaderUsesUpperCase,.WeekdayUsesUpperCase]
        myCalendar?.appearance.headerMinimumDissolvedAlpha = 0.0;
        myCalendar?.headerDateFormat = "MMMM,yy";
*/
        if UIApplication.sharedApplication().isIgnoringInteractionEvents() {
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
        }

        self.userInteractionEnabled = true
        myCalendar?.userInteractionEnabled = true
        myCalendar?.scopeGesture.enabled = true
        myCalendar?.allowsSelection = true
        
        addEventsTable()
        calculateStartAndEndDate()
        myCalendar?.reloadData()
        calendarCurrentPageDidChange(myCalendar)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func calculateStartAndEndDate() {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let components = NSCalendar.currentCalendar().components([.Year, .Month], fromDate: (myCalendar?.currentPage)!)
        let startOfMonth = NSCalendar.currentCalendar().dateFromComponents(components)!
        startDateOfMonth = dateFormatter.stringFromDate(startOfMonth)
        
        let comps2 = NSDateComponents()
        comps2.month = 1
        comps2.day = -1
        let endOfMonth = NSCalendar.currentCalendar().dateByAddingComponents(comps2, toDate: startOfMonth, options: [])!
        endDateOfMonth = dateFormatter.stringFromDate(endOfMonth)
        loadEventsInCalendar()
    }
    
    func addEventsTable() {
        eventsTableView.frame             =   CGRect(x: 5 * appConstants.ScreenFactor, y: self.frame.size.height * 0.71, width: self.frame.size.width - 10 * appConstants.ScreenFactor, height: self.frame.size.height * 0.29)
        eventsTableView.delegate          =   self
        eventsTableView.dataSource        =   self
        eventsTableView.backgroundColor   = UIColor.clearColor()
        eventsTableView.separatorStyle    = UITableViewCellSeparatorStyle.None;
        eventsTableView.separatorInset    = UIEdgeInsetsZero
        eventsTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        self.addSubview(eventsTableView)
    }
    
    // MARK : Events
    func loadEventsInCalendar() {
        showProgress()
        
        let postString = "action=loadEvents&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&start_date="+startDateOfMonth+"&end_date="+endDateOfMonth
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
            }
            
            if error != nil{
                return
            }
            print("response = \(response)")
            let data = self.appConstants.convertStringToDictionary(response)
            if data != nil {
                if ((data?[self.appConstants.ErrorCode]) != nil) {
                    //Error
                    return
                }else{
                    let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                    let event:NSDictionary = (mainList[self.appConstants.event])! as! NSDictionary
                    self.monthWiseEventsArray = event.objectForKey(self.appConstants.events) as! NSArray
                    self.eventMembersArray = event.objectForKey(self.appConstants.members) as! NSArray
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                        self.myCalendar?.reloadData()
                        self.layoutIfNeeded()
                    }
                }
            }
        }
    }

    func calendar(calendar: FSCalendar, hasEventForDate date: NSDate) -> Bool {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
    
        var showDotForEvent: Bool = false
        for i in 0  ..< monthWiseEventsArray.count  {
            let sDict = monthWiseEventsArray.objectAtIndex(i) as! NSDictionary
            let eventDate = sDict.valueForKey(self.appConstants.event_date) as? String
            if eventDate!.isEqual(dateFormatter.stringFromDate(date)) {
                let colorDic = NSDictionary(dictionary:
                    [self.appConstants.event_date : eventDate!,
                     self.appConstants.colour : self.appConstants.randomColor()],
                    copyItems: true)
                eventColorArray.addObject(colorDic)
                showDotForEvent = true
                break
            }
        }
        
        return showDotForEvent
    }
    
    func calendar(calendar: FSCalendar, numberOfEventsForDate date: NSDate) -> Int {
        if self.calendar(self.myCalendar, hasEventForDate: date) {
            return 1
        }else {
            return 0
        }
    }
    
    // FSCalendarDataSource
    func calendar(calendar: FSCalendar, imageForDate date: NSDate) -> UIImage? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        for i in 0  ..< eventColorArray.count  {
            let colorDict = eventColorArray.objectAtIndex(i) as! NSDictionary
            let eventDate = colorDict.valueForKey(self.appConstants.event_date) as? String
            if eventDate!.isEqual(dateFormatter.stringFromDate(date)) {
                return UIImage(color: (colorDict.valueForKey(self.appConstants.colour) as? UIColor)!)!
            }
        }
        return UIImage()
    }
    
    func calendar(calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calendar.frame = CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height)
        self.layoutIfNeeded()
    }
   
    func calendarCurrentPageDidChange(calendar: FSCalendar) {
        NSLog("change page to \(calendar.stringFromDate(calendar.currentPage))")
        dayWiseEventsArray.removeAllObjects()
        calculateStartAndEndDate()
        eventsTableView.reloadData()
        self.layoutIfNeeded()
    }
    
    func calendar(calendar: FSCalendar, shouldSelectDate date: NSDate) -> Bool {
        return true
    }
    
    func calendar(calendar: FSCalendar, didSelectDate date: NSDate) {
        NSLog("calendar did select date \(calendar.stringFromDate(date))")
        selectedDate = date
        dayWiseEventsArray.removeAllObjects()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        for i in 0  ..< eventColorArray.count  {
            let colorDict = eventColorArray.objectAtIndex(i) as! NSDictionary
            let eventDate = colorDict.valueForKey(self.appConstants.event_date) as? String
            if eventDate!.isEqual(dateFormatter.stringFromDate(date)) {
                selectedColorForShowedEvent = (colorDict.valueForKey(self.appConstants.colour) as? UIColor)!
                break
            }
        }
        
        for i in 0  ..< monthWiseEventsArray.count  {
            let sDict = monthWiseEventsArray.objectAtIndex(i) as! NSDictionary
            let eventDate = sDict.valueForKey(self.appConstants.event_date) as? String
            if eventDate!.isEqual(dateFormatter.stringFromDate(date)) {
                dayWiseEventsArray.addObject(sDict)
            }
        }
        
        eventsTableView.reloadData()
    }
    
    func calendar(calendar: FSCalendar, shouldDeselectDate date: NSDate) -> Bool {
        return true
    }
    
    func addNewEvent(gestureRecognizer: UITapGestureRecognizer){
        self.subviews.forEach({ $0.removeFromSuperview() })
        AppConstants.CurrentScreen = appConstants.AddOrUpdateEventScreen
        let addEventScreen : AddNewEventView = AddNewEventView(frame: self.frame)
        //addEventScreen.backgroundColor = UIColor().HexToColor(appConstants.add_daily_activity_bg, alpha: 1.0)
        addEventScreen.backgroundColor = UIColor.clearColor()
        addEventScreen.isUpdate = false
        let arr:NSArray = NSArray()
        addEventScreen.showUI(false, memberArray: arr)
        self.addSubview(addEventScreen)
    }
    
    func addImage(gestureRecognizer: UITapGestureRecognizer){
        self.subviews.forEach({ $0.removeFromSuperview() })
        AppConstants.CurrentScreen = appConstants.AddOrUpdateEventScreen
        let addEventScreen : AddNewEventView = AddNewEventView(frame: self.frame)
        addEventScreen.backgroundColor = UIColor().HexToColor(appConstants.add_daily_activity_bg, alpha: 1.0)
        addEventScreen.isUpdate = false
        let arr:NSArray = NSArray()
        addEventScreen.showUI(false, memberArray: arr)
        self.addSubview(addEventScreen)
    }
    
    func nextMonth(gestureRecognizer: UITapGestureRecognizer){
       // 2016-10-01
        let components: NSDateComponents = NSDateComponents()
        components.setValue(1, forComponent: NSCalendarUnit.Month);
        let expirationDate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: (myCalendar?.currentPage)!, options: NSCalendarOptions(rawValue: 0))
        
        myCalendar?.currentPage = expirationDate!
        calendarCurrentPageDidChange((myCalendar)!)
        myCalendar?.reloadData()
        myCalendar?.reloadInputViews()

    }
    
    func previousMonth(gestureRecognizer: UITapGestureRecognizer){
        let components: NSDateComponents = NSDateComponents()
        components.setValue(-1, forComponent: NSCalendarUnit.Month);
        let expirationDate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: (myCalendar?.currentPage)!, options: NSCalendarOptions(rawValue: 0))
        myCalendar?.currentPage = expirationDate!
        calendarCurrentPageDidChange((myCalendar)!)
        myCalendar?.reloadData()
        myCalendar?.reloadInputViews()

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
       return 46 * appConstants.ScreenFactor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = EventTableCellView(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        cell.setData(self.dayWiseEventsArray[indexPath.row] as! NSDictionary, selectedColor: selectedColorForShowedEvent, membersArray: eventMembersArray)
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dayWiseEventsArray.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.subviews.forEach({ $0.removeFromSuperview() })
        AppConstants.CurrentScreen = appConstants.AddOrUpdateEventScreen
        let dict: NSDictionary = self.dayWiseEventsArray[indexPath.row] as! NSDictionary
        // let attendingArray: NSArray = (dict.objectForKey(appConstants.attending) as? NSArray)!
        
        let attendingArray: NSArray = (dict.objectForKey("attending") as? NSArray)!


        let addEventScreen : AddNewEventView = AddNewEventView(frame: self.frame)
        //addEventScreen.backgroundColor = UIColor().HexToColor(appConstants.add_daily_activity_bg, alpha: 1.0)
        addEventScreen.backgroundColor = UIColor.clearColor()
        print("dict = \(dict)")
        print("attendingArray = \(attendingArray)")

        addEventScreen.alreadySetDictionary = dict
        addEventScreen.membersArray = attendingArray
        addEventScreen.isUpdate = true
      
        addEventScreen.showUI(true, memberArray:attendingArray)
        self.addSubview(addEventScreen)
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
 }

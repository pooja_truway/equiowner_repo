//
//  SubActivityView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 20/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Foundation

class SubActivityView: UITableViewCell, UITextFieldDelegate {
    
    var yy:CGFloat = 0, margin:CGFloat = 0
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    
    var cardLayout : UIView = UIView()
    var vline: UIView?
    var deleteSubActivity: UIImageView = UIImageView()
    
    var descLabel:SkyFloatingLabelTextField = SkyFloatingLabelTextField()
    let timeData : UITextField = UITextField()
    
    var dropMenuDailyType: UILabel = UILabel()
    var dropMenuUnit: UILabel = UILabel()
    let blankView: UIView = UIView()

    var subActivityIndex: NSIndexPath = NSIndexPath()
    var removeCallback: SubActivityRemovedCallback?

    var dataModel: NSMutableDictionary = NSMutableDictionary()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        initializeVariables()
        setUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        yy = 3 * appConstants.ScreenFactor
        cardLayout.frame = CGRectMake(0, yy ,self.bounds.size.width, self.bounds.size.height)
        descLabel.frame = CGRectMake(margin,yy ,cardLayout.frame.size.width - 3*margin, 24 * appConstants.ScreenFactor)
        deleteSubActivity.frame = CGRectMake(self.bounds.size.width - 3*margin,8,14 * appConstants.ScreenFactor,14 * appConstants.ScreenFactor)
        yy += descLabel.frame.size.height
        dropMenuDailyType.frame = CGRectMake(margin, yy ,cardLayout.frame.size.width - 2*margin, 20 * appConstants.ScreenFactor)
        yy += dropMenuDailyType.frame.size.height + 10
        dropMenuUnit.frame = CGRectMake(margin, yy ,cardLayout.frame.size.width - 2*margin, 20 * appConstants.ScreenFactor)
        blankView.frame = CGRectMake(0 , self.bounds.size.height - 3 * appConstants.ScreenFactor , self.bounds.size.width, 5 * appConstants.ScreenFactor)
        vline!.frame = CGRectMake(0 , 0 , 1, self.bounds.size.height - blankView.frame.size.height)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeVariables() {
        yy = 3 * appConstants.ScreenFactor
        margin = 5 * appConstants.ScreenFactor
    }
    
    func setUI(){
        cardLayout = UIView(frame:CGRectMake(0, yy ,self.bounds.size.width, self.bounds.size.height))
        cardLayout.backgroundColor = UIColor.whiteColor();
        contentView.addSubview(cardLayout)
        
        vline = UIView(frame: CGRectMake(0 , 0 , 1, self.bounds.size.height))
        vline!.backgroundColor = UIColor().HexToColor(appConstants.button_color)
        cardLayout.addSubview(vline!)
        
        descLabel = SkyFloatingLabelTextField(frame: CGRectMake(margin,yy ,cardLayout.frame.size.width - 3*margin, 24 * appConstants.ScreenFactor))
        descLabel.placeholder = "Add Description"
        descLabel.title = "Add Description"
        descLabel.delegate = self
        appConstants.styleFloatingFields(descLabel)
        cardLayout.addSubview(descLabel)
        
        deleteSubActivity.frame = CGRectMake(self.frame.size.width - 3*margin,8,12 * appConstants.ScreenFactor,12 * appConstants.ScreenFactor)
        deleteSubActivity.image = UIImage.init(imageLiteral: "cross.png")
        deleteSubActivity.userInteractionEnabled = true
        let delete = UITapGestureRecognizer(target: self, action: #selector(removeSubActivityLayout(_:)))
        deleteSubActivity.addGestureRecognizer(delete)
        contentView.addSubview(deleteSubActivity)
        
        yy += descLabel.frame.size.height
        
        //Amount type
        dropMenuDailyType.frame = CGRectMake(margin, yy ,cardLayout.frame.size.width - 2*margin, 20 * appConstants.ScreenFactor)
        dropMenuDailyType.backgroundColor = UIColor().HexToColor("#f8f8f9")
        dropMenuDailyType.layer.borderColor = UIColor().HexToColor("#ededee").CGColor
        dropMenuDailyType.layer.borderWidth = 1
        dropMenuDailyType.sizeThatFits(cardLayout.frame.size)
        dropMenuDailyType.userInteractionEnabled = true
        dropMenuDailyType.tag = 100
        dropMenuDailyType.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        dropMenuDailyType.text = "  Input"
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(showDropDown(_:)))
        dropMenuDailyType.addGestureRecognizer(tapRecognizer)
        cardLayout.addSubview(dropMenuDailyType)

        let dropdownBg : UIView = UIView()
        dropdownBg.frame = CGRectMake(cardLayout.frame.size.width - 30 * appConstants.ScreenFactor - 2*margin,0,30 * appConstants.ScreenFactor,20 * appConstants.ScreenFactor)
        dropdownBg.backgroundColor = UIColor().HexToColor(appConstants.button_color)
        dropMenuDailyType.addSubview(dropdownBg)
        
        let dropdownIcon : UIImageView = UIImageView()
        dropdownIcon.frame = CGRectMake(0,0,13 * appConstants.ScreenFactor,7 * appConstants.ScreenFactor)
        dropdownIcon.center = CGPointMake(dropdownBg.frame.size.width/2, dropdownBg.frame.size.height/2)
        dropdownIcon.image = UIImage.init(imageLiteral: "dropdown.png")
        dropdownBg.addSubview(dropdownIcon)
        
        yy += dropMenuDailyType.frame.size.height + 10
        
        addUnitLayout()
    }
    
    func addUnitLayout(){
        //Unit type
        dropMenuUnit.frame = CGRectMake(margin, yy ,cardLayout.frame.size.width - 2*margin, 20 * appConstants.ScreenFactor)
        dropMenuUnit.backgroundColor = UIColor().HexToColor("#f8f8f9")
        dropMenuUnit.layer.borderColor = UIColor().HexToColor("#ededee").CGColor
        dropMenuUnit.layer.borderWidth = 1
        dropMenuUnit.sizeThatFits(cardLayout.frame.size)
        dropMenuUnit.userInteractionEnabled = true
        dropMenuUnit.tag = 200
        dropMenuUnit.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        dropMenuUnit.text = appConstants.unitsItems[0]
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(showDropDown(_:)))
        dropMenuUnit.addGestureRecognizer(tapRecognizer)
        cardLayout.addSubview(dropMenuUnit)
   
        let unitdropdownBg : UIView = UIView()
        unitdropdownBg.frame = CGRectMake(cardLayout.frame.size.width - 30 * appConstants.ScreenFactor - 2*margin,0,30 * appConstants.ScreenFactor,20 * appConstants.ScreenFactor)
        unitdropdownBg.backgroundColor = UIColor().HexToColor(appConstants.button_color)
        dropMenuUnit.addSubview(unitdropdownBg)
        
        let unitdropdownIcon : UIImageView = UIImageView()
        unitdropdownIcon.frame = CGRectMake(0,0,13 * appConstants.ScreenFactor,7 * appConstants.ScreenFactor)
        unitdropdownIcon.center = CGPointMake(unitdropdownBg.frame.size.width/2, unitdropdownBg.frame.size.height/2)
        unitdropdownIcon.image = UIImage.init(imageLiteral: "dropdown.png")
        unitdropdownBg.addSubview(unitdropdownIcon)
        
        cardLayout.frame.size.height += dropMenuUnit.frame.size.height + 20
        contentView.frame.size.height = cardLayout.frame.size.height
        vline!.frame = CGRectMake(0 , 0 , 1, self.frame.size.height - blankView.frame.size.height)
        contentView.updateConstraints()
        
        blankView.backgroundColor = UIColor().HexToColor(appConstants.add_daily_activity_bg, alpha: 1.0)
        contentView.addSubview(blankView)
    }
    
    func dataInLayout(data: NSMutableDictionary) {
        
    }
    
    func removeUnitLayout(){
        dropMenuUnit.hidden = true
        print("Remove ",self.frame.size.height);
        self.updateConstraints()
        vline!.frame = CGRectMake(0 , 0 , 1, self.bounds.size.height)
        
    }
    
    func showUnitLayout(){
        dropMenuUnit.hidden = false
        print("Add ",self.frame.size.height);
        self.updateConstraints()
        vline!.frame = CGRectMake(0 , 0 , 1, self.bounds.size.height)
    }

    
    func removeSubActivityLayout(gestureRecognizer: UITapGestureRecognizer){
        removeCallback!.itemRemoved(subActivityIndex)
    }

    
    func showDropDown(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!
        if viewTouched.tag == 100 {
          showDialogForSelection(0)
        }else{
           showDialogForSelection(1)
        }
    }
    
    private func showDialogForSelection(index: Int) {
        let alertController = UIAlertController(title: "Select", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let slotsList = index == 0 ? appConstants.dailyItems : appConstants.unitsItems
        for i in 0  ..< slotsList.count  {
            let slotValue = slotsList[i] as String
            let oneAction = UIAlertAction(title: String(slotValue), style: .Default, handler: { (action: UIAlertAction!) in
                if index == 0 {
                    self.dropMenuDailyType.text = slotValue
                    if i == 1 {
                        self.removeUnitLayout()
                    }else{
                        self.showUnitLayout()
                    }
                }else{
                    self.dropMenuUnit.text = slotValue
                }
            })
            alertController.addAction(oneAction)
        }
        let Cancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(Cancel)
        let appDelegate  = UIApplication.sharedApplication().delegate as! AppDelegate
        let viewController = appDelegate.window!.rootViewController
        viewController!.presentViewController(alertController, animated: true, completion: nil)
    }
    
    //TextField Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        self.endEditing(true)
        return true;
    }
}
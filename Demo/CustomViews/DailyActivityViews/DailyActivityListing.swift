//
//  DailyActivityListing.swift
//  Demo
//
//  Created by KOMAL SHARMA on 14/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class DailyActivityListing: UIView, UITableViewDelegate, UITableViewDataSource {
    var fromWhere: String = ""
    var addButton: UIButton = UIButton()
    var addTaskLabel: UILabel = UILabel()

    var tableView: UITableView  =   UITableView()
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var dailyActivityList:NSArray = NSArray()
    var cellIdendifier: String = "DailyActivityListCell"
    
    var horseID: String = ""
    var indicator = UIActivityIndicatorView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let bottomLayout =  UIView.init(frame: CGRectMake(0, self.frame.size.height - 30 * appConstants.ScreenFactor, self.frame.size.width , 30 * appConstants.ScreenFactor))
    //    bottomLayout.backgroundColor = UIColor.whiteColor()
        bottomLayout.backgroundColor = UIColor.clearColor()
        //self.addSubview(bottomLayout)
        
        let hline: UIView = UIView(frame : CGRectMake(0, self.frame.size.height - 30 * appConstants.ScreenFactor, self.frame.size.width, 1))
        hline.backgroundColor = UIColor.blackColor()
       // self.addSubview(hline)
        
        addButton = UIButton.init(frame: CGRectMake(bottomLayout.frame.size.width - 30 * appConstants.ScreenFactor ,0, 20 * appConstants.ScreenFactor,20 * appConstants.ScreenFactor))
        addButton.center = CGPointMake(addButton.center.x, bottomLayout.frame.size.height/2)
        addButton.setImage(UIImage.init(imageLiteral: "plus.png"), forState: UIControlState.Normal)
        addButton.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(addNewActivity(_:)))
        addButton.addGestureRecognizer(tapRecognizer)
        
        //bottomLayout.addSubview(addButton)
        
        /*if !SharedPreferenceUtils._sharedInstance.getBoolValue(appConstants.ISOWNER) {
            addTaskLabel = UILabel.init(frame: CGRectMake(10, 0, self.frame.size.width - addButton.frame.size.width - 20 * appConstants.ScreenFactor, 30 * appConstants.ScreenFactor))
            addTaskLabel.textColor = UIColor.blackColor()
            addTaskLabel.text = "Add New Task"
            addTaskLabel.font = UIFont.init(name: "Helvetica", size: 9 * appConstants.ScreenFactor)
            addTaskLabel.backgroundColor = UIColor.clearColor()
            bottomLayout.addSubview(addTaskLabel)
        }*/
        
        tableView.frame             =   CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - addTaskLabel.frame.size.height)
        tableView.delegate          =   self
        tableView.dataSource        =   self
       // tableView.backgroundColor   = UIColor.whiteColor()
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.SingleLine;
        tableView.separatorColor    = UIColor.blackColor().colorWithAlphaComponent(0.8);
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        self.addSubview(tableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addNewActivity(gestureRecognizer: UITapGestureRecognizer){
        self.subviews.forEach({ $0.removeFromSuperview() })
        let dailyActivityAddNewTask:DailyActivityAddNewTask = DailyActivityAddNewTask(frame : self.frame)
        dailyActivityAddNewTask.backgroundColor = UIColor().HexToColor(appConstants.add_daily_activity_bg, alpha: 1.0)
        dailyActivityAddNewTask.horseID = horseID
        dailyActivityAddNewTask.fromWhere = fromWhere

        if fromWhere == "HORSE_DETAIL" {
            AppConstants.CurrentScreen = appConstants.AddDailyActivityFromDetailScreen
        }else {
            AppConstants.CurrentScreen = appConstants.AddDailyActivityScreen
        }
        self.addSubview(dailyActivityAddNewTask)
    }
    
    func getHorseDailyActivityList(horseID: String){
        showProgress()
        
        self.horseID = horseID
        let defaults = NSUserDefaults.standardUserDefaults()
            let trainerID = defaults.valueForKey("trainer_id")as! String
        print(trainerID)
        
        //send to server
    let postString = "action=dailyHorseActivityList&userId="+trainerID+"&horse_id="+horseID+"&start_date="+appConstants.getCurrentDate();
        // send trainer id to resolve the issue of daily activity
        
      //  let postString = "action=dailyHorseActivityList&userId="+defaults.valueForKey("trainer_id")+"&horse_id="+horseID+"&start_date="+appConstants.getCurrentDate();
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                
                if error != nil{
                    return
                }
                
                print("response activity list= \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        print("mainList = \(mainList)")
                        let outerActivityList:NSArray = mainList.objectForKey(self.appConstants.activity) as! NSArray
                        let innerData:NSDictionary = outerActivityList.objectAtIndex(0) as! NSDictionary
                        self.dailyActivityList = innerData.objectForKey(self.appConstants.activity) as! NSArray
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }

    func getHorseDetail(horseID: String){
        self.horseID = horseID

        //send to server
        let postString = "action=horsedetail&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID)+"&horse_id="+horseID;
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                if error != nil{
                    return
                }
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                        // self.horseList = mainList.objectForKey(self.appConstants.horses) as! NSArray
                        print("mainList = \(mainList)")
                        let baseURL = mainList.objectForKey(self.appConstants.media_base_url) as? String
                        let horseData:NSDictionary = (mainList[self.appConstants.horses])! as! NSDictionary
                        let mediaSource:String = (horseData.objectForKey(self.appConstants.media_source) as? String)!
                        var notificationDict: NSDictionary = NSDictionary()
                        let media_type:String = (horseData.objectForKey(self.appConstants.media_type) as? String)!
                      
                        if media_type == "image" {
                            //Image
                            notificationDict = [self.appConstants.name: (horseData.objectForKey(self.appConstants.name) as? String)!, self.appConstants.profice_pic: baseURL!+mediaSource]
                        }else{
                            //Video
                            notificationDict = [self.appConstants.name: (horseData.objectForKey(self.appConstants.name) as? String)!, self.appConstants.profice_pic: ""]
                        }
                        
                        NSNotificationCenter.defaultCenter().postNotificationName(self.appConstants.kN_ShowHorseCredentailsInHeaderNotification, object: notificationDict)
                    }
                }
            }
        }
    }


    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 35 * appConstants.ScreenFactor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = DailyActivityTableCellView(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        cell.backgroundColor = UIColor.clearColor()
        if(self.dailyActivityList.count > 0){
            cell.setDailyItemData(self.dailyActivityList[indexPath.row] as! NSDictionary)
        }
        cell.separatorInset = UIEdgeInsetsZero
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dailyActivityList.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        let dictToPass: NSDictionary = self.dailyActivityList[indexPath.row] as! NSDictionary
        print("dictToPass = \(dictToPass)")

        let difference:NSTimeInterval = appConstants.getDailyActivityFormattedDate((dictToPass.objectForKey(appConstants.time) as? String)!).timeIntervalSinceNow
        
        let ti = NSInteger(difference)
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        if((hours == 0 && minutes < -15) || (hours <= 0 && minutes < 0)) {
        
            self.subviews.forEach({ $0.removeFromSuperview() })
            let dailyActivityDetail:DailyActivityDetail = DailyActivityDetail(frame : self.frame)
         //   dailyActivityDetail.backgroundColor = UIColor().HexToColor(appConstants.add_daily_activity_bg, alpha: 1.0)
            dailyActivityDetail.backgroundColor = UIColor.clearColor()
            dailyActivityDetail.drawUI(dictToPass)
            dailyActivityDetail.fromWhere = fromWhere
            if fromWhere == "HORSE_DETAIL" {
                AppConstants.CurrentScreen = appConstants.DailyActivityDetailScreenFromDetailScreen
            }else {
                AppConstants.CurrentScreen = appConstants.DailyActivityDetailScreen
            }
            
            self.addSubview(dailyActivityDetail)
        }
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
}

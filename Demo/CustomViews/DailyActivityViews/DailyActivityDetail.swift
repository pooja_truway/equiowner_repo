//
//  DailyActivityDetail.swift
//  Demo
//
//  Created by KOMAL SHARMA on 21/10/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit

class DailyActivityDetail: UIView, CounterChangeCallback{
    var scrollView:UIScrollView?
    var yy:CGFloat = 0, margin:CGFloat = 0
    
    var webService: WebService = WebService()
    var appConstants: AppConstants = AppConstants()
    
    var activeView: UIView?
    
    var media_base_url : String?
    var userData : NSDictionary = NSDictionary()
    
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    var indicator = UIActivityIndicatorView()
    
    var steps:UILabel = UILabel()
    var stepsText:UILabel = UILabel()
    var stepsMainText:UILabel = UILabel()
    var total: Int = 0
    var doneCount: Int = 0

    var fromWhere: String = ""
    var subActivityList:NSArray = NSArray()
    var viewsArray:NSMutableArray = NSMutableArray()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addNotifications()
        initializeVariables()
        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
//        self.addGestureRecognizer(tap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    func addNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: nil);
        
    }
    
    func keyboardWillShow(sender: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        scrollView!.scrollEnabled = true
        let info : NSDictionary = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        scrollView!.contentInset = contentInsets
        scrollView!.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.frame
        aRect.size.height -= keyboardSize!.height
        if activeView != nil {
            if (!CGRectContainsPoint(aRect, activeView!.frame.origin))
            {
                scrollView!.scrollRectToVisible(activeView!.frame, animated: true)
            }
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let info : NSDictionary = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        scrollView!.contentInset = UIEdgeInsetsZero
        scrollView!.scrollIndicatorInsets = contentInsets
        dismissKeyboard()
        //scrollView!.scrollEnabled = false
    }
    
    func initializeVariables() {
        yy = 0
        margin = 5 * appConstants.ScreenFactor
    }
    
    func drawUI(data: NSDictionary) {
        AppConstants.dataModelForDetailEditing = data.mutableCopy() as! NSMutableDictionary
        AppConstants.checkBoxImageArray = NSMutableArray()

        subActivityList = data.objectForKey(self.appConstants.tasklist) as! NSArray
        total = subActivityList.count
        drawContainer()
        scrollView = UIScrollView.init(frame: CGRectMake(0,yy ,self.frame.size.width,self.frame.size.height - self.frame.size.height * 0.25))
       // scrollView!.backgroundColor = UIColor().HexToColor(appConstants.daily_light_bg)
        scrollView!.backgroundColor = UIColor.clearColor()
        scrollView!.contentSize = self.frame.size
        scrollView!.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        
        self.addSubview(scrollView!)
   
        yy = 0
        for i in 0  ..< subActivityList.count  {
            let arr = NSMutableArray()
            AppConstants.checkBoxImageArray.addObject(arr)
            
            let subActivity = subActivityList.objectAtIndex(i) as! NSDictionary
            let itemToDisplay = ItemView(frame: CGRect(x: 0, y: yy, width: self.frame.size.width, height: 140))
            itemToDisplay.mCounterChangeCallback = self
            itemToDisplay.autoresizesSubviews = true
            itemToDisplay.drawItems(subActivity, tag: i)
            scrollView!.addSubview(itemToDisplay)
            yy += itemToDisplay.frame.size.height
            
            viewsArray.addObject(itemToDisplay)
            
            if !SharedPreferenceUtils._sharedInstance.getBoolValue(appConstants.ISOWNER) {
                itemToDisplay.userInteractionEnabled = true
            }else{
                itemToDisplay.userInteractionEnabled = false
            }
        }
       /* if self!.requestData.itemStore.count > 0 {
            yy = 10
            for model: ItemView in self!.requestData.itemStore {
                var itemToDisplay = OrderItemsDisplayView(frame: CGRect(x: 0, y: yy, width: self!.frame.size.width, height: 140))
                itemToDisplay.drawItems(withData: model, in: parentView, inMyRequest: false)
                itemToDisplay.userInteractionEnabled = false
                itemToDisplay.autoresizesSubviews = true
                scrollView.addSubview(itemToDisplay)
                yy += itemToDisplay.frame.size.height + 15
                self!.addSeperatorView(in: scrollView)
            }
        }*/
        scrollView!.contentSize = CGSize.init(width: self.frame.size.width, height: yy)
        
    }
    
    func styleLabels(label: UILabel){
        label.textColor = UIColor.whiteColor()
        label.font = UIFont.init(name: "Helvetica", size: 7 * appConstants.ScreenFactor)
        label.backgroundColor = UIColor.clearColor()
    }
    
    func setData() {
        /*userNameContainer?.entryView.text = userData.objectForKey(appConstants.firstName) as? String
        addressContainer?.entryView.text = userData.objectForKey(appConstants.address) as? String
        lastNameContainer?.entryView.text = userData.objectForKey(appConstants.lastName) as? String
        phoneContainer?.entryView.text = userData.objectForKey(appConstants.phone) as? String
        
        let mediaSource:String = (userData.objectForKey(appConstants.profilePic) as? String)!
        userImage.downloadedFrom(media_base_url!+mediaSource)*/
    }
    
    func drawContainer(){
        let container = UIView.init(frame: CGRectMake(0,yy ,self.frame.size.width,self.frame.size.height * 0.25))
        //container.backgroundColor = UIColor().HexToColor(appConstants.daily_dark_bg)
        container.backgroundColor = UIColor.clearColor()
        self.addSubview(container)
        
        let stepsContainer = UIView.init(frame: CGRectMake(7 * appConstants.ScreenFactor, 7 * appConstants.ScreenFactor ,container.frame.size.width * 0.2,container.frame.size.height - 14 * appConstants.ScreenFactor))
      //  stepsContainer.backgroundColor = UIColor().HexToColor(appConstants.font_white_color).colorWithAlphaComponent(0.3)
        stepsContainer.backgroundColor = UIColor.clearColor()
        stepsContainer.layer.borderWidth = 1
        stepsContainer.layer.cornerRadius = 5
        stepsContainer.layer.borderColor = UIColor().HexToColor(appConstants.bg_light_grey).CGColor
        container.addSubview(stepsContainer)
        
        
        steps = UILabel.init(frame: CGRectMake(0, 2 * appConstants.ScreenFactor, stepsContainer.frame.size.width, stepsContainer.frame.size.height * 0.7))
        steps.textAlignment = .Center
        steps.textColor = UIColor.whiteColor()
        steps.font = UIFont.init(name: "Helvetica", size: 20 * appConstants.ScreenFactor)
        steps.text = "0"
        steps.backgroundColor = UIColor.clearColor()
        stepsContainer.addSubview(steps)
       
        stepsText = UILabel.init(frame: CGRectMake(0, stepsContainer.frame.size.height * 0.4, stepsContainer.frame.size.width, stepsContainer.frame.size.height * 0.6))
        stepsText.textAlignment = .Center
        stepsText.numberOfLines = 2
        stepsText.textColor = UIColor.whiteColor()
        stepsText.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        stepsText.text = "steps\nTaken"
        stepsText.backgroundColor = UIColor.clearColor()
        stepsContainer.addSubview(stepsText)
        
        stepsMainText = UILabel.init(frame: CGRectMake(container.frame.size.width * 0.3, 2 * appConstants.ScreenFactor ,container.frame.size.width * 0.7 - 3 * appConstants.ScreenFactor,container.frame.size.height - 4 * appConstants.ScreenFactor))
        stepsMainText.textAlignment = .Left
        stepsMainText.textColor = UIColor.whiteColor()
        stepsMainText.numberOfLines = 5
        stepsMainText.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        stepsMainText.text = ""
        stepsMainText.backgroundColor = UIColor.clearColor()
        container.addSubview(stepsMainText)

        updateStepsText(0)
        yy += container.frame.size.height
    }
    
    func updateStepsText(value: Int){
        stepsMainText.text = "You have completed " + String(value) + " of our list of " +  String(total) + " actions to help you get the best from the Trainer"
        steps.text = String(value)
    }
    
    func showProgress() {
        indicator.removeFromSuperview()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        indicator = UIActivityIndicatorView(frame: self.bounds)
        indicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        indicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        indicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.addSubview(indicator)
        indicator.userInteractionEnabled = false
        indicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.indicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    
    func countValue(toAdd: Bool) {
        if toAdd {
            doneCount += 1
        }else {
            doneCount -= 1
        }
        updateStepsText(doneCount)
    }
    
    func bringScrollTofront() {
        dismissKeyboard()
        scrollView!.contentSize = CGSize.init(width: self.frame.size.width, height: yy)
    }
}

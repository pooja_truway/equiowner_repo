//
//  DailyActivityAddNewTask.swift
//  Demo
//
//  Created by KOMAL SHARMA on 20/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class DailyActivityAddNewTask: UIView, UITextFieldDelegate, SubActivityRemovedCallback, UITableViewDelegate, UITableViewDataSource {
    var fromWhere: String = ""

    var yy:CGFloat = 0, margin:CGFloat = 0, sYY:CGFloat = 0
    var addButton: UIButton = UIButton()
    var submitButton: UIButton = UIButton()
    
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var dailyActivityList:NSArray = NSArray()
    
    var horseID: String = ""

    var headingLabel:SkyFloatingLabelTextField = SkyFloatingLabelTextField()
    let timeData : UITextField = UITextField()

    var subActivityViewArray: NSMutableArray = NSMutableArray()
    var tableView: UITableView  =   UITableView()
    var cellIdendifier: String = "SubActivityCell"
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeVariables()
        setTopLayer()
        setBottomButtons()
        addSubActivity()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.endEditing(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeVariables() {
        yy = 5
        margin = 5 * appConstants.ScreenFactor
        sYY = 0
    }
    
    func setTopLayer(){
        let activityLabel : UILabel = UILabel()
        activityLabel.frame = CGRectMake(margin,yy,self.frame.size.width,13 * appConstants.ScreenFactor)
        activityLabel.text = "Activity"
        appConstants.styleLabels(activityLabel)
        self.addSubview(activityLabel)
        
         yy+=activityLabel.frame.size.height
        
        let dailyActivityLayout : UIView = UIView(frame:CGRectMake(margin,yy,self.frame.size.width - 2*margin,55 * appConstants.ScreenFactor))
        dailyActivityLayout.backgroundColor = UIColor.whiteColor();
        self.addSubview(dailyActivityLayout)
        
        let vline1: UIView = UIView(frame : CGRectMake(0 , 0 , 1, dailyActivityLayout.frame.size.height))
        vline1.backgroundColor = UIColor().HexToColor(appConstants.button_color)
        dailyActivityLayout.addSubview(vline1)
        
        headingLabel = SkyFloatingLabelTextField(frame: CGRectMake(margin, 3 * appConstants.ScreenFactor,dailyActivityLayout.frame.size.width - 2*margin, 24 * appConstants.ScreenFactor))
        headingLabel.placeholder = "Add a Heading"
        headingLabel.title = "Add a Heading"
        headingLabel.delegate = self
        appConstants.styleFloatingFields(headingLabel)
        dailyActivityLayout.addSubview(headingLabel)
        
        let timeLayout : UIView = UIView(frame:CGRectMake(margin, 28 * appConstants.ScreenFactor,dailyActivityLayout.frame.size.width - 2*margin, 25 * appConstants.ScreenFactor))
        timeLayout.backgroundColor = UIColor.clearColor();
        dailyActivityLayout.addSubview(timeLayout)

        
        let timeLabel : UILabel = UILabel()
        timeLabel.frame = CGRectMake(0,0,self.frame.size.width,12 * appConstants.ScreenFactor)
        timeLabel.text = "Time"
        appConstants.styleLabels(timeLabel)
        timeLayout.addSubview(timeLabel)
        
        let timeIcon : UIImageView = UIImageView()
        timeIcon.frame = CGRectMake(0,14 * appConstants.ScreenFactor,9 * appConstants.ScreenFactor,9 * appConstants.ScreenFactor)
        timeIcon.image = UIImage.init(imageLiteral: "clock.png")
        timeLayout.addSubview(timeIcon)
        
        timeData.frame = CGRectMake(timeIcon.frame.size.width + 5,13 * appConstants.ScreenFactor,self.frame.size.width - (timeIcon.frame.size.width + 10),12 * appConstants.ScreenFactor)
        timeData.text = "Tap to Add Time"
        appConstants.styleInnerLabels(timeData)
        timeLayout.addSubview(timeData)
        let starttimedatePicker = UIDatePicker()
        starttimedatePicker.datePickerMode = UIDatePickerMode.Time
        starttimedatePicker.locale = NSLocale(localeIdentifier: "en_GB") // using Great Britain for 24 hr format

        timeData.inputView = starttimedatePicker
        starttimedatePicker.addTarget(self, action: #selector(startTimeDiveChanged(_:)), forControlEvents: .ValueChanged)
        
        yy+=dailyActivityLayout.frame.size.height + 10
        
        let subActivityLabel : UILabel = UILabel()
        subActivityLabel.frame = CGRectMake(margin,yy,self.frame.size.width,13 * appConstants.ScreenFactor)
        subActivityLabel.text = "Sub Activity"
        appConstants.styleLabels(subActivityLabel)
        self.addSubview(subActivityLabel)
        
        yy+=subActivityLabel.frame.size.height
        
        tableView.frame             =   CGRectMake(margin,yy , self.frame.size.width - 2 * margin,self.frame.size.height - yy)
        tableView.delegate          =   self
        tableView.dataSource        =   self
        tableView.backgroundColor   = UIColor.clearColor()
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.None;
        tableView.separatorColor    = UIColor.clearColor()
        tableView.separatorInset    = UIEdgeInsetsZero
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70 * appConstants.ScreenFactor
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdendifier)
        self.addSubview(tableView)
    }
    
    func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_GB") // using Great Britain for 24 hr format
        formatter.timeStyle = .ShortStyle
        timeData.text = formatter.stringFromDate(sender.date)
    }

    func setBottomButtons(){
        yy = self.frame.size.height * 0.91
        let bottomItemWidth:CGFloat = self.frame.size.width/2
        
        addButton = UIButton.init(frame: CGRectMake(margin,yy,bottomItemWidth - 1.5*margin,22 * appConstants.ScreenFactor))
        addButton.layer.cornerRadius = appConstants.CornerRadius
        addButton.setTitle("Add", forState: UIControlState.Normal)
        addButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        addButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        addButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        addButton.userInteractionEnabled = true
        addButton.tag = 1
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(buttonPressed(_:)))
        addButton.addGestureRecognizer(tapRecognizer)
        self.addSubview(addButton)
        
        submitButton = UIButton.init(frame: CGRectMake(bottomItemWidth,yy,bottomItemWidth - margin,22 * appConstants.ScreenFactor))
        submitButton.layer.cornerRadius = appConstants.CornerRadius
        submitButton.setTitle("Submit", forState: UIControlState.Normal)
        submitButton.titleLabel?.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        submitButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        submitButton.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        submitButton.userInteractionEnabled = true
        submitButton.tag = 2
        let tapRecognizerSubmit = UITapGestureRecognizer(target: self, action: #selector(buttonPressed(_:)))
        submitButton.addGestureRecognizer(tapRecognizerSubmit)
        self.addSubview(submitButton)
        
        tableView.frame.size.height -= self.frame.size.height * 0.13
    }
    
    func addSubActivity(){
        let data = NSMutableDictionary()
        data.setValue("", forKey: self.appConstants.description)
        subActivityViewArray.addObject(data)
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 83 * appConstants.ScreenFactor
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let subActivity = SubActivityView(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdendifier)
        subActivity.subActivityIndex = indexPath
        subActivity.removeCallback = self
        subActivity.separatorInset = UIEdgeInsetsZero
        subActivity.selectionStyle = UITableViewCellSelectionStyle.None
        subActivity.dataInLayout(subActivityViewArray.objectAtIndex(indexPath.row) as! NSMutableDictionary)
        return subActivity
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subActivityViewArray.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        dismissKeyboard()
    }
    
    func itemSelectedWithIndex(index: Int, name: String) {
        print("\(name) selected");
    }
    
    func buttonPressed(gestureRecognizer: UITapGestureRecognizer) {
        let viewTouched: UIView = gestureRecognizer.view!
        switch viewTouched.tag {
        case 1:
            //Add
            addSubActivity()
            break
        case 2:
            //Submit
            break
        default:
            break
        }
    }
    
    func itemRemoved(index: NSIndexPath) {
        if subActivityViewArray.count > 1 {
            subActivityViewArray.removeObjectAtIndex(index.row)
            tableView.deleteRowsAtIndexPaths([index], withRowAnimation: UITableViewRowAnimation.Fade)
            tableView.reloadData()
        }else{
            appConstants.showAlert("Oops!", message: "Could not delete this Sub Activity. There has to be atleast 1 Sub Activity!", controller: vc!)
        }
    }
    
    //TextField Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        dismissKeyboard()
        return true;
    }
}
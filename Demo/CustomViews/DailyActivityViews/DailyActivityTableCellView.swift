//
//  DailyActivityTableCellView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 07/10/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation
import UIKit

class DailyActivityTableCellView: UITableViewCell {
    var daily_item_time: UILabel = UILabel()
    var daily_item_title: UILabel = UILabel()
    var daily_item_status: UIImageView = UIImageView()

    var appConstants: AppConstants = AppConstants()
    var horseSelected: ListItemCallback?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        daily_item_time = UILabel.init(frame: CGRectZero)
        daily_item_time.textColor = UIColor.whiteColor()
        daily_item_time.textAlignment = .Center
        daily_item_time.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        daily_item_time.backgroundColor = UIColor().HexToColor(self.appConstants.bg_light_grey)
        contentView.addSubview(daily_item_time)
        
        daily_item_title = UILabel.init(frame: CGRectZero)
    //    daily_item_title.textColor = UIColor.blackColor()
        daily_item_title.textColor = UIColor.whiteColor()
        daily_item_title.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        daily_item_title.backgroundColor = UIColor.clearColor()
        contentView.addSubview(daily_item_title)
        
        daily_item_status = UIImageView.init(frame: CGRectZero)
        daily_item_status.image = UIImage.init(named: "greyclock.png")
        contentView.addSubview(daily_item_status)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        daily_item_time.frame = CGRectMake(0, 0, 40 * appConstants.ScreenFactor, self.bounds.size.height)

        daily_item_status.frame = CGRectMake(self.bounds.size.width - 30 * appConstants.ScreenFactor ,0, 25 * appConstants.ScreenFactor,25 * appConstants.ScreenFactor)
        daily_item_status.center = CGPointMake(daily_item_status.center.x, self.bounds.size.height/2)
                
        daily_item_title.frame = CGRectMake(daily_item_time.frame.size.width + 10, 0, frame.size.width - 33 * appConstants.ScreenFactor, self.bounds.size.height)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setDailyItemData(model: NSDictionary)  {
        //daily_item_time.text = model.objectForKey(appConstants.time) as? String
        let myString: String = (model.objectForKey(appConstants.time) as? String)!
        let myStringArr = myString.componentsSeparatedByString(":")
        daily_item_time.text = myStringArr[0] + ":" + myStringArr[1]
        
        
       // daily_item_time.text = model.objectForKey(appConstants.time) as? String
        daily_item_title.text = model.objectForKey(appConstants.task) as? String
        daily_item_status.image = UIImage.init(named: "greyclock.png")
        daily_item_time.backgroundColor = UIColor().HexToColor(self.appConstants.bg_light_grey)
        //Checks For time and Clock
        // No need to look for tasks that are done, they are already removed from server: Green color + blue condition removed
        let subActivityList:NSArray = model.objectForKey(self.appConstants.tasklist) as! NSArray
        let doneTask: Bool = isTasksDone(subActivityList)
        

        if(doneTask){
            // b. Green:  If all completed
            daily_item_time.backgroundColor = UIColor().HexToColor(self.appConstants.daily_sea)
            daily_item_status.image = UIImage.init(named: "blueclock.png")
        }else if(!doneTask && isTimeLeft((model.objectForKey(appConstants.time) as? String)!) == -1){
            // c. Red: If any one of the item is not fill or not start activity 1 hour after the prior time
            daily_item_time.backgroundColor = UIColor().HexToColor(self.appConstants.daily_pink)
            daily_item_status.image = UIImage.init(named: "redclock.png")
        }else if(isTimeLeft((model.objectForKey(appConstants.time) as? String)!) == 1){
            // d. Blue: Before 15 minute of the activity till next 1 hour
            daily_item_time.backgroundColor = UIColor().HexToColor(self.appConstants.daily_blue)
            daily_item_status.image = UIImage.init(named: "blueclock.png")
        }
    }
    
    func isTasksDone(subActivityAraay: NSArray) -> Bool{
        var doneTask: Int = 0
        for i in 0  ..< subActivityAraay.count  {
            var doneStatus: Int = 0
            let subActivity = subActivityAraay.objectAtIndex(i) as! NSDictionary
            if let dataType = subActivity.valueForKey(self.appConstants.datatype_id) as? String {
                let dataType: Int = Int(dataType)!
                if(dataType != 2) {
                    if(appConstants.nullToNil(subActivity.valueForKey(self.appConstants.tasklist_value)) != nil){
                        let tasklist_value: String = subActivity.valueForKey(self.appConstants.tasklist_value) as! String
                        if(!tasklist_value.isEmpty){
                            doneStatus = 1
                        }
                    }
                }
                else if(dataType==2)
                {
                    if(appConstants.nullToNil(subActivity.valueForKey(self.appConstants.tasklist_value)) != nil){
                        let tasklist_value: String = subActivity.valueForKey(self.appConstants.tasklist_value) as! String
                        if(tasklist_value.isEqual("Yes")){
                            doneStatus = 1
                        }
                    }
                }
            }
            if doneStatus == 1 {
                doneTask += 1
            }
        }
    
        if(doneTask == subActivityAraay.count){
            return  true;
        }else{
            return  false;
        }
    }
    
    func isTimeLeft(activityTime: String) -> Int {
        //Returns the differnce between current time and that object's time
        let difference:NSTimeInterval = appConstants.getDailyActivityFormattedDate(activityTime).timeIntervalSinceNow
      
        let ti = NSInteger(difference)
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        if(hours < 0){
            return -1;
        }else if((hours == 0 && minutes > -15) || (hours == 0 && minutes >= 0)) {
            return 1;
        }/*else if (hours >= 0 && min >= 15) {
             return 2;
         }*/else{
            return 0;
        }
    }
}

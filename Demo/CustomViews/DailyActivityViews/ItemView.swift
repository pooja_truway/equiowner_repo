//
//  ItemView.swift
//  Demo
//
//  Created by KOMAL SHARMA on 24/10/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import Foundation

class ItemView: UIView, UIGestureRecognizerDelegate, UITextFieldDelegate, UITextViewDelegate {
    var yy:CGFloat = 0, margin:CGFloat = 0
    var appConstants: AppConstants = AppConstants()
    
    var doneImage: UIImageView = UIImageView()
    var descriptionView: UILabel = UILabel()
    
    var addCommentsData: UITextView!
    
    //1/6 Number/Text format
    var numOrTextView: UITextField = UITextField()
    var dropdownLabel: UILabel = UILabel()
    var activeField: UITextField!
    
    //Amount/Unit/food
    var amountLabel: UITextField = UITextField()
    var unitLabel: UILabel = UILabel()
    //var typeAutoCompleteLabel: UILabel = UILabel()
    var typeAutoCompleteLabel: UITextField = UITextField()
    
    var amountText: UILabel = UILabel()
    var unitText: UILabel = UILabel()
    var typeText: UILabel = UILabel()
    
    //for time format
    let timeData : UITextField = UITextField()
    let starttimedatePicker = UIDatePicker()
    
    var commentView : UIView! = nil
    var modelData: NSMutableDictionary = NSMutableDictionary()
    let comment:UILabel = UILabel()
    var vc = UIApplication.sharedApplication().keyWindow!.rootViewController
    
    var type: Int = 0
    var mCounterChangeCallback: CounterChangeCallback!
    var commentviewtap: UITapGestureRecognizer!
    
    var itemTAG: Int = -1
    var checkBoxImageArray = NSMutableArray()
    
    func drawItems(data: NSDictionary, tag: Int) {
        let bgtap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.addGestureRecognizer(bgtap)
        
        itemTAG = tag
        modelData = data.mutableCopy() as! NSMutableDictionary
        modelData.setValue(false, forKey: self.appConstants.selected)
        
        
        margin = 5 * appConstants.ScreenFactor
        yy = 5 * appConstants.ScreenFactor
        
        let arrow : UIImageView = UIImageView(frame:CGRectMake(6 * margin,yy ,13 * appConstants.ScreenFactor, 20 * appConstants.ScreenFactor))
        let origImage = UIImage(named: "arrow.png");
        arrow.image = origImage
        self.addSubview(arrow)
        
        
        let dailyActivityLayout : UIView = UIView(frame:CGRectMake(arrow.frame.origin.x + arrow.frame.size.width/2,yy,self.frame.size.width - (arrow.frame.origin.x + arrow.frame.size.width) - 2 * appConstants.ScreenFactor, 55 * appConstants.ScreenFactor))
        dailyActivityLayout.userInteractionEnabled = true
        dailyActivityLayout.backgroundColor = UIColor().HexToColor(appConstants.daily_done_bg)
        self.addSubview(dailyActivityLayout)
        
        
        yy += 5
        
        
        
        //**************************  for Description View  ************************** //
        descriptionView.frame = CGRectMake(margin, yy , dailyActivityLayout.frame.size.width - 2*margin, 12 * appConstants.ScreenFactor)
        descriptionView.textColor = UIColor.whiteColor()
        descriptionView.text = modelData.objectForKey(appConstants.tasklist) as? String
        descriptionView.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        descriptionView.backgroundColor = UIColor.clearColor()
        dailyActivityLayout.addSubview(descriptionView)
        //**************************  for Description View  ************************** //
        
        
        yy += descriptionView.frame.size.height + 5
        
        let typeValue: String = modelData.objectForKey(appConstants.datatype_id) as! String
        type = Int(typeValue)!
        
        if type != 2 {
            if type >= 7 {
                addUnitAmountTypeContainer(dailyActivityLayout)
            }else{
                switch type {
                case 1:
                    //**************************  for 1 Text View  ************************** //
                    numOrTextView.frame = CGRectMake(margin, yy , dailyActivityLayout.frame.size.width - 2*margin, 12 * appConstants.ScreenFactor)
                    numOrTextView.textColor = UIColor.whiteColor()
                    numOrTextView.tag = 1
                    numOrTextView.delegate = self
                    numOrTextView.attributedPlaceholder = NSAttributedString(string:"Enter Text",
                                                                             attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
                    numOrTextView.tintColor = UIColor.whiteColor()
                    numOrTextView.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
                    numOrTextView.backgroundColor = UIColor.clearColor()
                    dailyActivityLayout.addSubview(numOrTextView)
                    numOrTextView.keyboardType = .Default
                    yy += numOrTextView.frame.size.height + 5
                    
                    
                    if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_value)) != nil){
                        let value: String = modelData.objectForKey(appConstants.tasklist_value) as! String
                        if !value.isEmpty {
                            numOrTextView.text = value
                        }
                    }
                    
                    break
                    
                case 3:
                    
                    //check box buttons
                    if(appConstants.nullToNil(modelData.objectForKey(appConstants.data)) != nil){
                        let unitsItems = modelData.objectForKey(appConstants.data) as! String
                        let slotsList = unitsItems.characters.split{$0 == ";"}.map(String.init)
                        
                        var arrayTask: String = ""
                        var arrayTaskList: [String] = []
                        
                        if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_value)) != nil){
                            arrayTask = modelData.objectForKey(appConstants.tasklist_value) as! String
                            if !arrayTask.isEmpty {
                                arrayTaskList = arrayTask.characters.split{$0 == ";"}.map(String.init)
                            }
                        }
                        
                        for i in 0  ..< slotsList.count  {
                            let slotValue = slotsList[i] as String
                            var isTicked = false
                            
                            let layout : UIView = UIView()
                            layout.frame = CGRectMake(1.5 * margin, yy ,dailyActivityLayout.frame.size.width - 3*margin, 12 * appConstants.ScreenFactor)
                            layout.userInteractionEnabled = true
                            layout.backgroundColor = UIColor.clearColor()
                            dailyActivityLayout.addSubview(layout)
                            
                            let tickImage : UIImageView = UIImageView()
                            tickImage.frame = CGRectMake(0,1 * appConstants.ScreenFactor,12 * appConstants.ScreenFactor,12 * appConstants.ScreenFactor)
                            tickImage.userInteractionEnabled = true
                            tickImage.tag = i
                            tickImage.image = UIImage.init(imageLiteral: "untick_c.png")
                            layout.addSubview(tickImage)
                            
                            let tickUntick: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tickUntick(_:)))
                            tickImage.addGestureRecognizer(tickUntick)
                            
                            let data : UILabel = UILabel()
                            data.frame = CGRectMake(tickImage.frame.size.width + 5,0,dailyActivityLayout.frame.size.width - (tickImage.frame.size.width + 10),12 * appConstants.ScreenFactor)
                            appConstants.styleLabels(data)
                            data.textColor = UIColor.grayColor()
                            data.text = slotValue
                            layout.addSubview(data)
                            
                            
                            for j in 0  ..< arrayTaskList.count  {
                                let value = arrayTaskList[j] as String
                                if(!value.isEmpty){
                                    if(slotValue.isEqual(value)){
                                        isTicked = true
                                        tickImage.image = UIImage.init(imageLiteral: "tick_c.png")
                                    }
                                }
                            }
                            
                            let json = NSMutableDictionary()
                            //json.setValue(tickImage, forKey: self.appConstants.logo)
                            json.setValue(i, forKey: self.appConstants._id)
                            json.setValue(isTicked, forKey: self.appConstants.selected)
                            json.setValue(slotValue, forKey: self.appConstants.value_type)
                            checkBoxImageArray.addObject(json)
                            
                            yy += layout.frame.size.height + 5
                        }
                        
                    }
                    
                    AppConstants.checkBoxImageArray.replaceObjectAtIndex(itemTAG, withObject: checkBoxImageArray)
                    
                    break
                    
                case 4:
                    //Show dropdown
                    dropdownLabel.frame = CGRectMake(margin, yy , dailyActivityLayout.frame.size.width - 2*margin, 12 * appConstants.ScreenFactor)
                    dropdownLabel.textColor = UIColor.whiteColor()
                    dropdownLabel.text = "Dropdown"
                    dropdownLabel.userInteractionEnabled = true
                    dropdownLabel.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
                    dropdownLabel.backgroundColor = UIColor.clearColor()
                    dailyActivityLayout.addSubview(dropdownLabel)
                    yy += dropdownLabel.frame.size.height + 5
                    
                    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showDropDownList))
                    dropdownLabel.addGestureRecognizer(tap)
                    
                    if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_value)) != nil){
                        let value: String = modelData.objectForKey(appConstants.tasklist_value) as! String
                        if !value.isEmpty {
                            dropdownLabel.text = value
                        }
                    }
                    break
                    
                case 5:
                    //**************************  for Timer  ************************** //
                    let timeLayout : UIView = UIView()
                    timeLayout.frame = CGRectMake(margin, yy ,dailyActivityLayout.frame.size.width - 2*margin, 12 * appConstants.ScreenFactor)
                    timeLayout.backgroundColor = UIColor.clearColor()
                    dailyActivityLayout.addSubview(timeLayout)
                    
                    let timeIcon : UIImageView = UIImageView()
                    timeIcon.frame = CGRectMake(0,1 * appConstants.ScreenFactor,10 * appConstants.ScreenFactor,10 * appConstants.ScreenFactor)
                    timeIcon.image = UIImage.init(imageLiteral: "clock_w.png")
                    timeLayout.addSubview(timeIcon)
                    
                    timeData.frame = CGRectMake(timeIcon.frame.size.width + 5,0,dailyActivityLayout.frame.size.width - (timeIcon.frame.size.width + 10),12 * appConstants.ScreenFactor)
                    timeData.delegate = self
                    timeData.attributedPlaceholder = NSAttributedString(string:"Tap to Add Time",
                                                                        attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
                    
                    if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_value)) != nil){
                        let value: String = modelData.objectForKey(appConstants.tasklist_value) as! String
                        if !value.isEmpty {
                            timeData.text = value
                        }
                    }
                    
                    appConstants.styleInnerLabels(timeData)
                    timeLayout.addSubview(timeData)
                    starttimedatePicker.datePickerMode = UIDatePickerMode.Time
                    timeData.inputView = starttimedatePicker
                    starttimedatePicker.addTarget(self, action: #selector(startTimeDiveChanged(_:)), forControlEvents: .ValueChanged)
                    //**************************  for Timer  ************************** //
                    
                    yy += timeLayout.frame.size.height + 5
                    
                    break
                    
                case 6:
                    //**************************  for Number View  ************************** //
                    numOrTextView.frame = CGRectMake(margin, yy , dailyActivityLayout.frame.size.width - 2*margin, 12 * appConstants.ScreenFactor)
                    numOrTextView.textColor = UIColor.whiteColor()
                    numOrTextView.tag = 1
                    numOrTextView.delegate = self
                    numOrTextView.attributedPlaceholder = NSAttributedString(string:"Times",
                                                                             attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
                    
                    numOrTextView.tintColor = UIColor.whiteColor()
                    numOrTextView.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
                    numOrTextView.backgroundColor = UIColor.clearColor()
                    dailyActivityLayout.addSubview(numOrTextView)
                    
                    numOrTextView.keyboardType = .NumberPad
                    //**************************  for Number View  ************************** //
                    
                    yy += numOrTextView.frame.size.height + 5
                    
                    if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_value)) != nil){
                        let value: String = modelData.objectForKey(appConstants.tasklist_value) as! String
                        if !value.isEmpty {
                            numOrTextView.text = value
                        }
                    }
                    
                    break
                default:
                    break
                }
            }
            
        }
        
        //**************************  For Comment  ************************** //
        let commentLayout : UIView = UIView(frame:CGRectMake(margin,yy,dailyActivityLayout.frame.size.width - 2*margin, 15 * appConstants.ScreenFactor))
        commentLayout.userInteractionEnabled = true
        commentLayout.backgroundColor = UIColor.clearColor()
        dailyActivityLayout.addSubview(commentLayout)
        
        let commentIcon : UIImageView = UIImageView()
        commentIcon.frame = CGRectMake(0,0,12 * appConstants.ScreenFactor,12 * appConstants.ScreenFactor)
        commentIcon.image = UIImage.init(imageLiteral: "comment.png")
        commentLayout.addSubview(commentIcon)
        
        
        comment.frame = CGRectMake(15 * appConstants.ScreenFactor, 0 , commentLayout.frame.size.width - 16 * appConstants.ScreenFactor, 12 * appConstants.ScreenFactor)
        comment.textColor = UIColor.whiteColor()
        comment.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        comment.backgroundColor = UIColor.clearColor()
        comment.text = "Tap to Add Comment"
        
        if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_comment)) != nil){
            let value: String = modelData.objectForKey(appConstants.tasklist_comment) as! String
            if !value.isEmpty {
                comment.text = value
            }
        }
        
        let addCommentRecognizer = UITapGestureRecognizer(target: self, action: #selector(addComment(_:)))
        commentLayout.addGestureRecognizer(addCommentRecognizer)
        commentLayout.addSubview(comment)
        //**************************  For Comment  ************************** //
        
        dailyActivityLayout.frame.size.height = commentLayout.frame.origin.y + commentLayout.frame.size.height
        self.frame.size.height = dailyActivityLayout.frame.size.height + 15 * appConstants.ScreenFactor
        
        //Add in the end
        let vline1: UIView = UIView(frame : CGRectMake(3 * margin , 0 , 2, self.frame.size.height))
        vline1.backgroundColor = UIColor().HexToColor(appConstants.daily_not_done_bg)
        self.addSubview(vline1)
        
        doneImage.frame = CGRectMake(3 * margin - 5 * appConstants.ScreenFactor ,dailyActivityLayout.frame.origin.y + 5 * appConstants.ScreenFactor ,12 * appConstants.ScreenFactor, 12 * appConstants.ScreenFactor)
        doneImage.image = UIImage.init(named: "cross.png")
        doneImage.userInteractionEnabled = true
        self.addSubview(doneImage)
        
        let doneRecognizer = UITapGestureRecognizer(target: self, action: #selector(done(_:)))
        doneImage.addGestureRecognizer(doneRecognizer)
        
        findDoneStatus()
        updateModel()
    }
    
    func addUnitAmountTypeContainer(dailyActivityLayout : UIView) {
        
        let unitAmountTypeContainer =  UIView.init(frame: CGRectMake(margin, yy, dailyActivityLayout.frame.size.width - 2*margin, 39 * appConstants.ScreenFactor))
        unitAmountTypeContainer.userInteractionEnabled = true
        unitAmountTypeContainer.backgroundColor = UIColor.clearColor()
        dailyActivityLayout.addSubview(unitAmountTypeContainer)
        yy += unitAmountTypeContainer.frame.size.height + 5
        
        
        typeText.frame = CGRectMake(0,0,unitAmountTypeContainer.frame.size.width/2 - 2,12 * appConstants.ScreenFactor)
        typeText.text = "Type"
        typeText.textAlignment = NSTextAlignment.Left
        appConstants.styleWhiteColorLabels(typeText)
        unitAmountTypeContainer.addSubview(typeText)
        
        
        typeAutoCompleteLabel.frame = CGRectMake(unitAmountTypeContainer.frame.size.width/2, 0 , unitAmountTypeContainer.frame.size.width/2, 12 * appConstants.ScreenFactor)
        /*typeAutoCompleteLabel.textColor = UIColor.whiteColor()
         typeAutoCompleteLabel.tag = 101
         typeAutoCompleteLabel.textAlignment = NSTextAlignment.Left
         typeAutoCompleteLabel.text = "Tap to Select"
         typeAutoCompleteLabel.tintColor = UIColor.whiteColor()
         typeAutoCompleteLabel.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
         typeAutoCompleteLabel.backgroundColor = UIColor.clearColor()
         typeAutoCompleteLabel.userInteractionEnabled = true
         let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showTypeDropDownList))
         typeAutoCompleteLabel.addGestureRecognizer(tap)
         unitAmountTypeContainer.addSubview(typeAutoCompleteLabel)*/
        
        
        typeAutoCompleteLabel.textColor = UIColor.whiteColor()
        typeAutoCompleteLabel.tag = 101
        typeAutoCompleteLabel.delegate = self
        typeAutoCompleteLabel.textAlignment = NSTextAlignment.Left
        typeAutoCompleteLabel.attributedPlaceholder = NSAttributedString(string:"Type",
                                                                         attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        typeAutoCompleteLabel.tintColor = UIColor.whiteColor()
        typeAutoCompleteLabel.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        typeAutoCompleteLabel.backgroundColor = UIColor.clearColor()
        
        
        if(appConstants.nullToNil(modelData.objectForKey(appConstants.value_type)) != nil){
            let value: String = modelData.objectForKey(appConstants.value_type) as! String
            if !value.isEmpty {
                typeAutoCompleteLabel.text = value
            }
        }
        
        
        unitAmountTypeContainer.addSubview(typeAutoCompleteLabel)
        
        unitText.frame = CGRectMake(0,13 * appConstants.ScreenFactor,unitAmountTypeContainer.frame.size.width/2 - 2,12 * appConstants.ScreenFactor)
        unitText.text = "Unit"
        unitText.textAlignment = NSTextAlignment.Left
        appConstants.styleWhiteColorLabels(unitText)
        unitAmountTypeContainer.addSubview(unitText)
        
        
        unitLabel.frame = CGRectMake(unitAmountTypeContainer.frame.size.width/2, unitText.frame.origin.y , unitAmountTypeContainer.frame.size.width/2, 12 * appConstants.ScreenFactor)
        unitLabel.textColor = UIColor.whiteColor()
        unitLabel.textAlignment = NSTextAlignment.Left
        unitLabel.tintColor = UIColor.whiteColor()
        unitLabel.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        unitLabel.backgroundColor = UIColor.clearColor()
        unitAmountTypeContainer.addSubview(unitLabel)
        
        
        if(appConstants.nullToNil(modelData.objectForKey(appConstants.datatype)) != nil){
            let value: String = modelData.objectForKey(appConstants.datatype) as! String
            if !value.isEmpty {
                unitLabel.text = value
            }
        }

        amountText.frame = CGRectMake(0,24 * appConstants.ScreenFactor,unitAmountTypeContainer.frame.size.width/2 - 2,12 * appConstants.ScreenFactor)
        amountText.text = "Amount"
        amountText.textAlignment = NSTextAlignment.Left
        appConstants.styleWhiteColorLabels(amountText)
        unitAmountTypeContainer.addSubview(amountText)
        
        
        amountLabel.frame = CGRectMake(unitAmountTypeContainer.frame.size.width/2, amountText.frame.origin.y , unitAmountTypeContainer.frame.size.width/2, 12 * appConstants.ScreenFactor)
        amountLabel.textColor = UIColor.whiteColor()
        amountLabel.tag = 102
        amountLabel.delegate = self
        amountLabel.textAlignment = NSTextAlignment.Left
        amountLabel.keyboardType = .DecimalPad
        amountLabel.attributedPlaceholder = NSAttributedString(string:"Amount",
                                                               attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        amountLabel.tintColor = UIColor.whiteColor()
        amountLabel.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        amountLabel.backgroundColor = UIColor.clearColor()
        unitAmountTypeContainer.addSubview(amountLabel)
        
        
        if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_value)) != nil){
            let value: String = modelData.objectForKey(appConstants.tasklist_value) as! String
            if !value.isEmpty {
                amountLabel.text = value
            }
        }else{
            amountLabel.text = ""
        }
    }
    
    
    func findDoneStatus() {
        if(type != 2) {
            if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_value)) != nil){
                let tasklist_value: String = modelData.objectForKey(appConstants.tasklist_value) as! String
                if(!tasklist_value.isEmpty){
                    modelData.setValue(true, forKey: self.appConstants.selected)
                }
                
            }
        }
        else if(type==2)
        {
            if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_value)) != nil){
                let tasklist_value: String = modelData.objectForKey(appConstants.tasklist_value) as! String
                if(tasklist_value.isEqual("Yes")){
                    modelData.setValue(true, forKey: self.appConstants.selected)
                }
            }
        }
        
        let done: Bool = modelData.objectForKey(appConstants.selected) as! Bool
        
        if !done {
            doneImage.image = UIImage.init(named: "cross.png")
        } else{
            doneImage.image = UIImage.init(named: "tick.png")
            mCounterChangeCallback.countValue(true)
        }
    }
    
    func dismissKeyboard() {
        starttimedatePicker.resignFirstResponder()
        starttimedatePicker.endEditing(true)
        
        if(activeField != nil){
            activeField.resignFirstResponder()
        }
        self.endEditing(true)
    }
    
    func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_GB") // using Great Britain for 24 hr format
        formatter.timeStyle = .ShortStyle
        timeData.text = appConstants.hour24To12HourTimeConverter(formatter.stringFromDate(sender.date))
        modelData.setObject(timeData.text!, forKey: appConstants.tasklist_value)
        updateModel()
    }
    
    func tickUntick(gestureRecognizer: UITapGestureRecognizer){
        let viewTouched: UIImageView = gestureRecognizer.view! as! UIImageView
        
        let json: NSMutableDictionary = checkBoxImageArray.objectAtIndex(viewTouched.tag) as! NSMutableDictionary
        let isTicked: Bool = json.objectForKey(self.appConstants.selected) as! Bool
        if(isTicked){
            //mark it unticked
            json.setObject(false, forKey: self.appConstants.selected)
            viewTouched.image = UIImage.init(imageLiteral: "untick_c.png")
        }else{
            //mark it ticked
            json.setObject(true, forKey: self.appConstants.selected)
            viewTouched.image = UIImage.init(imageLiteral: "tick_c.png")
        }
        checkBoxImageArray.replaceObjectAtIndex(viewTouched.tag, withObject: json)
    }
    
    func done(gestureRecognizer: UITapGestureRecognizer) {
        
        if type != 2 {
            if type >= 7 {
                if !amountLabel.text!.isEmpty &&
                    !typeAutoCompleteLabel.text!.isEmpty &&
                    !typeAutoCompleteLabel.text!.isEqual("Tap to Select")  {
                    changeTickValue()
                    modelData.setObject(amountLabel.text!, forKey: appConstants.tasklist_value)
                    modelData.setObject(typeAutoCompleteLabel.text!, forKey: appConstants.value_type)
                    updateModel()
                }
            }else{
                switch type {
                case 1:
                    if !numOrTextView.text!.isEmpty {
                        changeTickValue()
                    }
                    
                    break
                    
                case 3:
                    changeTickValue()
                    break
                    
                case 4:
                    if !dropdownLabel.text!.isEmpty {
                        changeTickValue()
                    }
                    break
                    
                case 5:
                    if !timeData.text!.isEmpty {
                        changeTickValue()
                    }
                    break
                    
                case 6:
                    if !numOrTextView.text!.isEmpty {
                        changeTickValue()
                    }
                    break
                default:
                    break
                }
            }
            
        }else{
            //type==2
            changeTickValue()
        }
    }
    
    func changeTickValue() {
        let done: Bool = modelData.objectForKey(appConstants.selected) as! Bool
        if !done {
            doneImage.image = UIImage.init(named: "tick.png")
            mCounterChangeCallback.countValue(true)
        } else{
            doneImage.image = UIImage.init(named: "cross.png")
            mCounterChangeCallback.countValue(false)
        }
        
        modelData.setObject(!done, forKey: appConstants.selected)
        updateModel()
    }
    
    func showDropDownList() {
        let unitsItems = modelData.objectForKey(appConstants.data) as! String
        
        let alertController = UIAlertController(title: "Select", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let slotsList = unitsItems.characters.split{$0 == ";"}.map(String.init)
        for i in 0  ..< slotsList.count  {
            let slotValue = slotsList[i] as String
            let oneAction = UIAlertAction(title: String(slotValue), style: .Default, handler: { (action: UIAlertAction!) in
                self.dropdownLabel.text = slotValue
            })
            alertController.addAction(oneAction)
        }
        let Cancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(Cancel)
        vc!.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showTypeDropDownList() {
        
        let alertController = UIAlertController(title: "Select", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let slotsList = appConstants.food_types
        for i in 0  ..< slotsList.count  {
            let slotValue = slotsList[i] as String
            let oneAction = UIAlertAction(title: String(slotValue), style: .Default, handler: { (action: UIAlertAction!) in
                self.typeAutoCompleteLabel.text = slotValue
                self.modelData.setObject(self.typeAutoCompleteLabel.text!, forKey: self.appConstants.value_type)
                self.updateModel()
            })
            alertController.addAction(oneAction)
        }
        let Cancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(Cancel)
        vc!.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func addComment(gestureRecognizer: UITapGestureRecognizer) {
        dismissKeyboard()
        addDescriptionView()
    }
    
    
    func removeComment() {
        mCounterChangeCallback.bringScrollTofront()
        dismissKeyboard()
        if(commentView != nil){
            
            addCommentsData.resignFirstResponder()
            if(commentviewtap != nil){
                commentView.removeGestureRecognizer(commentviewtap)
            }
            commentView.removeFromSuperview()
            commentView = nil
        }
    }
    
    func addDescriptionView(){
        mCounterChangeCallback.bringScrollTofront()
        dismissKeyboard()
        
        if(commentView != nil){
            addCommentsData.resignFirstResponder()
            if(commentviewtap != nil){
                commentView.removeGestureRecognizer(commentviewtap)
            }
            commentView.removeFromSuperview()
            commentView = nil
        }
        
        commentView = UIView.init(frame: CGRectMake(0, 0, appConstants.screenWidth, appConstants.screenHeight))
        commentView.userInteractionEnabled = true
        commentView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        vc!.view.addSubview(commentView)
        
        let mainLayout : UIView = UIView(frame:CGRectMake(margin,yy,appConstants.screenWidth - 2*margin, 90 * appConstants.ScreenFactor))
        mainLayout.center = CGPointMake(appConstants.screenWidth/2, appConstants.screenHeight/2)
        mainLayout.backgroundColor = UIColor().HexToColor(appConstants.font_white_color)
        commentView.addSubview(mainLayout)
        
        var y: CGFloat = 0
        
        let heading:UILabel = UILabel()
        heading.frame = CGRectMake(0, y , mainLayout.frame.size.width, 20 * appConstants.ScreenFactor)
        heading.textColor = UIColor.whiteColor()
        heading.textAlignment = .Center
        heading.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        heading.backgroundColor = UIColor().HexToColor(appConstants.accent_material_light)
        heading.text = "Description"
        mainLayout.addSubview(heading)
        
        y += heading.frame.size.height
        
        let title:UILabel = UILabel()
        title.frame = CGRectMake(margin, y , mainLayout.frame.size.width - 2*margin, 15 * appConstants.ScreenFactor)
        title.textColor = UIColor.blackColor()
        title.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        title.backgroundColor = UIColor.clearColor()
        title.text = modelData.objectForKey(appConstants.tasklist) as? String
        mainLayout.addSubview(title)
        
        y += title.frame.size.height
        
        //Add in the end
        let hline1: UIView = UIView(frame : CGRectMake(margin , y , mainLayout.frame.size.width - 2 * margin, 1))
        hline1.backgroundColor = UIColor().HexToColor(appConstants.bg_light_grey)
        mainLayout.addSubview(hline1)
        
        y += hline1.frame.size.height
        
        let addNotesHeading:UILabel = UILabel()
        addNotesHeading.frame = CGRectMake(margin, y , mainLayout.frame.size.width - 2*margin, 15 * appConstants.ScreenFactor)
        addNotesHeading.textColor = UIColor().HexToColor(appConstants.accent_material_light)
        addNotesHeading.font = UIFont.init(name: "Helvetica", size: 8 * appConstants.ScreenFactor)
        addNotesHeading.backgroundColor = UIColor.clearColor()
        addNotesHeading.text = "ADD NOTES"
        mainLayout.addSubview(addNotesHeading)
        
        y += addNotesHeading.frame.size.height
        
        addCommentsData = UITextView()
        addCommentsData.frame = CGRectMake(margin, y , mainLayout.frame.size.width - 2*margin, 30 * appConstants.ScreenFactor)
        addCommentsData.textColor = UIColor.blackColor()
        addCommentsData.tag = 100
        addCommentsData.delegate = self
        
        
        if(appConstants.nullToNil(modelData.objectForKey(appConstants.tasklist_comment)) != nil){
            addCommentsData.text = modelData.objectForKey(appConstants.tasklist_comment) as! String
        }
        
        
        addCommentsData.layer.cornerRadius = 5
        addCommentsData.layer.borderColor = UIColor().HexToColor(appConstants.bg_light_grey).CGColor
        addCommentsData.layer.borderWidth = 1
        
        addCommentsData.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        addCommentsData.backgroundColor = UIColor.clearColor()
        mainLayout.addSubview(addCommentsData)
        
        commentviewtap = UITapGestureRecognizer(target: self, action: #selector(removeComment))
        commentView.addGestureRecognizer(commentviewtap)
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeField = textField
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        dismissKeyboard()
        //self.userInteractionEnabled = true
        mCounterChangeCallback.bringScrollTofront()
        return true;
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 1 {
            //numOrTextview: change tasklist_value
            if !textField.text!.isEmpty {
                modelData.setValue(numOrTextView.text! + string, forKey: appConstants.tasklist_value)
                updateModel()
            }
        }else if textField.tag == 101 {
            if !textField.text!.isEmpty {
                self.modelData.setObject(self.typeAutoCompleteLabel.text! + string, forKey: self.appConstants.value_type)
                self.updateModel()
            }
        }
        else if textField.tag == 102 {
            if !textField.text!.isEmpty {
                self.modelData.setObject(self.amountLabel.text! + string, forKey: self.appConstants.tasklist_value)
                self.updateModel()
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("end edit called")
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.tag == 100 {
            //comment
            if !textView.text.isEmpty {
                comment.text = textView.text
                modelData.setValue(comment.text, forKey: appConstants.tasklist_comment)
                updateModel()
            }
        }
    }
    
    func updateModelView() throws {
        let subActivityList11: NSArray = AppConstants.dataModelForDetailEditing.objectForKey(self.appConstants.tasklist) as! NSArray
        let subActivityList: NSMutableArray = subActivityList11.mutableCopy() as! NSMutableArray
        subActivityList.replaceObjectAtIndex(itemTAG, withObject: modelData)
        AppConstants.dataModelForDetailEditing.setObject(subActivityList, forKey: self.appConstants.tasklist)
    }
    
    func updateModel() {
        do {
            try updateModelView()
        } catch {
        }
    }
}

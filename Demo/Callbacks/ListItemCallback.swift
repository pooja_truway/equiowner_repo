//
//  ListItemCallback.swift
//  Demo
//
//  Created by KOMAL SHARMA on 14/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation

@objc protocol ListItemCallback {
    func itemSelected(model: NSDictionary) -> Void
}
//
//  DropDownSelectionCallback.swift
//  Demo
//
//  Created by KOMAL SHARMA on 21/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation
@objc protocol MenuSelectionCallback {
    func menuSelected(selectedItem: Int) -> Void
}
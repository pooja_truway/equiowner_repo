//
//  MenuSelectionCallback.swift
//  Demo
//
//  Created by KOMAL SHARMA on 06/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation

@objc protocol DropDownSelectionCallback {
    func itemSelectedFromDropDown(selectedItem: Int, tag: Int) -> Void
}

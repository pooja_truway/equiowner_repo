//
//  SubActivityRemovedCallback.swift
//  Demo
//
//  Created by KOMAL SHARMA on 26/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation

@objc protocol SubActivityRemovedCallback {
    func itemRemoved(index: NSIndexPath) -> Void
}
//
//  CounterChangeCallback.swift
//  Demo
//
//  Created by KOMAL SHARMA on 25/10/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import Foundation
@objc protocol CounterChangeCallback {
    func countValue(toAdd: Bool) -> Void
    func bringScrollTofront() -> Void

}

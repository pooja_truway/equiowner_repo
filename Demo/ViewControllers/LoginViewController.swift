//
//  LoginViewController.swift
//  Demo
//
//  Created by KOMAL SHARMA on 04/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
//import Quickblox

class LoginViewController: UIViewController,UITextFieldDelegate {
    var yy:CGFloat = 0, margin:CGFloat = 0
    
    let messageWebService  = MessageSycWebService()
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
   // var notificationToken = Notifications()
    var activityIndicator:UIActivityIndicatorView!
    
    var scrollView:UIScrollView?
    var backgroundView:UIImageView?
    var horseLogoWithBackground:UIImageView?
    var loginButton:UIButton?
    var passwordContainer: LoginInputViews?
    var userNameContainer: LoginInputViews?
    var activeView: UIView?
    var usernameText: UITextField = UITextField()
    var passwordText: UITextField = UITextField()
    var paddingView = UIView(frame:CGRectMake(0, 0, 30, 30))
   // var  contactChatDict : NSMutableDictionary = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        addNotifications()
        initializeVariables()
        drawUI()
        self.hideKeyboardWhenTappedAround()
    }
    
    
    func getNotificationCount(userId: String)
    {
            let postString = "action=mynotifications&userId="+userId;
            
            webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                        if error != nil{
                        return
                    }
                    
                    print("response = \(response)")
                    let data = self.appConstants.convertStringToDictionary(response)
                    if data != nil {
                        if ((data?[self.appConstants.ErrorCode]) != nil) {
                            //Error
                            return
                        }else{
                            //User Data
                            let mainList:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                            print("mainList = \(mainList)")
                            let totalNumber : NSString = mainList .valueForKey("total") as! NSString
                           print(totalNumber)
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setObject(totalNumber, forKey: "notificationBadge")
                             defaults.synchronize()
                            
                            
                            dispatch_async(dispatch_get_main_queue()) {
                                self.hideProgress()
                                UIApplication.sharedApplication().keyWindow!.rootViewController = MainViewController()
                            }
                            
                        }
                    }
                }
            }
        }
        
    func addNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name:UIKeyboardWillHideNotification, object: nil);
    }
    
    func keyboardWillShow(sender: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        scrollView!.scrollEnabled = true
        let info : NSDictionary = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        scrollView!.contentInset = contentInsets
        scrollView!.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeView != nil {
            if (!CGRectContainsPoint(aRect, activeView!.frame.origin))
            {
               scrollView!.scrollRectToVisible(activeView!.frame, animated: true)
            }
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let info : NSDictionary = sender.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        scrollView!.contentInset = UIEdgeInsetsZero
        scrollView!.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        scrollView!.scrollEnabled = false
    }
    
    func initializeVariables() {
        yy = 40 * appConstants.ScreenFactor
        margin = 20 * appConstants.ScreenFactor
    }
    
    func drawUI() {
        scrollView = UIScrollView.init(frame: self.view.frame)
        scrollView!.contentSize = self.view.frame.size
        scrollView!.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        
        self.view.addSubview(scrollView!)
        
        backgroundView = UIImageView.init(frame: scrollView!.frame)
        backgroundView!.image = UIImage.init(imageLiteral: "Signup.png")
        scrollView!.addSubview(backgroundView!)
        
//        horseLogoWithBackground = UIImageView.init(frame: CGRectMake(appConstants.screenWidth * 0.3,yy,appConstants.screenWidth * 0.7 ,appConstants.screenHeight * 0.43))
//        horseLogoWithBackground!.image = UIImage.init(imageLiteral: "logoshade.png")
//        scrollView!.addSubview(horseLogoWithBackground!)
        
        loginButton = UIButton.init(frame: CGRectMake(margin,appConstants.screenHeight - 70 * appConstants.ScreenFactor,appConstants.screenWidth - 2*margin,30 * appConstants.ScreenFactor))
        loginButton?.layer.cornerRadius = appConstants.CornerRadius
        loginButton?.setTitle("LET'S START", forState: UIControlState.Normal)
        loginButton?.titleLabel?.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        loginButton?.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        loginButton?.backgroundColor = UIColor().HexToColor(appConstants.button_color, alpha: 1.0)
        loginButton!.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.loginUser(_:)))
        loginButton!.addGestureRecognizer(tapRecognizer)
        scrollView!.addSubview(loginButton!)
        
        
        

        usernameText.frame = CGRectMake(margin,appConstants.screenHeight - 160 * appConstants.ScreenFactor,appConstants.screenWidth - 2*margin,30 * appConstants.ScreenFactor)
        usernameText.textColor = UIColor.blackColor()
        usernameText.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        usernameText.attributedPlaceholder = NSAttributedString(string:"Username",
                                                             attributes:[NSForegroundColorAttributeName: UIColor.darkGrayColor()])
        usernameText.keyboardType = UIKeyboardType.EmailAddress

       // usernameText.leftView=paddingView;

        usernameText.backgroundColor = UIColor.whiteColor()
        let paddingForFirst = UIView(frame: CGRectMake(0, 0, 15, self.usernameText.frame.size.height))
        usernameText.leftView = paddingForFirst
        usernameText.leftViewMode = UITextFieldViewMode .Always
        scrollView!.addSubview(usernameText)
        
        
        passwordText.frame = CGRectMake(margin,loginButton!.frame.origin.y - 45 * appConstants.ScreenFactor,appConstants.screenWidth - 2*margin,30 * appConstants.ScreenFactor)
        passwordText.textColor = UIColor.blackColor()
        passwordText.font = UIFont.init(name: "Helvetica", size: 10 * appConstants.ScreenFactor)
        passwordText.attributedPlaceholder = NSAttributedString(string:"Password",
                                                             attributes:[NSForegroundColorAttributeName: UIColor.darkGrayColor()])
        
        let paddingForSecond = UIView(frame: CGRectMake(0, 0, 15, self.passwordText.frame.size.height))
        passwordText.leftView = paddingForSecond
        passwordText.leftViewMode = UITextFieldViewMode .Always
        passwordText.secureTextEntry = true
        passwordText.backgroundColor = UIColor.whiteColor()
        scrollView!.addSubview(passwordText)

        
        
        
//        passwordContainer = LoginInputViews(frame: CGRectMake(margin,loginButton!.frame.origin.y - 50 * appConstants.ScreenFactor,appConstants.screenWidth - 2*margin,30 * appConstants.ScreenFactor))
//        passwordContainer!.addView(2)
//        passwordContainer?.entryView.delegate = self
//        scrollView!.addSubview(passwordContainer!)
//        
//        userNameContainer = LoginInputViews(frame: CGRectMake(margin,passwordContainer!.frame.origin.y - 40 * appConstants.ScreenFactor ,appConstants.screenWidth - 2*margin,30 * appConstants.ScreenFactor))
//        userNameContainer!.addView(1)
//        userNameContainer?.entryView.delegate = self
//        scrollView!.addSubview(userNameContainer!)

    }
    
    func loginUser(gestureRecognizer: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
        
        //Validate
        if (usernameText.text!.isEmpty) {
            appConstants.showAlert("Whoa!", message: "You missed the Username.",controller: self)
            return
        }else if (passwordText.text!.isEmpty) {
            appConstants.showAlert("Whoa!", message: "You missed the Password.",controller: self)
            return
        }else{
            
            showProgress()
            
            //send to server
            let postString = "action=login&password="+passwordText.text!+"&loginemailID="+usernameText.text!;
            
            webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
                dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                    if error != nil{
                        self.hideProgress()
                        self.appConstants.showAlert("Whoa!", message: "Something's not correct. Please try again.",controller: self)
                        return
                    }
                    print("response = \(response)")
                    SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.LOGIN_DATA, value: response as String)
                    
                    let data = self.appConstants.convertStringToDictionary(response)
                    if data != nil {
                        if ((data?[self.appConstants.ErrorCode]) != nil) {
                            //Error
                            self.hideProgress()
                            self.appConstants.showAlert("Whoa!", message: (data?[self.appConstants.Message])! as! String,controller: self)
                            return
                        }else{
                            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.token, value: (data?[self.appConstants.token])! as! String)
                            
                            //User Data
                            
                            let userData:NSDictionary = (data?[self.appConstants.data])! as! NSDictionary
                            let notificationDict = (userData[self.appConstants.notifications])! as! NSDictionary
                            print("notificationDict = \(notificationDict)")
                            
                            if let count = notificationDict.valueForKey(self.appConstants.horse_count) as? NSNumber {
                                SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.horse_count, value: "\(count)")
                            }
                            
                            if ((userData.valueForKey(self.appConstants.user_type) as? String == "3" ) || (userData.valueForKey(self.appConstants.user_type) as? String == "5")){
                                SharedPreferenceUtils._sharedInstance.saveBoolValue(self.appConstants.ISOWNER, value: false)
                            }else {
                                SharedPreferenceUtils._sharedInstance.saveBoolValue(self.appConstants.ISOWNER, value: true)
                            }
                            
                            
                            SharedPreferenceUtils.sharedInstance.saveStringValue(self.appConstants.token , value: data?[self.appConstants.token] as! String)
                            SharedPreferenceUtils.sharedInstance.saveStringValue(self.appConstants.token , value: data?[self.appConstants.token] as! String)
                            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.name, value: userData.valueForKey(self.appConstants.name) as! String)
                            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.memberID, value: userData.valueForKey(self.appConstants.memberID) as! String)
                            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.emailID, value: userData.valueForKey(self.appConstants.emailID) as! String)
                            
                            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.user_type, value: userData.valueForKey(self.appConstants.user_type) as! String)

                            
                            SharedPreferenceUtils._sharedInstance.saveBoolValue(self.appConstants.IS_LOGIN, value: true)
                          //  self.getNotificationCount("70") //pooja
                            
                            //todo:shailendra get user id from sharedprefrence and send to server to get notification count and notification
                            //
                            //let userId:String = userData.valueForKey(self.appConstants.memberID) as! String
                            self.getNotificationCount(userData.valueForKey(self.appConstants.memberID) as! String)
                            
                            //   self.contactChatDict = userData.objectForKey(self.appConstants.chat) as! NSMutableDictionary
                            
                        
                            
                    /*        dispatch_async(dispatch_get_main_queue()) {
                                self.hideProgress()
                                UIApplication.sharedApplication().keyWindow!.rootViewController = MainViewController()
                            }*/ //pooja to include badge
                            
                            
                            
                            /*let userIdForChat: NSNumber! = contactChatDict.valueForKey(self.appConstants.current_id_chat) as! NSNumber
                             //                            let userIdForChatInt: Int! = (Int(userIdForChat))!
                             let userIdForChatFinal: String! = String(userIdForChat)
                             let passwordChat: String! = self.appConstants.passwordChat
                             
                             let user = QBUUser()
                             user.login = userIdForChatFinal
                             user.password = passwordChat
                             user.email=userIdForChatFinal+"@gmail.com"
                             
                             
                             if(!SharedPreferenceUtils.sharedInstance.getBoolValue(self.appConstants.CHAT_USER_REGISTERED))
                             {
                             QBRequest.signUp(user, successBlock: { (response: QBResponse?, userResponse : QBUUser??) -> Void in
                             
                             print("response = \( userResponse??.ID)")
                             SharedPreferenceUtils.sharedInstance.saveBoolValue(self.appConstants.CHAT_USER_REGISTERED, value: true)
                             SharedPreferenceUtils.sharedInstance.saveStringValue(self.appConstants.CHAT_USER_ID, value: userIdForChatFinal+"@gmail.com")
                             SharedPreferenceUtils.sharedInstance.saveStringValue(self.appConstants.CHAT_CURRENT_ID, value: userIdForChatFinal)
                             SharedPreferenceUtils.sharedInstance.saveStringValue(self.appConstants.CHAT_USER_PASSWORD, value: passwordChat)
                             SharedPreferenceUtils.sharedInstance.saveIntValue(self.appConstants.CHAT_USER_ID_SERVER, value: Int((userResponse??.ID)!))
                             
                             let userId:String = userData.valueForKey(self.appConstants.memberID) as! String
                             let userChatId : String = String(Int((userResponse??.ID)!))
                             
                             let postStringChat = "action=initiatchat&userId="+userId+"&userName="+userChatId+"&userPassword="+passwordChat
                             
                             self.webService.HTTPPostJSON(self.appConstants.LOGIN, data:postStringChat, callback: { (response, error) in
                             dispatch_async(dispatch_get_main_queue()) {
                             self.hideProgress()
                             UIApplication.sharedApplication().keyWindow!.rootViewController = MainViewController()
                             }
                             })
                             })
                             { (responce : QBResponse!) -> Void in
                             self.hideProgress()
                             }
                             
                             }else{
                             self.hideProgress()
                             }*/
                        }
                    }
                }
            }
        }
    }

    //TextField Delegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        // called when 'return' key pressed. return NO to ignore.
        self.view.endEditing(true)
        return true;
    }
    
    func textFieldDidBeginEditing(textField: UITextField){
        if textField.tag == 1 {
            activeView = userNameContainer
        }else{
            activeView = passwordContainer
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField){
        activeView = nil
    }
      
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self);
    }
    
    func showProgress() {
        if(activityIndicator != nil){
            activityIndicator.removeFromSuperview()
            activityIndicator = nil
        }
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        activityIndicator = UIActivityIndicatorView(frame: self.view.bounds)
        activityIndicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        activityIndicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        activityIndicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.view.addSubview(activityIndicator)
        activityIndicator.userInteractionEnabled = false
        activityIndicator.startAnimating()
    }
    
    
    func hideProgress() {
        if(activityIndicator != nil){
            self.activityIndicator.removeFromSuperview()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            activityIndicator = nil
        }
    }
}

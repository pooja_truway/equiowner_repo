//
//  MainViewController.swift
//  Demo
//
//  Created by KOMAL SHARMA on 04/08/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
//import Quickblox


class MainViewController: UIViewController, MenuSelectionCallback {
    var yy:CGFloat = 0, margin:CGFloat = 0
    
    var appConstants: AppConstants = AppConstants()
    var webService: WebService = WebService()
    var activityIndicator:UIActivityIndicatorView!
    
    var backgroundView:UIImageView?
    var headerContainer: HeaderView?
    var sideBarContainer: SideNavigationBarView?
    
    var hline: UIView = UIView()
    var vline: UIView = UIView()
    var backButton:UILabel?
    
    var containerView: UIView = UIView()
    let messageWebService: MessageSycWebService = MessageSycWebService()
    
    private let notificationManager = NotificationManager()
    var  timer = NSTimer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.view.backgroundColor = UIColor.whiteColor()
        self.view.backgroundColor = UIColor.clearColor()
        initializeVariables()
        drawUI()
        
        
        
        // if !SharedPreferenceUtils._sharedInstance.getStringValue(appConstants.GCM_TOKEN).isEmpty
        //  {
    //   registerUser() //pooja removed on 3rd july to remove the crash on app starting - this url was not used in code
        
        // }
        // chatLogin()
    }
    
    func registerUser()
    {
        let json = NSMutableDictionary()
        json.setValue(SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.emailID), forKey: "userName")
        json.setValue(SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID), forKey: "userId")
        
        json.setValue("IOS", forKey: "os")
        json.setValue("GCM DUMMY FOR IOS", forKey: "gcmId")
        
        let data = self.appConstants.convertDictionaryToString(json)
        print("data = \(data)")
        
        messageWebService.HTTPPostJSON(appConstants.REGISTER_USER_URL, data: data!) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                if error != nil{
                    return
                }
                print("response123 :  \(response)")
                let data = self.appConstants.convertStringToDictionary(response)! as NSDictionary
             
                print("response = \(response)")
                print("response = \(data)")
                
                
                let messageServerUserId = data.objectForKey("messageServerUserId")
                let stringMessage: String = messageServerUserId as! String
                
                
                SharedPreferenceUtils.sharedInstance.saveBoolValue(self.appConstants.CHAT_USER_REGISTERED, value: true)
                SharedPreferenceUtils.sharedInstance.saveStringValue(self.appConstants.CHAT_USER_ID, value: stringMessage)
                SharedPreferenceUtils.sharedInstance.saveStringValue(self.appConstants.CHAT_CURRENT_ID, value: stringMessage)
                
                
                let postStringChat = "action=initiatchat&userId="+SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.memberID)+"&userName="+stringMessage+"&userPassword="+self.appConstants.passwordChat+"&userSecreteKey="+stringMessage
                
                self.webService.HTTPPostJSON(self.appConstants.INITIATE_CHAT, data:postStringChat, callback: { (response, error) in
                    
                })
            }
        }
    }
    
    func initializeVariables() {
        yy = 2 * appConstants.ScreenFactor
        margin = 20 * appConstants.ScreenFactor
        notificationManager.registerObserver(appConstants.kN_UpdateProfilePicNotification) { note in
            self.showUserCredentials()
        }
        
        notificationManager.registerObserver(appConstants.kN_StartTimerNotification) { note in
            self.startTimer()
        }
        
        
        notificationManager.registerObserver(appConstants.kN_ShowHorseCredentailsInHeaderNotification) { note in
            let dict = note.object as! NSDictionary
            let imageURL:String = dict[self.appConstants.profice_pic] as! String
            
            if imageURL.isEmpty {
                self.headerContainer?.image.image = UIImage.init(imageLiteral: "newProfilePic.png")
            }else{
                self.headerContainer?.image.downloadedFrom(imageURL)
            }
            
            self.headerContainer?.name.text = dict[self.appConstants.name] as? String
        }
        
        
        
    }
    
    func gotoNotification(gestureRecognizer: UITapGestureRecognizer)
    {
        showBackButton()
        AppConstants.CurrentScreen = appConstants.HorseListingScreen
        containerView.subviews.forEach({ $0.removeFromSuperview() })
        let notifList : Notifications = Notifications(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
        containerView.addSubview(notifList)
        
        
    }
    
    func showUserCredentials() {
        
        if !SharedPreferenceUtils._sharedInstance.getBoolValue(appConstants.FIRST_TYM_IMAGE) {
            getUserDetail()
        }
        
        let imageString: String = SharedPreferenceUtils._sharedInstance.getStringValue(self.appConstants.PROFILE_IMAGE)
        if !imageString.isEmpty {
            let dataDecoded:NSData = NSData(base64EncodedString: imageString, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!
            self.headerContainer?.image.image = UIImage(data: dataDecoded)!
        }else {
            self.headerContainer?.image.image = UIImage.init(imageLiteral: "newProfilePic.png")
        }
        
        self.headerContainer?.name.text = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.name)
    }
    
    func getUserDetail(){
        //send to server
        let postString = "action=getdetails&userId="+SharedPreferenceUtils._sharedInstance.getStringValue(appConstants.memberID);
        
        webService.HTTPPostJSON(appConstants.LOGIN, data: postString) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                if error != nil{
                    return
                }
                SharedPreferenceUtils._sharedInstance.saveBoolValue(self.appConstants.FIRST_TYM_IMAGE, value: true)
                
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    if ((data?[self.appConstants.ErrorCode]) != nil) {
                        //Error
                        return
                    }else{
                        //User Data
                        let userData = (data?[self.appConstants.data])! as! NSDictionary
                        let media_base_url = userData.objectForKey(self.appConstants.media_base_url) as? String
                        let mediaSource:String = (userData.objectForKey(self.appConstants.profilePic) as? String)!
                        self.headerContainer!.image.downloadedFrom(media_base_url!+mediaSource)
                        self.headerContainer!.image.downloadWithCallback(media_base_url!+mediaSource, contentMode: UIViewContentMode.ScaleAspectFit, callback: { (image) in
                            let selectedImage = self.appConstants.RBSquareImage(image)
                            let imageData:NSData = UIImageJPEGRepresentation(selectedImage,0.5)!
                            let strBase64:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                            SharedPreferenceUtils._sharedInstance.saveStringValue(self.appConstants.PROFILE_IMAGE, value: strBase64)
                            NSNotificationCenter.defaultCenter().postNotificationName(self.appConstants.kN_UpdateProfilePicNotification, object: "")
                        })
                    }
                }
            }
        }
    }
    
    
    func drawUI() {
        backgroundView = UIImageView.init(frame: self.view.frame)
        //       backgroundView!.image = UIImage.init(imageLiteral: "blurbackground.jpg")
        backgroundView!.image = UIImage.init(imageLiteral: "mainImage.png")
        self.view.addSubview(backgroundView!)
        
        headerContainer = HeaderView(frame: CGRectMake(0,yy,self.view.frame.size.width,self.view.frame.size.height * 0.2))
        self.view.addSubview(headerContainer!)
        
        let defaults = NSUserDefaults.standardUserDefaults() //pooja
        let badgeStr = defaults.valueForKey("notificationBadge")
        self.headerContainer?.notifLbl.text = badgeStr as? String
        self.view.addSubview(headerContainer!)

        print("helllooooooooo \(badgeStr as? String)")
        
        self.headerContainer?.notifButton.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(gotoNotification(_:)))
        self.headerContainer?.notifButton.addGestureRecognizer(tapRecognizer)
        
        
        yy += headerContainer!.frame.size.height
        
        hline.frame = CGRectMake(0 , yy , appConstants.screenWidth, 1)
        hline.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        self.view.addSubview(hline)
        
        sideBarContainer = SideNavigationBarView(frame: CGRectMake(0,yy,self.view.frame.size.width * 0.15,self.view.frame.size.height * 0.8))
        sideBarContainer!.menuSelected = self
        
        self.view.addSubview(sideBarContainer!)
        
        vline.frame = CGRectMake(self.view.frame.size.width * 0.15 , yy , 1, appConstants.screenHeight)
        vline.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        self.view.addSubview(vline)
        
        containerView.frame = CGRectMake(self.view.frame.size.width * 0.15 + 2 , yy + 2 , self.view.frame.size.width * 0.85 - 2, self.view.frame.size.height * 0.8 - 2)
        containerView.backgroundColor = UIColor.clearColor()
        self.view.addSubview(containerView)
        
       
        
        self.showFirstScreen()
    }
    
    /* func chatLogin(){
     let user = QBUUser()
     user.ID = UInt(SharedPreferenceUtils.sharedInstance.getIntValue(self.appConstants.CHAT_USER_ID_SERVER))
     user.password = "123456789"
     QBChat.instance().connectWithUser(user) { (error: NSError?) -> Void in
     print(error)
     }
     
     QBRequest.logInWithUserEmail(SharedPreferenceUtils.sharedInstance.getStringValue(self.appConstants.CHAT_USER_ID), password: self.appConstants.CHAT_PASSWORD, successBlock: { (response, user) in
     print(response)
     }, errorBlock: { (error) in
     print(error)
     })
     
     let timeout: NSTimeInterval = 2.0
     
     QBChat.instance().pingServerWithTimeout(timeout) { (timeInterval: NSTimeInterval, success: Bool) in
     // time interval of ping and whether it was successful
     }
     }*/
    
    
    func showBackButton() {
        if backButton != nil {
            backButton?.removeFromSuperview()
        }
        
        backButton = UILabel.init(frame: CGRectMake(self.view.frame.size.width - 40 * appConstants.ScreenFactor,yy - 20 * appConstants.ScreenFactor, 40 * appConstants.ScreenFactor,20 * appConstants.ScreenFactor))
        backButton!.text = "Back"
        backButton!.font = UIFont.init(name: "Helvetica", size: 12 * appConstants.ScreenFactor)
        backButton!.backgroundColor = UIColor.clearColor()
        backButton!.textAlignment = NSTextAlignment.Left
        backButton?.textColor = UIColor.whiteColor()
        backButton!.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(back(_:)))
        backButton!.addGestureRecognizer(tapRecognizer)
        self.view.addSubview(backButton!)
    }
    
    func back(gestureRecognizer: UITapGestureRecognizer){
        
        stopTimer()
        
        if AppConstants.CurrentScreen == appConstants.HomeScreen
            || AppConstants.CurrentScreen == appConstants.HorseListingScreen
            || AppConstants.CurrentScreen == appConstants.CalendarScreen
            || AppConstants.CurrentScreen == appConstants.OwnerListingScreen
            || AppConstants.CurrentScreen == appConstants.SettingsScreen
            || AppConstants.CurrentScreen == appConstants.MessagingContactListScreen
           // || AppConstants.CurrentScreen == appConstants.OwnerDetailScreen   //pooja to rectify owner detail back issue  --27/4
        {
            print(AppConstants.CurrentScreen)
            if backButton != nil {
                backButton?.removeFromSuperview()
            }
            AppConstants.SelectedMenu = 0
            showFirstScreen()
            
            
        }
        
        
        else if AppConstants.CurrentScreen == appConstants.NotificationScreen{
            print(AppConstants.CurrentScreen)
            AppConstants.CurrentScreen = appConstants.HomeScreen
            
             drawUI()
        containerView.subviews.forEach({ $0.removeFromSuperview() })
           
            
        }
        
        
        else if
            AppConstants.CurrentScreen == appConstants.HorseDetailingScreen
            ||
                AppConstants.CurrentScreen == appConstants.GalleryScreen
            || AppConstants.CurrentScreen == appConstants.DailyActivityScreen
           // || AppConstants.CurrentScreen == appConstants.OwnerListingScreen//pooja
        {
            
            print(AppConstants.CurrentScreen)
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            showUserCredentials()
            let horseList : HorseListing = HorseListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
            if AppConstants.CurrentScreen == appConstants.HorseDetailingScreen {
                horseList.fromWhere = "HORSE_DETAIL"
                
            }else if AppConstants.CurrentScreen == appConstants.GalleryScreen {
                horseList.fromWhere = "GALLERY"
            }

            else {
                horseList.fromWhere = ""
            }
            AppConstants.CurrentScreen = appConstants.HorseListingScreen
            containerView.addSubview(horseList) //pooja commented
        }else if AppConstants.CurrentScreen == appConstants.DailyActivityFromDetailScreen
            ||  AppConstants.CurrentScreen == appConstants.GalleryScreenFromHorseDetail {
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            print(AppConstants.CurrentScreen)
            AppConstants.CurrentScreen = appConstants.HorseDetailingScreen
            let horseDetail:HorseDetailView = HorseDetailView(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
            // horseDetail.backgroundColor = UIColor().HexToColor(appConstants.font_black_color, alpha: 1.0)
            horseDetail.backgroundColor = UIColor.clearColor()
            horseDetail.getHorseDetail(AppConstants.CurrentHorseID)
            containerView.addSubview(horseDetail)
        }else if
            AppConstants.CurrentScreen == appConstants.MessagingScreenFromOwnerListing
        //OwnerScreenDetailByPooja
            //pooja on 27-4 to resolve back issue from ownerdetail screen
         
        {
            print(AppConstants.CurrentScreen)
          containerView.subviews.forEach({ $0.removeFromSuperview() })
            let defaults = NSUserDefaults.standardUserDefaults()
            let contactDict : NSDictionary = defaults.valueForKey("dictForOwnerDetail") as! NSDictionary
            let base_url : String = defaults.valueForKey("media_base_url") as! String
            
            
            let chatHorseList : ContactDetailView = ContactDetailView(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
            
            chatHorseList.backgroundColor = UIColor.clearColor()
            chatHorseList.getUserDetail(contactDict.objectForKey(appConstants.userId) as! String, media_base_url: base_url , chatDict: contactDict)
             containerView.addSubview(chatHorseList)
            
            
        }
        
        else if AppConstants.CurrentScreen == appConstants.OwnerDetailScreen{
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            let contactList : ContactListing = ContactListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
        
            containerView.addSubview(contactList)

        } //pooja - 1/5
        else if AppConstants.CurrentScreen == appConstants.AddOrUpdateEventScreen{
            print(AppConstants.CurrentScreen)
            AppConstants.CurrentScreen = appConstants.CalendarScreen
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            let calendarDisplay : ShowCalendarInAppView = ShowCalendarInAppView(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
            //
            calendarDisplay.backgroundColor = UIColor().HexToColor(appConstants.font_black_color, alpha: 1.0)
            calendarDisplay.backgroundColor = UIColor.clearColor()
            containerView.addSubview(calendarDisplay)
        }else if AppConstants.CurrentScreen == appConstants.AddDailyActivityScreen
            || AppConstants.CurrentScreen == appConstants.AddDailyActivityFromDetailScreen {
            print(AppConstants.CurrentScreen)
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            let horseDailyActivity:DailyActivityListing = DailyActivityListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
            if AppConstants.CurrentScreen == appConstants.AddDailyActivityFromDetailScreen {
                horseDailyActivity.fromWhere = "HORSE_DETAIL"
                AppConstants.CurrentScreen = appConstants.DailyActivityFromDetailScreen
            }else {
                horseDailyActivity.fromWhere = "DAILY_ACT"
                AppConstants.CurrentScreen = appConstants.DailyActivityScreen
            }
            //   horseDailyActivity.backgroundColor = UIColor.whiteColor()
            horseDailyActivity.backgroundColor = UIColor.clearColor()
            horseDailyActivity.getHorseDetail(AppConstants.CurrentHorseID)
            horseDailyActivity.getHorseDailyActivityList(AppConstants.CurrentHorseID)
            containerView.addSubview(horseDailyActivity)
        }else if AppConstants.CurrentScreen == appConstants.DailyActivityDetailScreen
            || AppConstants.CurrentScreen == appConstants.DailyActivityDetailScreenFromDetailScreen {
            //***************** call webservice to change the edited data ****************
            //***************** call webservice to change the edited data ****************
            //***************** call webservice to change the edited data ****************
            //***************** call webservice to change the edited data ****************
            if !SharedPreferenceUtils._sharedInstance.getBoolValue(appConstants.ISOWNER) && AppConstants.dataModelForDetailEditing != nil {
                showProgress()
                let toSend: NSMutableDictionary = NSMutableDictionary()
                let taskListToSend: NSMutableArray = NSMutableArray()
                
                toSend.setObject(SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.memberID), forKey: appConstants.userId)
                toSend.setObject(AppConstants.dataModelForDetailEditing.objectForKey(self.appConstants.task_id) as! String, forKey: appConstants.task_id)
                toSend.setObject("dailyHorseActivitySave", forKey: appConstants.action)
                toSend.setObject(AppConstants.CurrentHorseID, forKey: "horse_id")
                
                let subActivityList: NSMutableArray = AppConstants.dataModelForDetailEditing.objectForKey(self.appConstants.tasklist) as! NSMutableArray
                
                
                for i in 0  ..< subActivityList.count  {
                    let activityDataToSend: NSMutableDictionary = NSMutableDictionary()
                    let subActivity = subActivityList.objectAtIndex(i) as! NSMutableDictionary
                    
                    let typeValue: String = subActivity.objectForKey(appConstants.datatype_id) as! String
                    let value:Int = Int(typeValue)!
                    let done: Bool = subActivity.objectForKey(appConstants.selected) as! Bool
                    
                    let value1 = subActivity.objectForKey(appConstants.tasklist_comment)
                    if(appConstants.nullToNil(value1) != nil && !( value1 is NSNull )){
                        let value11: NSString = value1 as! NSString
                        if(value11.isEqual("Tap to Add Comment")){
                            activityDataToSend.setObject("", forKey: "comment")
                        }else{
                            activityDataToSend.setObject(value11, forKey: "comment")
                        }
                    }else{
                        activityDataToSend.setObject("", forKey: "comment")
                    }
                    
                    activityDataToSend.setObject(subActivity.objectForKey(appConstants.tasklist_id) as! String, forKey: "id")
                    activityDataToSend.setObject(typeValue, forKey: "input_type")
                    
                    if(value >= 7){
                        if(appConstants.nullToNil(subActivity.objectForKey(appConstants.tasklist_value)) != nil){
                            let value: String = subActivity.objectForKey(appConstants.tasklist_value) as! String
                            activityDataToSend.setObject(value, forKey: "value")
                        }else{
                            activityDataToSend.setObject("", forKey: "value")
                        }
                        
                        if(appConstants.nullToNil(subActivity.objectForKey(appConstants.value_type)) != nil){
                            let value: String = subActivity.objectForKey(appConstants.value_type) as! String
                            activityDataToSend.setObject(value, forKey: appConstants.value_type)
                        }else{
                            activityDataToSend.setObject("", forKey: appConstants.value_type)
                        }
                        
                    }else {
                        activityDataToSend.setObject("", forKey: appConstants.value_type)
                        if(value == 2) {
                            if(done) {
                                activityDataToSend.setObject("Yes", forKey: "value")
                            }else{
                                activityDataToSend.setObject("No", forKey: "value") ////
                            }
                        }else if(value == 5 || value == 4) {
                            if(appConstants.nullToNil(subActivity.objectForKey(appConstants.tasklist_value)) != nil){
                                let value: String = subActivity.objectForKey(appConstants.tasklist_value) as! String
                                activityDataToSend.setObject(value, forKey: "value")
                            }else{
                                activityDataToSend.setObject("", forKey: "value")
                            }
                        }else if(value == 3) {
                            //Checkbox condition
                            var dataC: String = ""
                            
                            let arr = AppConstants.checkBoxImageArray.objectAtIndex(i) as! NSMutableArray
                            for iii in 0  ..< arr.count  {
                                let checkData = arr.objectAtIndex(iii) as! NSMutableDictionary
                                let isTicked: Bool = checkData.objectForKey(self.appConstants.selected) as! Bool
                                if(isTicked){
                                    dataC += checkData.valueForKey(self.appConstants.value_type) as! String
                                }
                                if(iii < AppConstants.checkBoxImageArray.count - 1){
                                    dataC += ";"
                                }
                            }
                            
                            activityDataToSend.setObject(dataC, forKey: "value")
                        }else{
                            if(appConstants.nullToNil(subActivity.objectForKey(appConstants.tasklist_value)) != nil){
                                let value: String = subActivity.objectForKey(appConstants.tasklist_value) as! String
                                activityDataToSend.setObject(value, forKey: "value")
                            }else{
                                activityDataToSend.setObject("", forKey: "value")
                            }
                        }
                    }
                    taskListToSend.addObject(activityDataToSend)
                }
                toSend.setObject(taskListToSend, forKey: "tasklist")
                print("dataModelForDetailEditing>>> = \(toSend)")
                print("dataModelForDetailEditing ")
                
                //Webservice call
                do {
                    
                    try updateData(toSend)
                } catch {
                    debugPrint("Generic error")
                }
                
            }else{
                containerView.subviews.forEach({ $0.removeFromSuperview() })
                let horseDailyActivity:DailyActivityListing = DailyActivityListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                if AppConstants.CurrentScreen == appConstants.DailyActivityDetailScreenFromDetailScreen {
                    horseDailyActivity.fromWhere = "HORSE_DETAIL"
                    AppConstants.CurrentScreen = appConstants.DailyActivityFromDetailScreen
                }else {
                    horseDailyActivity.fromWhere = "DAILY_ACT"
                    AppConstants.CurrentScreen = appConstants.DailyActivityScreen
                }
                //   horseDailyActivity.backgroundColor = UIColor.whiteColor()
                horseDailyActivity.backgroundColor = UIColor.clearColor()
                horseDailyActivity.getHorseDetail(AppConstants.CurrentHorseID)
                horseDailyActivity.getHorseDailyActivityList(AppConstants.CurrentHorseID)
                containerView.addSubview(horseDailyActivity)
            }
        }else if AppConstants.CurrentScreen == appConstants.ChangePasswordScreen {
            AppConstants.CurrentScreen = appConstants.SettingsScreen
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            let settingView : SettingView = SettingView(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
            containerView.addSubview(settingView)
        }else if AppConstants.CurrentScreen == appConstants.ShareScreenFromGalleryScreen
            || AppConstants.CurrentScreen == appConstants.ShareScreenFromGalleryScreenFromHorseDetail {
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            let horseGalleryView:GalleryView = GalleryView(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
            horseGalleryView.getGalleryDetail(AppConstants.CurrentHorseID)
            
            if AppConstants.CurrentScreen == appConstants.ShareScreenFromGalleryScreen {
                AppConstants.CurrentScreen = appConstants.GalleryScreen
            }else {
                AppConstants.CurrentScreen = appConstants.GalleryScreenFromHorseDetail
            }
            containerView.addSubview(horseGalleryView)
        }else if AppConstants.CurrentScreen == appConstants.MessagingScreen {
            containerView.subviews.forEach({ $0.removeFromSuperview() })
            
            AppConstants.CurrentScreen = appConstants.MessagingContactListScreen
            
            let userType = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.user_type)
            
            //  let animals = NSUserDefaults .standardUserDefaults () .objectForKey ( "user_type" )
            print( "We saved this data: \( userType )" )
            
            if(userType == "2")
            {
                let chatContactList : ChatContactListing = ChatContactListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                containerView.addSubview(chatContactList)
                
            }
                
            else
            {
                let chatHorseList : ChatHorseList = ChatHorseList(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                containerView.addSubview(chatHorseList)
            }
            
        }
    }
    
    func updateData(toSend : NSMutableDictionary) throws{
        
        let data = self.appConstants.convertDictionaryToString(toSend)
        
       // let data = try! NSJSONSerialization.dataWithJSONObject(toSend, options: NSJSONWritingOptions.PrettyPrinted)
        
      //  let jsonstr = NSString(data: data, encoding: NSUTF8StringEncoding)
      //  let trimmedStr = jsonstr!.stringByReplacingOccurrencesOfString("(", withString: "")
      //  print(trimmedStr)
        
        webService.HTTPPostJSON(appConstants.DAILY_ACTIVITY, data: data!) { (response, error) -> Void in
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.hideProgress()
                
                if error != nil{
                    return
                }
                print("response = \(response)")
                let data = self.appConstants.convertStringToDictionary(response)
                if data != nil {
                    AppConstants.dataModelForDetailEditing = nil
                    
                    //if ((data?[self.appConstants.ErrorCode]) != nil) {
                    //Error
                    //return
                    //}else{
                    self.containerView.subviews.forEach({ $0.removeFromSuperview() })
                    let horseDailyActivity:DailyActivityListing = DailyActivityListing(frame: CGRectMake(0 , 0 , self.containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                    if AppConstants.CurrentScreen == self.appConstants.DailyActivityDetailScreenFromDetailScreen {
                        horseDailyActivity.fromWhere = "HORSE_DETAIL"
                        AppConstants.CurrentScreen = self.appConstants.DailyActivityFromDetailScreen
                    }else {
                        horseDailyActivity.fromWhere = "DAILY_ACT"
                        AppConstants.CurrentScreen = self.appConstants.DailyActivityScreen
                    }
                    //  horseDailyActivity.backgroundColor = UIColor.whiteColor()
                    horseDailyActivity.backgroundColor = UIColor.clearColor()
                    horseDailyActivity.getHorseDetail(AppConstants.CurrentHorseID)
                    horseDailyActivity.getHorseDailyActivityList(AppConstants.CurrentHorseID)
                    self.containerView.addSubview(horseDailyActivity)
                    // }
                }
            }
        }
    }
    
    func showFirstScreen() {
        //By default container view
        AppConstants.CurrentHorseID = ""
        AppConstants.CurrentScreen = appConstants.HomeScreen
        containerView.subviews.forEach({ $0.removeFromSuperview() })
        let mainContainer : NavigationMainView = NavigationMainView(frame: CGRectMake(0 ,0 , containerView.frame.size.width, self.view.frame.size.height * 0.8))
        mainContainer.menuSelected = self
        containerView.addSubview(mainContainer)
        
        showUserCredentials()
    }
    
    func menuSelected(selectedItem: Int) -> Void {
        showUserCredentials()
        
        switch selectedItem {
        case 1:
            //Horse List
            if AppConstants.SelectedMenu != 1{
                showBackButton()
                AppConstants.SelectedMenu = selectedItem
                AppConstants.CurrentScreen = appConstants.HorseListingScreen
                containerView.subviews.forEach({ $0.removeFromSuperview() })
                let horseList : HorseListing = HorseListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                horseList.fromWhere = "HORSE_DETAIL"
                containerView.addSubview(horseList)
            }
            break
        case 2:
            //Daily Activity
            if AppConstants.SelectedMenu != 2 {
                showBackButton()
                AppConstants.SelectedMenu = selectedItem
                AppConstants.CurrentScreen = appConstants.HorseListingScreen
                containerView.subviews.forEach({ $0.removeFromSuperview() })
                let horseList : HorseListing = HorseListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                horseList.fromWhere = "DAILY_ACT"
                containerView.addSubview(horseList)
            }
            break
        case 3:
            //Messaging
            if AppConstants.SelectedMenu != 3 {
                showBackButton()
                AppConstants.CurrentScreen = appConstants.MessagingContactListScreen
                AppConstants.SelectedMenu = selectedItem
                containerView.subviews.forEach({ $0.removeFromSuperview() })
                
                let userType = SharedPreferenceUtils.sharedInstance.getStringValue(appConstants.user_type)
                
              /* if(userType == "2")
                {
                    let chatContactList : ChatContactListing = ChatContactListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                    containerView.addSubview(chatContactList)
                    
                }
                 
                else
                {
                    let chatHorseList : ChatHorseList = ChatHorseList(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                    chatHorseList.fromWhere = "CHAT"
                    containerView.addSubview(chatHorseList)
                }*/
                
                if (userType=="3"){
                    //trainer
                    
                    let chatHorseList : ChatHorseList = ChatHorseList(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                    chatHorseList.fromWhere = "CHAT"
                    containerView.addSubview(chatHorseList)
                    // trainer will see horselist and get message history(bottom of screen )
                    // a list of horse hould be appear with view button
                    //on click on view btn new window will be appear with owner list and a chat windwo(bottom of screen) should be appear
                }else{
                    //  owner
                    let ownerMessgagelist :OwnerMessageFragment=OwnerMessageFragment(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                    containerView.addSubview(ownerMessgagelist)
                }
            }
            break
        case 4:
            //Calendar
            if AppConstants.SelectedMenu != 4 {
                showBackButton()
                AppConstants.SelectedMenu = selectedItem
                AppConstants.CurrentScreen = appConstants.CalendarScreen
                
                containerView.subviews.forEach({ $0.removeFromSuperview() })
                let calendarDisplay : ShowCalendarInAppView = ShowCalendarInAppView(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                calendarDisplay.backgroundColor = UIColor.clearColor()
                
                //    calendarDisplay.backgroundColor = UIColor().HexToColor(appConstants.font_black_color, alpha: 1.0)
                calendarDisplay.backgroundColor = UIColor.clearColor()
                containerView.addSubview(calendarDisplay)
            }
            break
        case 5:
            //Picture
            if AppConstants.SelectedMenu != 5 {
                showBackButton()
                AppConstants.SelectedMenu = selectedItem
                AppConstants.CurrentScreen = appConstants.HorseListingScreen
                containerView.subviews.forEach({ $0.removeFromSuperview() })
                let horseList : HorseListing = HorseListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                horseList.fromWhere = "GALLERY"
                containerView.addSubview(horseList)
            }
            break
        case 6:
            //Owner Listing
            if AppConstants.SelectedMenu != 6 {
                showBackButton()
                AppConstants.SelectedMenu = selectedItem
                AppConstants.CurrentScreen = appConstants.OwnerListingScreen
                containerView.subviews.forEach({ $0.removeFromSuperview() })
                let contactList : ContactListing = ContactListing(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                containerView.addSubview(contactList)
            }
            break
        case 7:
            //Settings
            if AppConstants.SelectedMenu != 7 {
                showBackButton()
                AppConstants.SelectedMenu = selectedItem
                AppConstants.CurrentScreen = appConstants.SettingsScreen
                containerView.subviews.forEach({ $0.removeFromSuperview() })
                let settingView : SettingView = SettingView(frame: CGRectMake(0 , 0 , containerView.frame.size.width, self.view.frame.size.height * 0.8 - 2))
                containerView.addSubview(settingView)
            }
            break
        default:
            break
            
        }
    }
    
    func showProgress() {
        if(activityIndicator != nil){
            activityIndicator.removeFromSuperview()
            activityIndicator = nil
        }
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        activityIndicator = UIActivityIndicatorView(frame: self.view.bounds)
        activityIndicator.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        activityIndicator.tintColor = UIColor().HexToColor(appConstants.button_color)
        activityIndicator.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.3)
        self.view.addSubview(activityIndicator)
        activityIndicator.userInteractionEnabled = false
        activityIndicator.startAnimating()
    }
    
    
    func hideProgress() {
        self.activityIndicator.removeFromSuperview()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
        activityIndicator = nil
    }
    
    func startTimer()  {
        stopTimer()
        timer = NSTimer.scheduledTimerWithTimeInterval(15, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
    
    func update() {
        if(AppConstants.CurrentScreen == appConstants.MessagingScreen){
            NSNotificationCenter.defaultCenter().postNotificationName(self.appConstants.kN_FetchTimerNotification, object: "")
        }
    }
    
    func stopTimer() {
        timer.invalidate()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}

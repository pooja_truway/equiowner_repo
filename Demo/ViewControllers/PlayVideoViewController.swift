//
//  PlayVideoViewController.swift
//  Demo
//
//  Created by KOMAL SHARMA on 02/10/16.
//  Copyright © 2016 BreakfastBay. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PlayVideoViewController: UIViewController, UINavigationBarDelegate{
    var moviePath: String = ""
     
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let player = AVPlayer(URL: NSURL(string:moviePath)!)
        let playerController = AVPlayerViewController()
        
        playerController.player = player
        self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        playerController.view.frame = self.view.frame
        
        player.play()
        
        
        self.view.backgroundColor = UIColor.blackColor()
        let navigationBar = UINavigationBar(frame: CGRectMake(0, 0, self.view.frame.size.width, 50))
        navigationBar.backgroundColor = UIColor.whiteColor()
        navigationBar.delegate = self;
        
        // Create left and right button for navigation item
        let leftButton =  UIBarButtonItem(title: "Cancel", style:   UIBarButtonItemStyle.Plain, target: self, action: #selector(PlayVideoViewController.cancel))
        
        // Create two buttons for the navigation item
        navigationItem.leftBarButtonItem = leftButton
        
        // Assign the navigation item to the navigation bar
        navigationBar.items = [navigationItem]
        
        // Make the navigation bar a subview of the current view controller
        self.view.addSubview(navigationBar)
    
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}